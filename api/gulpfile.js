let gulp = require('gulp');
let jasmineNode = require('gulp-jasmine-node');


/**
 * Specifies a relative path to the api static folder where files are
 * served from
 */
let testsRootDir = './tests';

/**
 * Execute all tests once for the client code
 */
gulp.task('test', function (done) {
    gulp.src([`${testsRootDir}/**/*[tT]est[s].js`]).pipe(jasmineNode({}));
});
