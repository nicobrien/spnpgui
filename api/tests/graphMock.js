/**
 * A api testing helper
 * Data and functions for generating graphs that may be parsed by csplConverter
 */

const oneInputArc = { 
    places: [ 
        { id: '27', name: 'test_place', tokens: '5' } 
    ],
    transitions: [ 
        { 
            id: '29',
            name: 'test_transition',
            distribution: 'Exponential',
            parameter: '2' 
        } 
    ],
    edges: [ 
        // this is an arc from the place to the transition: an input arc
        { source: '27', target: '29' } 
    ] 
};

const oneOutputArc = { 
    places: [ 
        { id: '27', name: 'test_place', tokens: '5' } 
    ],
    transitions: [ 
        { 
            id: '29',
            name: 'test_transition',
            distribution: 'Exponential',
            parameter: '2' 
        } 
    ],
    edges: [ 
        // this is an arc from the place to the transition: an input arc
        { source: '29', target: '27' } 
    ] 
};

const threeInOneOut = { 
    places: [ 
        { id: '2', name: 'p1', tokens: '55' },
        { id: '9', name: 'p2', tokens: '700' } 
    ],
    transitions: [ 
        { 
            id: '3',
            name: 't0',
            distribution: 'Exponential',
            parameter: '0' 
        }, { 
            id: '5',
            name: 't1',
            distribution: 'Exponential',
            parameter: '0' 
        }, { 
            id: '6',
            name: 't2',
            distribution: 'Exponential',
            parameter: '0' 
        } 
    ],
    edges: [
        { source: '2', target: '3' },
        { source: '2', target: '5' },
        { source: '2', target: '6' },
        { source: '3', target: '9' } 
    ] 
};

/**
 * A function that can generate a graph with a single place
 * 
 * @param {String} name The name to give the place
 * @param {Number} tokens The number of tokens to give the place
 */
const onePlaceGraphCreator = ({ name, tokens }) =>({ 
    places: [ 
        placeCreator({ id: '1', name, tokens }),
    ],
    transitions: [ ],
    edges: [ ] 
});

/**
 * A function that can generate a graph with a single transition
 * 
 * @param {String} name The name to give the transition
 * @param {Number} distribution The distribution type to give the transition
 * @param {Number} parameter The parameter to give the transition
 */
const oneTransitionGraphCreator = ({ name, distribution, parameter }) => ({ 
    places: [ ],
    transitions: [
        transitionCreator({ id: 1, name, distribution, parameter })
     ],
    edges: [ ] 
});

/**
 * A function that can generate a single transition object
 * 
 * @param {Number} id the id of the transition
 * @param {String} name The name to give the transition
 * @param {Number} distribution The distribution type to give the transition
 * @param {Number} parameter The parameter to give the transition
 */
const transitionCreator = ({ id, name, distribution, parameter }) => ({
    id: id,
    name: name,
    distribution: distribution,
    parameter: parameter 
});

/**
 * Sources and targets should not be of the same type
 * Sources and targets may be generated using {@link transitionCreator} and
 * {@link placeCreator}
 * 
 * @param {Object} source a source object - either place or transition
 * @param {*} target a target object - either place or transition
 */
const edgeCreator = (source, target) => ({
    source: source.id, 
    target: target.id
});

/**
 * A function that can generate a single place object
 * 
 * @param {Number} id the id of the place
 * @param {String} name The name to give the place
 * @param {Number} tokens The number of tokens to give the place
 */
const placeCreator = ({ id, name, tokens }) => ({
    id: id,
    name: name,
    tokens: tokens
});

/**
 * A function that can generate an empty graph
 */
const emptyGraph = () =>  ({ places: [], transitions: [], edges: [] });

module.exports = {
    emptyGraph: emptyGraph,
    oneInputArc: oneInputArc,
    edgeCreator: edgeCreator,
    placeCreator: placeCreator,
    oneOutputArc: oneOutputArc,
    threeInOneOut: threeInOneOut,
    transitionCreator: transitionCreator,
    onePlaceGraphCreator: onePlaceGraphCreator,
    oneTransitionGraphCreator: oneTransitionGraphCreator
};