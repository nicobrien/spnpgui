
/**
 * Example test
 */
describe("An example test test", function() {
    it("should have true === true", function(){
        expect(true).toBe(true);
    });
});

/**
 * Example of how to temporarily disable tests without commenting
 * prefix 'describe' or 'it' with 'x'
 */
describe("An example test that wont be run", function() {
    xit("if it is prefixed by x then the specs shouldn't be run", function(){
        expect(false).toBe(true);
    });
});
