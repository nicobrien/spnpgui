
const RESOURCE_NOT_FOUND = {};

const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const HttpStatus = require('http-status-codes');
const graphsController = require('./controllers/graphsController');
const CommandlineController = require('./controllers/commandlineController');

const app = express();

/**
 * The spnp-engine service is currently run locally
 */
const SPNP_ENGINE_URL = '127.0.0.1';
const SPNP_ENGINE_ENDPOINT = '/analyze';

let clController = new CommandlineController(process.argv);
const port = clController.getPort();
const csplPort = clController.getCSPLPort();


app.use(express.static('static'));
app.use(bodyParser.json());

/**
 * The root get should return the app
 */
app.get('/', (req, res) => { res.sendFile(path.join(__dirname + '/static/index.html')); });

/**
 * Create a graph in the database and return the id
 * 
 * req.body = { 
 *     [name: <name-for-graph>, model: <graph-model>, ... ]
 * }
 */
app.post('/createGraph', graphsController.createGraph);

/**
 * Save an existing graph in the database
 * 
 * req.body = { 
 *     id: <id>, ... 
 *     [name: <name-for-graph>, model: <graph-model>, ...] 
 * }
 */
app.post('/saveGraph/:id([0-9]+)', graphsController.saveGraph);

/**
 * Retrieve a graph from the database by id
 * 
 * req.body = { id: <id>, ... }
 */
app.get('/graphs/:id([0-9]+)', graphsController.getGraph);

/**
 * Retrieve all graphs from the database
 * The response will be an array of graph objects which have the
 * structure:
 * 
 * { 
 *     id: <id>, 
 *     name: <name>, 
 *     model: <model>, 
 *     create_time: <time>, 
 *     update_time: <time>
 * }
 */
app.get('/graphs', graphsController.getGraphs);

/**
 * Retrieve the CSPL (c-like stochastic petri net
 * language) description for a given graph.
 * 
 * req.body = { nodes: nodes }
 */
app.post('/cspl', graphsController.getCSPL);

/**
 * Convert the graph to CSPL and pass to the spnp-engine for 
 * analysis
 * @WIP
 */
app.post('/analyze', graphsController.analyze(`http://${SPNP_ENGINE_URL}:${csplPort}${SPNP_ENGINE_ENDPOINT}`));


// todo: have a 404 page
app.get('/*', (req, res) => { res.status(HttpStatus.NOT_FOUND).send(RESOURCE_NOT_FOUND); });

/**
 * Start the server
 */
app.listen(port, () => {
    console.log('SPNP-GUI server listening on port: ', port);
    console.log('SPNP-ENGINE service set to: ', `${SPNP_ENGINE_URL}:${csplPort}${SPNP_ENGINE_ENDPOINT}`);
}).on('error', (e) => {
    console.log(`Could not start service on ${port}. Try specifying a different port with command line arg:`);
    console.log('node app.js -p 3001');
});


module.exports = app;
