

/**
 * Handles parsing of spnp results
 */
class SPNPResultParser {

    /**
     * @param {String} result  The result of SPNP
     * analysis on a graph
     */
    constructor(result) {
        this._spnpResult = (result && result.result && result.result.out)
            ? result.result.out
            : '';
    }

    /**
     * Parse the results and return a client
     * side consumable object representing the 
     * SPNP results
     */
    parse() {
        this._average = this.getStdAverage();
        this._netInfo = this.getNetInfoObj();
        this._expected = this.getExpectedStatements();
        return {
            raw: this._spnpResult,
            netInfo: this._netInfo,
            expected: this._expected,
            average: this._average
        };
    }

    /**
     * @return {Object} If the result has a net description
     * then return a object description of the net in the 
     * form { "discrete places": <n>, ... }
     */
    getNetInfoObj() {
        const RE_HAS_NET = /\nNET:\n[=]+/g;
        if (!RE_HAS_NET.test(this._spnpResult)) { 
            console.log('Didnt find net info');
            return {}; 
        }

        const makeTestRe = (name, re) => {
            const key = `RE_${name.toUpperCase().replace(/ /g, '_')}`;
            let testRe = {};
            testRe[key] = { name, re };
            return testRe;
        };

        const tests = Object.assign(
            {},
            makeTestRe('discrete places', /\n\tdiscrete places: ([0-9]+)\n/g),
            makeTestRe('timed transitions', /\n\ttimed transitions: ([0-9]+)\n/g),
            makeTestRe('constant input arcs', /\n\tconstant input arcs: ([0-9]+)\n/g),
            makeTestRe('variable input arcs', /\n\tvariable input arcs: ([0-9]+)\n/g),
            makeTestRe('constant output arcs', /\n\tconstant output arcs: ([0-9]+)\n/g),
            makeTestRe('variable output arcs', /\n\tvariable output arcs: ([0-9]+)\n/g),
            makeTestRe('immediate transitions', /\n\timmediate transitions: ([0-9]+)\n/g),
            makeTestRe('constant inhibitor arcs', /\n\tconstant inhibitor arcs: ([0-9]+)\n/g),
            makeTestRe('variable inhibitor arcs', /\n\tvariable inhibitor arcs: ([0-9]+)\n/g)
        );

        const infoObj = {};

        Object.keys(tests).forEach(function(key) {
            const re = tests[key].re;
            const name = tests[key].name;
            const match = re.exec(this._spnpResult);
            if (match && match.length > 1) {
                const value = match[1];
                infoObj[name] = value;
            }
        }, this);

        return infoObj;
    }

    /**
     * @return {Array} If the result has expectations 
     * then return an array of them
     */
    getExpectedStatements() {

        // IMPORTANT: /g also prevents infinite loop by updating 
        // internal index of match from exec
        const RE_EXPECTED = /\nEXPECTED: ([\w\s\d=:\.]+)\n/g;

        const expected = [];
        let match = RE_EXPECTED.exec(this._spnpResult);
        while (match != null) {
            expected.push(match[1]);
            match = RE_EXPECTED.exec(this._spnpResult);
        }
        return expected;
    }

    /**
     * @return {Object} If the result has Averages, then
     * return an object mapping places and transitions
     * to those averages of the form 
     * {
     *      p0: {
     *          probability: <Number>,
     *          average: <Number>
     *      },
     *      ...
     * }
     */
    getStdAverage() {

        const RE_HAS_AVERAGE = /\nAVERAGE:\n[=]+/g;
        if (!RE_HAS_AVERAGE.test(this._spnpResult)) { 
            console.log('Didnt find average info');
            return {}; 
        }

        let str = '';
        const RE_AVERAGE_SECTION = /\nAVERAGE:\n[=]+([\d\s\w\[\]:\.\-\+]+\n==)[=]+/g;
        let match = RE_AVERAGE_SECTION.exec(this._spnpResult);
        if (match) {
            str = match[1];
        }

        const RE_DATA = /\n[\s]+[\d]+:\s([\w\d]+)[\s]+([\d\.e\-\+]+)[\s]+([\d\.e\-\+]+)/g;
        
        const RE_AVERAGE_PLACE = /\n[\s]+PLACE[\s]+Pr\[nonempty\][\s]+Av\[tokens\]([\s\w\d:\.\-\+]+)TRANSITION/g;
        match = RE_AVERAGE_PLACE.exec(str);
        let places = {};
        if (match) {
            let placeStr = match[1];
            let placeMatch = RE_DATA.exec(placeStr);
            while (placeMatch != null) {
                places[placeMatch[1]] = { nonempty: placeMatch[2], tokens: placeMatch[3] };
                placeMatch = RE_DATA.exec(placeStr);
            }
        }

        const RE_AVERAGE_TRANSITION = /\n[\s]+TRANSITION[\s]+Pr\[enabled\][\s]+Av\[throughput\]([\s\w\d:\.\-\+]+)\n[==]+/g;
        match = RE_AVERAGE_TRANSITION.exec(str);
        let transitions = {};
        if (match) {
            let transitionStr = match[1];
            let transitionMatch = RE_DATA.exec(transitionStr);
            while (transitionMatch != null) {
                transitions[transitionMatch[1]] = { enabled: transitionMatch[2], throughput: transitionMatch[3] };
                transitionMatch = RE_DATA.exec(transitionStr);
            }
        }

        return { places, transitions };
    }
}


module.exports = SPNPResultParser;