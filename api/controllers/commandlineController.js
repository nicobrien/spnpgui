/**
 * @todo: type checking for the command line arguments
 */


 /**
  * {@see https://www.npmjs.com/package/minimist}
  */
const parseArgs = require('minimist');

/**
 * Aliases maps string names to strings or arrays of string
 * argument names that will be parsed as aliases
 */
const aliases = {
    p: ['port' ],
    ep: ['cspl-port']
};

/**
 * The default values to be used where command line args are not supplied
 */
const defaults = {

    /** The default port that this service will listen on */
    p: 3000,

    /** The default spnp engine port that cspl will be sent to */
    ep: 49161

};

/**
 * The CommandLineController is responsible for parsing
 * command line arguments and making them available in 
 * an easy to consume format for the server.
 */
class CommandLineController {

    /**
     * 
     * @param {Object} argv The commandline agrs in process.argv
     */
    constructor(argv) {
        this._args = parseArgs(
            argv.slice(2),
            {
                alias: aliases,
                default: defaults
            }
        );
    }

    /**
     * @return a port number that this service will
     * listen on, either the cli (preferred)
     * or a default port as described in {@link defaults}
     */
    getPort() {
        return this._args.p;
    }

    /**
     * @return a port number for the cspl service , either 
     * the cli (preferred) or a default port as described 
     * in {@link defaults}
     */
    getCSPLPort() {
        return this._args.ep;
    }
}

module.exports = CommandLineController;
