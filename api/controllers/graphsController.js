/**
 * note: the DTO descriptions for the following handlers use 
 * square bracket notation to indicate optional parameters
 * @example req.body = { required_param: param, ... [optional_param: param, ...] }
 */


 /**
  * A converter that can transform an object representation
  * of the graph to CSPL
  */
const CSPLConverter = require('./csplConverter');

/**
 * A parser that can parse the results of spnp run over
 * cspl for client consumption.
 */
const SPNPResultParser = require('./spnpResultParser');

/**
 * The db actions
 */
let sqliteActions = require ('../database/sqliteActions');

/**
 * Used for promise based calls to spnp engine service
 */
const request = require('request-promise-native');

/**
 * Nicer constants
 */
const HttpStatus = require('http-status-codes');

/**
 * Create a graph in the database and return the id
 * 
 * req.body = { 
 *     [name: <name-for-graph>, model: <graph-model>, ... ]
 * }
 * 
 * @param {Object} req The request object representing the HTTP
 * request for createGraph. The req.body does not require, but
 * can contain the name and model for the graph
 * @param {Object} res Represents the HTTP response that the 
 * app sends when it gets an HTTP request
 */
const createGraph = async (req, res) => {
    try {
        const id = await sqliteActions.insertGraph(req.body);
        const graph = await sqliteActions.selectGraph(id);
        res.status(HttpStatus.OK).send({ id: id, graph: graph, message: 'created graph' });
    } catch (e) {
        console.log(e);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).send('Failed to create graph');
    }
};

/**
 * Save an existing graph in the database
 * 
 * req.body = { 
 *     id: <id>, ... 
 *     [name: <name-for-graph>, model: <graph-model>, ...] 
 * }
 * 
 * @param {Object} req The request object representing the HTTP
 * request for saveGraph. The req.body must contain at a 
 * minimum an id but may also contain the graph name and model
 * @param {Object} res Represents the HTTP response that the 
 * app sends when it gets an HTTP request
 */
const saveGraph = async (req, res) => {
    try {
        let data = req.body;
        await sqliteActions.updateGraph(data);
        res.status(HttpStatus.OK).send({ message: 'saved graph' });
    } catch (e) {
        console.log(e);
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).send('Failed to save graph');
    }
};

/**
 * Retrieve all graphs from the database
 * The response will be an array of graph objects which have the
 * structure:
 * 
 * { 
 *     id: <id>, 
 *     name: <name>, 
 *     model: <model>, 
 *     create_time: <time>, 
 *     update_time: <time>
 * }
 * 
 * @param {Object} req The request object representing the HTTP
 * request for getGraphs.
 * @param {Object} res Represents the HTTP response that the 
 * app sends when it gets an HTTP request
 */
const getGraphs = async (req, res) => {
    try {
        let rows = await sqliteActions.selectGraphs();
        let graphs = rows;
        res.status(HttpStatus.OK).send(graphs);
    } catch (e) {
        console.log('Could not get graphs from db, please check graphs table is created...')
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({});
    }
};

/**
 * Retrieve a graph from the database by id
 * 
 * req.body = { id: <id>, ... }
 * 
 * @param {Object} req The request object representing the HTTP
 * request for getGraphs. Must contain the id of the graph to be
 * fetched
 * @param {Object} res Represents the HTTP response that the 
 * app sends when it gets an HTTP request.
 */
const getGraph = async (req, res) => {
    const id = req.params.id;
    if (!id) {
        res.status(HttpStatus.BAD_REQUEST).send({ message: 'id not provided for graph lookup' });
    }
    try {
        let row = await sqliteActions.selectGraph(id);
        let graph = row;
        res.status(HttpStatus.OK).send(graph);
    } catch (e) {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ message: 'Failed to get graph'});
    }
};


/**
 * Retrieve the CSPL (c-like stochastic petri net
 * language) description for a given graph.
 * 
 * @param {Object} req The request object representing the HTTP
 * request for getCSPL. must contain the nodes of the graph.
 * @param {Object} res Represents the HTTP response that the 
 * app sends when it gets an HTTP request.
 */
const getCSPL = async (req, res) => {
    try {
        const nodes = req.body.nodes;
        if (!nodes) {
            res.status(HttpStatus.BAD_REQUEST).send({ message: 'No nodes supplied. To get CSPL we require the nodes of the graph.' });
        }
        const converter = new CSPLConverter(nodes);
        const cspl = converter.convert();
        const payload = { cspl };
        res.status(HttpStatus.OK).send(payload);
    } catch (e) {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ message: 'Failed to get CSPL'});
    }
};

/**
 * Function constructor for the analyze controller handler
 * 
 * @param {String} csplServiceUri the uri of the spnp-engine service
 * @return {Function} a controller function that can 
 * handle requests to be sent to the spnp-engine service
 */
const analyze = (csplServiceUri) => 
    (req, res) => {
        const converter = new CSPLConverter(req.body.nodes);
        const cspl = converter.convert();
        const payload = { 
            'cspl': cspl
        };
        const options = {
            method: 'POST',
            uri: csplServiceUri,
            body: payload,
            json: true
        };
        request(options)
            .then(result => {
                try {
                    const parser = new SPNPResultParser(result);
                    const analysis = parser.parse();
                    res.send(Object.assign({}, analysis, { cspl }));
                } catch (e) {
                    res.send({ error: e, msg: 'failed to parse results from spnp-engine' });
                }
            })
            .catch(e => {
                console.log('failed to get result');
                res.send({ error: e });
            });
        
};

module.exports = {
    createGraph: createGraph,
    saveGraph: saveGraph,
    getGraphs: getGraphs,
    getGraph: getGraph,
    analyze: analyze,
    getCSPL: getCSPL
};