
/**
 * todo: implement logging as middleware for the db calls in order to simplify this code
 */

const dbPath = './database/spnp_database.db';

/**
 * https://github.com/mapbox/node-sqlite3
 */
let sqlite3 = require('sqlite3').verbose();
let db = new sqlite3.Database(dbPath);

/**
 * Select all graphs from the db
 * 
 * @return A promise wrapping the results of the select query
 */
const selectGraphs = async () => {
    const statement = "SELECT * FROM graphs ORDER BY update_time DESC";
    return new Promise((resolve, reject) => {
        db.all(
            statement, { },
            function callback(error, rows) {
                if (error) { 
                    console.log(`FAILED: ${statement}`); 
                    reject(error); 
                } else {
                    console.log(` ${statement} -- rows: ${rows.length}`);
                    resolve(rows);
                }
            }
        );
    });
};

/**
 * Select a single graph by id from the database
 * 
 * @return A promise wrapping the results of the select query
 */
const selectGraph = async (id) => {
    if (!id) {
        throw 'id was null: Cannot fetch graph without an id';
    }
    const statement = "SELECT * FROM graphs WHERE id = $id";
    return new Promise((resolve, reject) => {
        db.get(
            statement, { $id: id },
            function callback(error, row) {
                if (error) { 
                    console.log(`FAILED: ${statement} -- id: ${id}`); 
                    reject(error); 
                } else {
                    console.log(`${statement} -- id: ${id}`);
                    resolve(row);
                }
            }
        );
    });
};

/**
 * Execute an insert operation to the SQLite db connection
 * for the purposes of creating a graph
 * 
 * @param {Object} graphDTO the DTO that describes the
 * graph object that we wish to persist
 * @return A promise wrapping the id of the inserted row
 */
const insertGraph = async (graphDTO) => {
    const statement = "INSERT INTO graphs (name, model) VALUES ($name, $model)";
    let { name, model } = graphDTO;
    name = name || '';
    model = model || '';
    return new Promise((resolve, reject) => {
        db.run(
            statement, 
            { $name: name, $model: model },
            function callback(error) {
                if (error) { 
                    console.log(`FAILED: ${statement} -- name: ${name}, model: ${model}`); 
                    reject(error); 
                } else {
                    let id = this.lastID;
                    console.log(`${statement} -- name: ${name}, id: ${id}`);
                    resolve(id);
                }
            }
        );
    });
};

/**
 * Execute an update operation to the SQLite db connection
 * for the purposes of updating the state of an existing graph
 * 
 * @param {Object} graphDTO the DTO that describes the
 * graph object that we wish to update. Note that only the name 
 * and the model can be updated by the DTO. The update time is 
 * also updated automatically by the db.
 * @return A promise wrapping the id of the updated row
 */
const updateGraph = async({ id, name, model, cspl }) => {
    console.log(cspl);
    if (!id) {
        throw 'id was null: Cannot update graph without an id';
    }
    const statement = "UPDATE graphs SET name = $name, model = $model WHERE id = $id";
    return new Promise((resolve, reject) => {
        db.run(
            statement, 
            { $name: name, $model: model, $id: id },
            function callback(error) {
                if (error) { 
                    console.log(`FAILED: ${statement} -- name: ${name}, model: ${model}, id: ${id}`); 
                    reject(error); 
                } else {
                    let id = this.lastID;
                    console.log(`${statement} -- name: ${name}, id: ${id}`);
                    resolve(id);
                }
            }
        );
    });
};

module.exports = {
    selectGraphs: selectGraphs,
    selectGraph: selectGraph,
    insertGraph: insertGraph,
    updateGraph: updateGraph
};