------------------------------------------------------
-- NOTE:
-- Once multi-user support is added to the application
-- it should be decided whether to alter this table
-- by adding a foreign key (user_id) or to create a
-- link table with a new users table
------------------------------------------------------


------------------------------------------------------
-- The graphs table stores the users created graphs
------------------------------------------------------
CREATE TABLE `graphs` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `name`          TEXT NOT NULL,
    `model`         BLOB NOT NULL,
    `create_time`   TEXT DEFAULT CURRENT_TIMESTAMP NOT NULL,
    `update_time`   TEXT DEFAULT CURRENT_TIMESTAMP NOT NULL
);


------------------------------------------------------
-- This trigger function is responsible for resetting
-- the update time of the graphs table
------------------------------------------------------
CREATE TRIGGER IF NOT EXISTS on_update_graph_set_update_time 
    AFTER UPDATE 
    ON graphs 
    FOR EACH ROW 
BEGIN
    UPDATE graphs 
    SET update_time = DATETIME('now')
    WHERE id = OLD.id;
END;