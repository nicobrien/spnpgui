
# Setup

Installing on **Ubuntu 16.04** (Note: further instructions for other OSs can be found https://docs.docker.com/engine/installation)

### Requirements

64 bit of
+ Zesty 17.04
+ Xenial 16.04 (LTS)
+ Trusty 14.04 (LTS)

### Installation - Setup

Enure that older versions of docker, docker-engine are uninstalled:

```sh
sudo apt-get remove docker docker-engine docker.io
```

Install dependencies

```sh
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
```

Install Dockers GPG key and verify

```sh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88
```

You should see:

```sh
pub   4096R/0EBFCD88 2017-02-22
      Key fingerprint = 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
uid                  Docker Release (CE deb) <docker@docker.com>
sub   4096R/F273FCD8 2017-02-22
```

Add stable repository to /etc/apt/sources.list.
For 64bit xenial this is:

```sh
deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable
```

This can be done via the command:

```sh
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```
Where `$(lsb_release -cs)` returns the Ubuntu version.

### Installation - Final

Run

```sh
sudo apt-get update
sudo apt-get install docker-ce
apt-cache madison docker-ce
```

The last command lists the versions that are available, pick one (eg:<VERSION> 17.06.0~ce-0~ubuntu ) and install:

```sh
sudo apt-get install docker-ce=<VERSION>
```

### Test

```sh
sudo docker run hello-world
```

### Startup

Use systemd to manage starup service (start docker on system boot). This can be disabled by changing `enable` to `disable`.

```sh
sudo systemctl enable docker
```

