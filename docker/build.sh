# This is to workaround the context being wrong for the build
# ie: we want the docker server to have access to the ../api folder
echo 'Moving Dockerfile'
cp Dockerfile ../api
echo 'Ensuring gulp is available for build'
sudo npm i -g gulp
cd ../website
gulp vendorBundle
gulp bundle
cd ../api
sudo docker build -t spnp-server .
echo 'Removing Dockerfile from /api'
rm Dockerfile
cd ../docker
echo 'Done.'
echo 
echo 'If there were no errors you can run spnp with: '
echo 'sudo docker run -p 49160:3000 spnp-server'
echo
echo 'Helpful commands:'
echo
echo '      sudo docker images                                -show docker images'
echo '      sudo docker exec -it <container-id> /bin/bash     -enter container'
