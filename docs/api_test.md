# Tests Organization

The gulp task pipes all files in the api/tests folder that have an certain extestions to the jasmine test runner.

The matching is: `${testsRootDir}/**/*[tT]est[s].js`

This means that any files in the testsRootDir (currently api/tests) which are js files and end with test, tests, Test, Tests will be assumed to be tests, and executed.
The 'test' ending is to allow js mock files which can store mock data for testing purposes and be required by various test files without being executed by the test runner.


# Running Tests
run tests with `gulp test`

More specifically, ensure that gulp is installed - it is a dev dependency so running the following (assuming that you are in the /api folder) should set everything up:

```sh
npm install
gulp test
```

if gulp is not found this is probably because it is not installed 'globally'. If you have permissions to do so, this can be done by

```sh
npm i -g gulp
```

Otherwise an alternative is to run

```sh
node ./node_modules/gulp/bin/gulp.js test
```

# gulp-jasmine-node

The gulp task uses gulp-jasmine-node, which wraps jasmine-node. 

https://github.com/mhevery/jasmine-node

At the time of writing this uses jasmine 1.3.1 api which can be found:

https://jasmine.github.io/1.3/introduction