# About
The database currently used is sqlite3. 
This is expected to be replaced by some other open source RDBMS storage solution.

# Further Reading
Please see rationale_sqlite.pdf for further discussion about the decision to use sqlite.
