Background
==========

The SPNPGui application that is being developed as a SENG402 year long
project now requires a data storage solution. This solution is to
provide the user with a means of persistenty storing their constructed
stochastic petri nets (SPN)s.

The data that is expected to be persisted by the application includes:

### Graph Models

The graph models will likely be of type string and will include a XML
based description of the renderable graph. This can include information
such as the number and type of nodes in the SPN, the x and y cartesian
co-ordinates of the position of those nodes, a description of the set of
arcs between the nodes and the user data associated with each node.

### Graph Id and Name

It is necessary but insufficient to store the graphs against a named key
only as this would require the names of the graphs to be unique in
either an relational or non relational data storagae system. The
implementation of such a constraint would not be useful for a user and
for this reason each graph will require a unique identifier, likely an
integer.

### User Id

While the current implementation is for a single user only, it is
important to remember that one reason a web based application was chosen
was the ease of deployment over some network. This means that any
decision of data storage should consider the future implementation of
how to identify graphs belonging to a specific user on a network. This
will likely involve, at the least, a user ID, username and user
password.

It is likely that future iterations of the application will have
additional data storage requirements, the result of new features being
added. There are two approaches to mitigate the potential cost of
change.

### Prototype

Firstly, the simplest data storage solution could be chosen to meet the
current requirements. This solution could be considered prototypical and
would be easy and relatively quick to implement and integrate with the
application code. Rather than spending time designing a solution that is
amenable to future changes, a solution could be rapidly deployed with
the understanding that future changes would likely require the current
solution to be abandoned. The rationale behind choosing this approach is
that time would not be wasted preparing for future requirements that may
not occur. Given the time constraints of the 402 project, this solution
is preferable. It is noted that if a prototype based approach is taken,
then it should be possible to migrate data from any chosen solution to
future solutions.

### Future Planning

An alternative approach is to spend further time considering how the
application requirements may change and then take those considerations
into account while attempting to implement a robust data storage
solution that will be more amenable to change.

File
====

Storage on disc is a simple but difficult to manage approach. It is
noted that the use of files to store user data is not a good software
engineering practice. Such a solution will typically not provide file IO
atomicity, consistency, isolation and durability (ACID). For example,
data consistency is difficult due to the problem of maintaining
relations between data sets such as the graph id and corresponding graph
model. Relational databases provide the ACID properties.

Relational Databases
====================

It is noted that relational database management systems (RDBMS) assume
that the user has a well defined data structure. Notably, it "assumes
that the data is dense and is largely uniform. RDBMS builds on a
prerequisite that the properties of the data can be defined up front and
that its interrelationships are well established and systematically
referenced. It also assumes that indexes can be consistently defined on
data sets and that such indexes can be uniformly leveraged for faster
querying." [1].

The data requirements are well defined at this stage of the development
process. Furthermore, the relationships between the data are well
understood: a graph model would have a primary key of graph id, and
later perhaps a foreign key of user id. The primary key would initially
serve as the index for the graph model. The graph model would also have
a name associated with it. All data in a graph model relation would have
non null constraints. The graph name could possibly not be specified by
the user, though in this case it could be programmatically named by the
application as the empty string, or perhaps *unnamed spn*. There is no
requirement to store a graph model for a null graph description, so the
model column could also have a non null constraint. For this reason the
table would not be sparse, rather it would be densely populated.

"RDBMS can certainly deal with some irregularities and lack of structure
but in the context of massive sparse data sets with loosely defined
structures, RDBMS appears a forced fit. With massive data sets the
typical storage mechanisms and access methods also get stretched.
Denormalizing tables, dropping constraints, and relaxing transactional
guarantee can help an RDBMS scale, but after these modifications an
RDBMS starts resembling a NoSQL product." [1].

"Flexibility comes at a price. NoSQL alleviates the problems that RDBMS
imposes and makes it easy to work with large sparse data, but in turn
takes away the power of transactional integrity and flexible indexing
and querying. Ironically, one of the features most missed in NoSQL is
SQL, and product vendors in the space are making all sorts of attempts
to bridge this gap." [1].

It is unlikely that the data storage requirements of the application
will be so large that a RDBMS storage solution will be less viable than
a NoSQL solution. Say the application is run on a university network,
serving a course on stochastic modelling which has 40 students.

Again assume that each student creates 100 SPNs each with a id, name,
username, password, user id and graph model. Reasonable data sizes for
these data values taken from PostgresSQL data types could be:

-   Graph Id, bigint - 8B

-   Graph Name, text - 50 characters @ 1B/char + 1B overhead is 51B

-   Graph Model, text - 1500 characters @ 1B/char + 4B overhead is 1504B

-   User Id, bigint - 8B

-   Password, text - 20 characters @ 1B/char + 1B overhead is 21B

-   User Name, text - 20 characters @ 1B/char + 1B overhead is 21B

This is approximately 1613B per entry. Which equates to

```
40 * 100 * 1613B = 6452000B 
```

or approx 6Mb per course.

Hence, data integrity and the ACID properties that a RDBMS provides are
considered more valuable than the benefits of using a document or key
based storage system for the purposes of the application.

Conclusion
==========

Based on the considerations from sections [background] and [sql], it is
decided that SQLite3 is a good choice as a data storage solution for the
current application.

SQLite3 can be run cross platform. This is important as currently the
only deployment of SPNP that has been successfully achieved is on a
Windows 7 platform, while much of the development is performed on linux.

SQLite3 is a light weight RDBMS solution that stores data in a single
file. It provides the ACID properties. The file size limits are so large
that it is very unlikely that even a significant multiple of the
estimated course consumption, 6MB, will reach it.

It is likely that future implementations would want to migrate to a more
fully featured RDBMS, such as PostgresSQL. It is somewhat trivial to
migrate the above mentioned data from a SQLite3 database to a
PostgresSQL database. This is especially the case as the data types
would not need to be converted.

### Reference:
Tiwari, S. (2011). "NOSQL. What is It and Why Do You Need It," in
*Professional NoSQL.* pp. 5 John Wiley & Sons.
