# Installation instructions for SPNP v6.1.2 on Ubuntu 16.04 x86_64bit

## Contents
1. [Aquire source](#aquire-source)
2. [Required packages](#required-packages)
3. [Pre-Build steps](#pre-build-steps)
4. [Build source](#build-source)
5. [Test build](#test-build)
6. [Troubleshooting](#troubleshooting)

## Aquire source

The source directory structure for SPNP v6.1.2 is as follows:
```sh
|  root
    |  docs
    |  example
    |  include
    |  lib
    |  sim_example
    |  src
    |  temp
    |  test
    Makerun
    Makerun.cygwin
    Makerun.dos
    Makerun.linux
    spnpini.bash
    spnpinit.bat
    spnpinit.cst
    spnpinit.cygwin
```

The folders and files we are primarily concerned with for installation and setup are 
```sh
/src/configure.in
Makerun.linux 
spnpini.bash
```

## Required packages

The following packages will need to be installed if they have not already.

```sh
sudo apt-get install automake gcc-multilib g++-multilib
```

The build step relies on automake, and the gcc-multilib package allows buliding of 32bit binary files on 64bit architecture.


## Pre-Build steps

### Fix configure file

_configure.in_ is now expected to be named _configure.ac_. If aclocal generates a warning or error to this effect, simply renaming the file will allow you to continue:

```sh
mv configure.in configure.ac
```
### Fix Makerun.linux file

_Makerun.linux_ should be changed to ensure that a 32bit binary is compiled. Future work could be done to update SPNP to build as 64bit, however at the moment (with v.6.1.2) attemting to make with the a native 64bit architecture results in a mismatch when running gcc over an spn file with `libspnp6_linux.a`. For example:
```sh
gcc  -g -o cluster.spn cluster.o -L/home/spnp/Documents/SPNP_sources/lib -lspnp6_linux -lm
/usr/bin/ld: skipping incompatible /home/spnp/Documents/SPNP_sources/lib/libspnp6_linux.a when searching for -lspnp6_linux
```

A method to resolve this problem is to adjust the following two lines in _Makerun.linux_ by adding -m32:
```sh
...
CC = gcc -c -g -m32 -DDEBUG_FLAG=0
LD = gcc -g -m32
...
```

## Build Source

If any problems are encountered in the following steps, check the [troubleshooting](#troubleshooting) section before resorting to google or sledgehammers.

In the source folder `/src`, there are the *.c files. The following commands are to be run in order to compile the source:

```sh
cd ./src

aclocal
autoconf
automake -a -c
```

Assuming that the Makerun.linux file has been adjusted as describe [here](#fix-makerunlinux-file), the following should be run

```sh
./configure --host=i686-linux-gnu "CFLAGS=-m32" "CXXFLAGS=-m32" "LDFLAGS=-m32"
```
And then make:

```sh
make
```

Following successful make, the libspnp6.a file should be moved to the /lib folder and renamed so that it is accessible by SPNP, as follows

```sh
cp libspnp6.a ../lib/libspnp6_linux.a
```

Change the path to the root directory in spnpinit.bash, first get the path to the root directory. Navigate to the root directory (may not be called root in your installation) as shown [here](#aquire-source). Then run

```sh
pwd
```

This will return something like `/home/spnp/Documents/SPNP_sources`. This path should be added to spnpinit.bash by changing the SPNP_DIRECTORY environment variable. For example:

```sh
export SPNP_DIRECTORY=/home/spnp/Documents/SPNP_sources
function spnp { make -f $SPNP_DIRECTORY/Makerun.linux SPN=$@; }
export PATH=/usr/bin:$PATH:.
alias spnpclean="rm *.spn *.log *.o *.mc *.prb *.rg"
```

The bash file is then run with source(https://ss64.com/bash/source.html) (alias .) as follows:

```sh
. spnpinit.bash
```

When a script is run using source it runs within the existing shell and variables created by the script (SPNP_DIRECTORY in this case) remain available after the script completes.

## Test build

To test SPNP there are example petri nets in the example folder. cluster.c can be run with:

```sh
cd example
spnp cluster
```
**NOTE:** Each time SPNP is run (from a new terminal) the spnpinit.bash file will need to be re-sourced (as described  above).



## Troubleshooting

### Permission denied when running ./configure
Check permissions on configure:
```sh 
chmod 755 ./configure 
```


### Segmentation fault when running test build

If an segmentation fault occurs when running `spnp cluster` or some other spn, try removing all old output files for the spn and re run:
```sh
rm cluster.log cluster.mc cluster.o cluster.out cluster.prb cluster.rg cluster.spn
spnp cluster
```
Note that `spnpclean` may be used if it has been aliased in spnpinit.bash:

```sh
alias spnpclean="rm *.spn *.log *.o *.mc *.prb *.rg"
```

### Error cluster.o (or other spn object file) not found

If you see: `cluster.o is not found`.

Try running
```sh
gcc cluster.c > cluster.o
spnp cluster
spnpclean
```


### Makerun errors

If you see: 
`make: ~/Makerun.linux: No such file or directory` or `make *** No rule to make target '~/Makerun.linux'`

This could be because the SPNP_DIRECTORY environment variable is incorrect in spnpinit.bash. Double check the correct root directory (absolute not relative) is used in spnpinit.bash