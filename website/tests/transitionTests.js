
import SpnpTransition from '../src/models/transition';
import Mocks from './mocks.js';


describe('SPNP Transitions', function() {

    it('when added should have unique names', function() {
        let p1 = SpnpTransition.insert(Mocks.graph, 0, 0);
        let p2 = SpnpTransition.insert(Mocks.graph, 0, 0);
        expect(p1).not.toEqual(p2);
    });

});
