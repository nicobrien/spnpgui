
import SpnpPlace from '../src/models/place';

var graphMock = {
    elementNameController: {
        i: 0,
        getNextPlaceName: function() {
            let name = 'p' + this.i;
            this.i++;
            return name;
        }
    },
    getDefaultParent: function() { },
    getModel: function() {
        return {
            beginUpdate: function() { },
            endUpdate: function() { }
        };
    },
    // The insert vertex function is mocked drop all data and return only placename
    insertVertex: function(a, b, placeName, c, d, e, f, g) {
        return placeName;
    }
};

describe('SPNP Places', function() {

    it('when added should have unique names', function() {
        var p1 = SpnpPlace.insert(graphMock, 0, 0);
        var p2 = SpnpPlace.insert(graphMock, 0, 0);
        expect(p1).not.toEqual(p2);
    });

});
