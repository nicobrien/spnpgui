/**
 * Mocks a mxGraph function
 */
var graph = {
    elementNameController: {
        i: 0,
        getNextPlaceName: function() {
            let name = 'p' + this.i;
            this.i++;
            return name;
        },
        getNextTransitionName: function() {
            let name = 'p' + this.i;
            this.i++;
            return name;
        }
    },
    getDefaultParent: function() { },
    getModel: function() {
        return {
            beginUpdate: function() { },
            endUpdate: function() { }
        };
    },
    // The insert vertex function is mocked to drop all data and return only placename
    insertVertex: function(a, b, placeName, c, d, e, f, g) {
        return placeName;
    }
};
export default {
    graph: graph
};
