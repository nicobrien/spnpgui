'use strict';

var chalk = require('chalk');
var babelify = require('babelify');
var browserify = require('browserify');
var del = require('del');
var gulp = require('gulp');
var gutil = require('gulp-util');
var jsHint = require('gulp-jshint');
var karma = require('karma').Server;
var reactify = require('reactify');
var sourceMaps = require('gulp-sourcemaps');
var vinylSourecStream = require('vinyl-source-stream');
var watchify = require('watchify');



/**
 * We keep track of the vendor libs so that they can be bundled separately,
 * this allows faster bundling of the client code that changes most often
 *  during development. See also the 'vendorBundle' task
 */
var vendorLibs = ['react', 'react-dom', 'semantic-ui-react'];

/**
 * Specifies a relative path to the api static folder where files are
 * served from
 */
var destinationBuildFolder = '../api/static';

/**
 * Execute all tests once for the client code
 */
gulp.task('test', function (done) {
    karma.start({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, function() {
        done();
    });
});

/**
 * Useful for rapid development, continuously runs tests against the karma 
 * server when changes are detected to the client code
 */
gulp.task('tdd', function (done) {
    karma.start({
        configFile: __dirname + '/karma.conf.js',
        singleRun: false
    }, function() {
        done();
    });
});

/**
 * Lints the spnp client code and outputs results to console.
 */
gulp.task('lint', function() {
    gulp.src(['src/*.js', '!src/mxClient.js'])
        .pipe(jsHint())
        .pipe(jsHint.reporter('default'));
});

/**
 * Bundles the client source code and copies to api static folder to be served
 * Note that this will bundle both the spnp code as well as the vendor bundles 
 * If the vendor dependencies have already been bundled, it is faster to call 
 * the bundle task direct
 */
gulp.task('package', ['clean', 'bundle', 'vendorBundle']);

/**
 * Bundles the client code and api static folder to be served. Does not bundle 
 * vendor code.
 * Waits for the 'copyDeps' task to complete
 */
gulp.task('bundle', ['copyDeps'], function() {
    var browserifyOpt = {
        entries: './src/index.js',
        fullPaths: true
    };
    return browserify(browserifyOpt)
        .external(vendorLibs)
        .transform(babelify)
        .transform(reactify)
        .bundle()
        .on('error', gutil.log)
        .pipe(vinylSourecStream('bundle.js'))
        .pipe(gulp.dest(destinationBuildFolder));
});

/**
 * Bundles the vendor libs and copies to the static api folder
 */
gulp.task('vendorBundle', function() {
    return browserify({ fullPaths: true })
        .require(vendorLibs)
        .bundle()
        .on('error', gutil.log)
        .pipe(vinylSourecStream('vendor-bundle.js'))
        .pipe(gulp.dest(destinationBuildFolder));
});

/**
 * Will continuously bundle when a change is detected to the source code, 
 * this can be useful for rapid development. see 'tdd' task also
 */
gulp.task('watch', function() {
    gulp.watch(['./src/*', './src/**/*'], ['bundle']);
});

/**
 * Remove all bundle files from the destinationBuildFolder
 * @param cb We pass a callback to 'hint' to gulp that this task should not
 * be run asyncronously - ie: it should be run in order so as not to cause 
 * problems by running while bundling.
 */
gulp.task('clean', function(cb) {
    var pathToClean = [
        destinationBuildFolder + '/**', 
        '!' + destinationBuildFolder
    ];
    // force option allows deletion of files outside the current path
    del(pathToClean, { force: true}).then(function(paths) {
        console.log('', paths);
    });
    cb();
});

/**
 * Copies other de
 * We keep the directory structure of website by using { base: '.' }
 * option for gulp.src because:
 *  1. It kees the api static folder tidy
 *  2. mxGraph expects that it can find /<base-dir>/resources/editor.txt 
 *     and /<base-dir>/resources/graph.txt, where <base-dir> is specified by the
 *     global constant mxBasePath = 'src'; declaration in index.html
 */
gulp.task('copyDeps', function() {
    var otherDepenencies = [
        './css/**/*',
        './images/*',
        './src/resources/**/*',
        './src/mxClient.js',
        'index.html',
        'favicon.ico'
    ];
    return gulp.src(otherDepenencies, { base: '.' })
        .pipe(gulp.dest(destinationBuildFolder));
});

// make this better to use - more info
var logError = function(error) {
    if (error.fileName) {
        // regular error
        gutil.log(chalk.red(error.name) 
            + ': ' + chalk.yellow(error.fileName.replace(__dirname + '/src/js/', ''))
            + ': ' + 'Line ' + chalk.magenta(error.lineNumber)
            + ' & ' + 'Column '
            + chalk.magenta(error.columnNumber || error.column)
            + ': ' + chalk.blue(error.description));
    } else {
        // browserify error..
        gutil.log(chalk.red(error.name)
            + ': '
            + chalk.yellow(error.message));
    }
};