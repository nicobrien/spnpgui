/* jshint ignore:start */ // Dont use jshint to parse react jsx


/**
 * We use async/await, which babel-polyfill will transpile in
 * order to have this syntax run. This is because at the time of 
 * development, async/await was not yet in the standard.
 * 
 * READ BEFORE MOVING: this import needs to be the first in the 
 * entry point {@see https://babeljs.io/docs/usage/polyfill/ }
 */
import 'babel-polyfill';


import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import store from './store';
import { init } from './mxGraphInit';

/**
 * The tooltip module requires import of this css
 * once in the project. It is noted that 
 * semantic-react-ui does include a Tooltip
 * component. This is not used because currently
 * it has a non configurable immediate display time
 * which can be frustrating for advanced users.
 * Furthermore, this is currently manually copied
 * by gulp task copyDeps due to a parsing error 
 * during bundle
 */
// import 'react-tippy/dist/tippy.css';

import PageComponent from './view/components/pageComponent';

import OpenController from './controllers/openController';
import NotificationController from './controllers/notificationController';
import CanvasController from './controllers/canvasController';
import { getGraphs } from './actions/graphsListActions';
import { eventsService, NAV_MENU_BUTTON_CLICKED_EVENT } from './controllers/events';

/**
 * Following the creation of the react DOM, we then initialize 
 * the spn graph canvas.
 */
const onLoad = () => {
    const notificationController = new NotificationController();
    const canvas = document.getElementById('spnCanvas');
    const graph = init(canvas);
    const container = document.getElementById('spnContent');
    const canvasController = new CanvasController(graph, container);
    const openController = new OpenController();
    eventsService.publish(NAV_MENU_BUTTON_CLICKED_EVENT, 'home');
    store.dispatch(getGraphs(openController));
};

ReactDOM.render(
    <Provider store={ store }>
        <PageComponent />
    </Provider>,
    document.getElementById('root'),
    onLoad
);