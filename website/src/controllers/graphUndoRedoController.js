

import { 
    eventsService, 
    removeAll, 
    UNDO_GRAPH_EDIT_EVENT,
    REDO_GRAPH_EDIT_EVENT,
    AFTER_GRAPH_LOAD_EVENT,
    SPNP_NOTIFICATION_EVENT 
} from './events';


/**
 * Responsible for handling the undo redo events that may be published
 */
class GraphUndoRedoController {

    /**
     * @param {mxGraph} graph The graph this controller is for
     */
    constructor(graph) {
        this._graph = graph;
        this._undoManager = new mxUndoManager();
        this._eventSubscriptions = [];
        this.init();
    }

    /**
     * Initialize the required handlers
     */
    init() {
        var self = this;
        const listener = (sender, evt) => { self._undoManager.undoableEditHappened(evt.getProperty('edit')); };
        self._graph.getModel().addListener(mxEvent.UNDO, listener);
        self._graph.getView().addListener(mxEvent.UNDO, listener);
        this._eventSubscriptions.push(eventsService.subscribe(UNDO_GRAPH_EDIT_EVENT, this.onUndoGraphEdit.bind(this)));
        this._eventSubscriptions.push(eventsService.subscribe(REDO_GRAPH_EDIT_EVENT, this.onRedoGraphEdit.bind(this)));
        this._eventSubscriptions.push(eventsService.subscribe(AFTER_GRAPH_LOAD_EVENT, this.onGraphLoad.bind(this)));
    }

    /**
     * Tear down init
     */
    destroy() {
        removeAll(this._eventSubscriptions);
        this._eventSubscriptions = [];
    }
}

/**
 * When a new graph is loaded we want to reset the undo stack
 */
GraphUndoRedoController.prototype.onGraphLoad = function() {
    this._undoManager.clear();
};

/**
 * Handler for the undo event
 * 
 * @param {Object} event The event that caused this handler to fire
 */
GraphUndoRedoController.prototype.onUndoGraphEdit = function(event) {
    if (this._undoManager.canUndo()) {
        this._undoManager.undo();
    } else {
        eventsService.publish(SPNP_NOTIFICATION_EVENT, { message: "There are no actions to undo", type: 'info' });
    }
};

/**
 * Handler for the redo event
 * 
 * @param {Object} event The event that caused this handler to fire
 */
GraphUndoRedoController.prototype.onRedoGraphEdit = function(event) {
    if (this._undoManager.canRedo()) {
        // Don't reload page
        event.preventDefault();
        this._undoManager.redo();
    } else {
        eventsService.publish(SPNP_NOTIFICATION_EVENT, { message: "There are no actions to redo", type: 'info' });
    }
};

export default GraphUndoRedoController;