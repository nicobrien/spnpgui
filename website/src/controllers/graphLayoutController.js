
import { 
    eventsService, 
    removeAll, 
    SET_HEIRARCHICAL_LAYOUT_EVENT,
    SET_ORGANIC_LAYOUT_EVENT,
    SET_STACK_LAYOUT_EVENT
} from './events';
import { scrollToMiddle } from '../mxGraphInit';
import Logger from './logger';


/**
 * The {@link GraphLayoutController} is responsible for handling the
 * dynamic layout events that may occur
 */
class GraphLayoutController {

    /**
     * @param {mxGraph} graph The graph this controller is for
     */
    constructor(graph) {
        this._graph = graph;
        this._logger = new Logger({ name: 'GraphLayoutController' });
        this._eventSubscriptions = [];
        this.init();
    }

    init() {
        this._eventSubscriptions.push(eventsService.subscribe(SET_HEIRARCHICAL_LAYOUT_EVENT, this.onHierarchicalLayout.bind(this)));
        this._eventSubscriptions.push(eventsService.subscribe(SET_ORGANIC_LAYOUT_EVENT, this.onOrganicLayout.bind(this)));
        this._eventSubscriptions.push(eventsService.subscribe(SET_STACK_LAYOUT_EVENT, this.onStackLayout.bind(this)));
    }

    /**
     * Tear down init
     */
    destroy() {
        removeAll(this._eventSubscriptions);
        this._eventSubscriptions = [];
    }

    /**
     * Shift all cells in the graph by (x, y) pixels
     * 
     * @param {Number} x The x translation to apply to the cells
     * @param {Number} y The y translation to apply to the cells
     */
    translateAll(x, y) {
        const g = this._graph;
        let cells = g.getChildCells(g.getDefaultParent(), true, true);
        g.getModel().beginUpdate();
        try {
            cells.forEach((c) => {
                c.geometry.translate(x, y);
            });
        } finally {
            g.getModel().endUpdate();
        }
    }

    /**
     * Perform a layout on all cells in the graph
     * 
     * @param {mxLayout} scheme the layout to apply to the graph
     */
    layout({ layout }) {
        const g = this._graph;
        g.getModel().beginUpdate();
        let parent = g.getDefaultParent();
        try {
            layout.execute(parent);
        } catch (e) {
            throw e;
        } finally {
            /* Animation is a bit weird with translate: Default values are 6, 1.5, 20 */
            let morph = new mxMorphing(g, 10, 2.7, 20);
            morph.addListener(mxEvent.DONE, function() {
                g.getModel().endUpdate();
                scrollToMiddle(g);
            });
            morph.startAnimation();
        }
    }
}

/**
 * Event handler for setting the graph to have a heirarchical layout
 */
GraphLayoutController.prototype.onHierarchicalLayout = function() {
    const g = this._graph;
    const layout = new mxHierarchicalLayout(g);
    layout.forceConstant = 40;
    const translate = { x: 400, y: 50 };
    this.layout({ layout });
    this._logger.info(`Heirarchical layout completed`);
};

/**
 * Event handler for setting the graph to have a organic layout
 */
GraphLayoutController.prototype.onOrganicLayout = function() {
    const g = this._graph;
    const layout = new mxFastOrganicLayout(g);
    layout.forceConstant = 80;
    this.layout({ layout });
    this._logger.info(`Organic layout completed`);
};


/**
 * Event handler for setting the graph to have a stack layout
 * @param {Boolean} horizontal True iff the layout should be horizontal,
 * false if it should be vertical
 */
GraphLayoutController.prototype.onStackLayout = function({ horizontal }) {
    const g = this._graph;
    const layout = new mxStackLayout(g);
    layout.horizontal = horizontal || true;
    layout.keepFirstLocation = true;
    layout.spacing = 80;
    this.layout({ layout });
    this._logger.info(`Stack layout completed, horizontal: ${ horizontal }`);
};

export default GraphLayoutController;