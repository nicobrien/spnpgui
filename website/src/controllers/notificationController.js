
import toastr from 'toastr';
import { 
    eventsService, 
    removeAll, 
    SPNP_NOTIFICATION_EVENT,
    SPNP_CLEAR_NOTIFICATION_EVENT,
    NAV_MENU_BUTTON_CLICKED_EVENT
} from './events';


/**
 * Handles user notifications for the app
 * 
 * The NotificationController is expected to be used by publishing 
 * a {@link eventService#SPNP_NOTIFICATION_EVENT}. This will trigger the 
 * {@link onNotification} handler
 */
class NotificationContorller {

    constructor() { 
        this._eventSubscriptions = [];
        this.init();
    }

    init() {
        toastr.options = {
            'closeButton': true,
            'debug': false,
            'newestOnTop': false,
            'progressBar': true,
            'positionClass': 'toast-bottom-right',
            'preventDuplicates': true,
            'onclick': null,
            'showDuration': '800',
            'hideDuration': '1000',
            'timeOut': '5000',
            'extendedTimeOut': '1000',
            'showEasing': 'swing',
            'hideEasing': 'linear',
            'showMethod': 'fadeIn',
            'hideMethod': 'fadeOut'
        };
        eventsService.subscribe(SPNP_NOTIFICATION_EVENT, this.onNotification.bind(this));
        eventsService.subscribe(SPNP_CLEAR_NOTIFICATION_EVENT, this.onClearNotification.bind(this));
        eventsService.subscribe(NAV_MENU_BUTTON_CLICKED_EVENT, this.onNavClick.bind(this));
    }

    /**
     * Tear down init
     */
    destroy() {
        removeAll(this._eventSubscriptions);
        this._eventSubscriptions = [];
    }
}

/**
 * @param {String} message The message to be displayed
 * @param {String} type The type of the toast. One of
 * [success|info|warning|error] default is info
 */
NotificationContorller.prototype.onNotification = function({ message, type }) {
    if (!message) {
        console.warn('NotificationController.onNotification called with no message');
        return;
    }
    const messageType = type || 'info';
    toastr[messageType](message);
};

/**
 * Clear all notifications before displaying an optional notification
 * 
 * @param {String} message The message to be displayed
 * @param {String} type The type of the toast. One of
 * [success|info|warning|error] default is info
 */
NotificationContorller.prototype.onClearNotification = function({ message, type }) {
    toastr.clear();
    if (message) {
        const messageType = type || 'info';
        toastr[messageType](message);
    }
};

/**
 * Clear all notifications if we navigate to the home page
 * 
 * @param {String} selection The menu button clicked
 */
NotificationContorller.prototype.onNavClick = function(selection) {
    if (selection === 'home') {
        toastr.clear();
    }
};

export default NotificationContorller;