
import axios from 'axios';
import Logger from './logger';
import GraphEncoderController from './graphEncoderController';
const HttpStatus = require('http-status-codes');


const ROUTE = '/cspl';

/**
 * This controller is responsible for fetching cspl
 * for a given graph
 */
export default class GetCsplController {

    /**
     * @param {String} graphModel The XML encoded mxGraph to get
     * cspl for
     */
    constructor(graphModel) { 
        this._logger = new Logger({ name: 'GetCsplController' });
        this._graphModel = graphModel;
    }

    /**
     * Requests the cspl for a graph
     * 
     * @return {Promise} returns promise wrapped result
     */
    getCSPL() {
        this._logger.info('Requesting cspl for graph...');
        const encodingController = new GraphEncoderController(this._graphModel);
        const nodes = encodingController.encode();
        let self = this;
        return new Promise((resolve, reject) => {
            axios.post(ROUTE, { nodes })
                .then(response => {
                    if (response.status === HttpStatus.OK) {
                        resolve(response.data);
                        self._logger.info('Received cspl ok.');
                    } else {
                        self._logger.error('Failed to fetch cspl from server. Status: ', response.status);
                        reject(response.status);
                    }
                })
                .catch(error => {
                    self._logger.error('Exception in fetch of getCSPL', error);
                    reject(error);
                });
        });
    }

}

