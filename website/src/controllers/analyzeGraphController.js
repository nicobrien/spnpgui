
import axios from 'axios';
import GraphEncoderController from './graphEncoderController';
import { eventsService, removeAll } from './events';
import Logger from './logger';
const HttpStatus = require('http-status-codes');


const route = '/analyze';

/**
 * This controller is responsible for initiating 
 * analysis of a graph and returning the results
 * when completeted.
 */
class AnalyzeGraphController {

    /**
    * @param {String} graphModel an XML representation 
    * of the mxGraph that is to be analyzed
    */
    constructor(graphModel) { 
        this._graphModel = graphModel;
        this._logger = new Logger({ name: 'AnalyzeGraphController' });
        this._graphEncoderController = new GraphEncoderController(this._graphModel);
    }

    /**
     * Request SPNP analysis for a graph
     * 
     * @return {Promise} returns promise wrapped object of
     * analysis results.
     */
    requestAnalysis() {
        this._logger.info('Requesting analysis of graph from server...');
        let self = this;

        const graphDTO = { 
            nodes: this._graphEncoderController.encode()
        };

        return new Promise((resolve, reject) => {
            axios.post(route, graphDTO)
                .then(response => {
                    if (response.status === HttpStatus.OK) {
                        self._logger.info('Received analysis results from server ok.');
                        resolve(response.data);
                    } else {
                        self._logger.error('Failed to analyze graphs from server. Status: ', response.status);
                        reject(response.status);
                    }
                })
                .catch(error => {
                    self._logger.error('Exception in requestAnalysis() - ', error);
                    reject(error);
                });
        });
    }

}



export default AnalyzeGraphController;