
import axios from 'axios';
import { eventsService, removeAll, SAVE_GRAPH_EVENT } from './events';
import Logger from './logger';

import SpnpTransiton from '../models/transition';
import SpnpPlace from '../models/place';


/**
 * This  controller is responsible for encoding the 
 * XML form of the graph into an object form. 
 * 
 * Information is lost in this process, specifically:
 * all layout and positioning information. That is to
 * say that an encoded graph object is not a complete
 * representation of a mxGraph.
 */
class GraphEncoderController {

    /**
     * @param {String} graphModel an XML representation of the
     * selected mxGraph that is to be analyzed
     */
    constructor(graphModel) {
        this._graphModel = graphModel;
        this._logger = new Logger({ name: 'GraphEncoderController' });
    }

    /**
     * Retrieve the encoded graph object
     */
    encode() {
        let self = this;
        this._logger.fine('Encoding graph...');
        const nodes = this.modelXmlToObj(this._graphModel);
        this._logger.fine(`Encoding complete with ${nodes.places.length} places, ${nodes.transitions.length} transitions and ${nodes.edges.length} edges`);
        return nodes;
    }

    /**
     * @param {String} xml The xml string representation of the mxGraph
     * @return {Object} representation of the xml model, places and transitions
     * to make server side transformation to CSPL easier
     */
    modelXmlToObj(xml) {
        let doc = mxUtils.parseXml(xml);
        let codec = new mxCodec(doc);
        let root = doc.documentElement.firstChild;
        let childArray = Array.prototype.slice.call(root.children);
        let nodes = {
            places: childArray.filter(n => n.tagName === SpnpPlace.PLACE_TAG_NAME).map(p => placeToObject(p)),
            transitions: childArray.filter(n => n.tagName === SpnpTransiton.TRANSITION_TAG_NAME).map(t => transitionToObject(t)),
            edges: childArray.filter(n => n.tagName === 'mxCell' && n.getAttribute('edge') === '1').map(e => edgeToObject(e))
        };
        return nodes;
    }
}

const edgeToObject = (e) => ({
    source: e.getAttribute('source'),
    target: e.getAttribute('target')
});

/**
 * @param {Node} t DocumentElement transition node
 * @return {Object} transition object representation
 */
const transitionToObject = (t) => ({
    id: t.getAttribute('id'),
    name: t.getAttribute('label'),
    distribution: t.getAttribute('distribution'),
    parameter: t.getAttribute('parameter')
});

/**
 * @param {Node} p DocumentElement place node
 * @return {Object} place object representation
 */
const placeToObject = (p) => ({
    id: p.getAttribute('id'),
    name: p.getAttribute('label'),
    tokens: p.getAttribute('tokens')
});

export default GraphEncoderController;