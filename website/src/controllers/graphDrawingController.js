/**
 * @todo {It may be the case in the future (as more drawing tools are added to the graph) that 
 * this controller should be broken into sub controllers (one for place, transition, ...etc).}
 */

import { 
    eventsService,
    removeAll,
    PLACE_NAME_CHANGED_EVENT,
    PLACE_TOKENS_CHANGED_EVENT,
    TRANSITION_NAME_CHANGED_EVENT,
    TRANSITION_PARAMETER_CHANGED_EVENT,
    GRAPH_SELECTION_CHANGED_EVENT
} from './events';
import SpnpPlace from '../models/place';
import SpnpTransition from '../models/transition';
import { singlePlaceSelected } from '../actions/place';
import { singleTransitionSelected } from '../actions/transition';
import store from '../store';


/**
 * The {@link GraphDrawingController} is responsible for managing the
 * drawing events that can occur within the graph.
 */
class GraphDrawingController {

    /**
     * @param {mxGraph} graph The graph this controller is for
     */
    constructor(graph) {
        this._graph = graph;
        this._eventSubscriptions = [];
        this.init();
    }

    init() {
        this._eventSubscriptions.push(eventsService.subscribe(PLACE_NAME_CHANGED_EVENT, this.onPlaceNameChange.bind(this)));
        this._eventSubscriptions.push(eventsService.subscribe(PLACE_TOKENS_CHANGED_EVENT, this.onPlaceTokensChanged.bind(this)));
        this._eventSubscriptions.push(eventsService.subscribe(TRANSITION_NAME_CHANGED_EVENT, this.onTransitionNameChange.bind(this)));
        this._eventSubscriptions.push(eventsService.subscribe(TRANSITION_PARAMETER_CHANGED_EVENT, this.onTransitionParameterChange.bind(this)));
        const selectionModel = this._graph.getSelectionModel();
        selectionModel.addListener(mxEvent.CHANGE, this.onSelectionChange.bind(this));
        selectionModel.addListener(mxEvent.CHANGE, this.onTransitionSelection.bind(this));
        selectionModel.addListener(mxEvent.CHANGE, this.onPlaceSelection.bind(this));
    }

    /**
     * Tear down init
     */
    destroy() {
        removeAll(this._eventSubscriptions);
        this._eventSubscriptions = [];
    }
}

/**
 * Event handler for place selection
 * 
 * @param {Element} sender The selected element
 * @param {Object} event The event that caused this handler
 * to fire
 */
GraphDrawingController.prototype.onPlaceSelection = function(sender, event) {
    const cells = sender.cells;
    if (cells && cells.length === 1) {
        const selected = cells[0];
        if (SpnpPlace.isPlace(selected)) {
            const id = selected.id;
            const name = selected.value.getAttribute('label');
            const tokens = selected.value.getAttribute('tokens');
            store.dispatch(singlePlaceSelected(id, name, tokens));
        }
    }
};

/**
 * Event handler for transition selection
 * 
 * @param {Element} sender The selected element
 * @param {Object} event The event that caused this handler
 * to fire
 */
GraphDrawingController.prototype.onTransitionSelection = function(sender, event) {
    const cells = sender.cells;
    if (cells && cells.length === 1) {
        const selected = cells[0];
        if (SpnpTransition.isTransition(selected)) {
            const id = selected.id;
            const name = selected.value.getAttribute('label');
            const distribution = selected.value.getAttribute('distribution');
            const parameter = selected.value.getAttribute('parameter');
            store.dispatch(singleTransitionSelected(id, name, distribution, parameter));
        }
    }
};

/**
 * Event handler for selection change events in the graph
 * One action we want to take is to update the properties view 
 * which is a react component. For this reason the event is 
 * published to the {@link EventService}
 * 
 * @param {Element} sender The selected element, should be an
 * mxCell eg: transition, place
 * @param {Object} event The event that caused this handler
 * to fire
 */
GraphDrawingController.prototype.onSelectionChange = function(sender, event) {
    let cells = sender.cells;
    eventsService.publish(GRAPH_SELECTION_CHANGED_EVENT, { selected: cells });
};

/**
 * When the place name is changed externally we update the graph view
 * to reflect this.
 * This event is likely publised via the {@link EventService}
 * 
 * @param {String} placeId The id of the place that has been updated
 * @param {String} name The new name of the place
 */
GraphDrawingController.prototype.onPlaceNameChange = function({ placeId, name }) {
    if (this._graph.elementNameController.validatePlaceName(name)) {
        const graphModel = this._graph.getModel();
        const selectedCell = graphModel.getCell(placeId);
        if (selectedCell) {
            const tokens = selectedCell.value.getAttribute('tokens', 0);
            const place = SpnpPlace.create(name, tokens);
            const oldName = selectedCell.value.getAttribute('label');
            applyChange(graphModel,selectedCell, place);
            this._graph.elementNameController.replaceName(oldName ,name);
        }
    }
};

/**
 * When the place has its number of tokens changed externally 
 * we update the graph view to reflect this.
 * 
 * This event is likely publised via the {@link EventService}
 * 
 * @param {String} placeId The id of the place that has been updated
 * @param {String} tokens The new number of tokens for the selected place
 */
GraphDrawingController.prototype.onPlaceTokensChanged = function({ placeId, tokens }) {
    if (this._graph.elementNameController.validatePlaceTokens(tokens)) {
        const graphModel = this._graph.getModel();
        const selectedCell = graphModel.getCell(placeId);
        if (selectedCell) {
            const name = selectedCell.value.getAttribute('label', 'unknown');
            const place = SpnpPlace.create(name, tokens);
            applyChange(graphModel,selectedCell, place);
        }
    }
};

/**
 * When the transition name is changed externally we update the 
 * graph view to reflect this.
 * 
 * This event is likely publised via the {@link EventService}
 * 
 * @param {String} transitionId The id of the transition that has been updated
 * @param {String} name The new name for the currently selected transition
 */
GraphDrawingController.prototype.onTransitionNameChange = function({ transitionId, name }) {
    if (this._graph.elementNameController.validateTransitionName(name)) {
        const graphModel = this._graph.getModel();
        const selectedCell = graphModel.getCell(transitionId);
        if (selectedCell) {
            const distribution = selectedCell.value.getAttribute('distribution');
            const parameter = selectedCell.value.getAttribute('parameter');
            const oldName = selectedCell.value.getAttribute('label');
            const transition = SpnpTransition.createTransition(name, distribution, parameter);
            applyChange(graphModel,selectedCell, transition);
            this._graph.elementNameController.replaceName(oldName ,name);
        }
    }
};

/**
 * When the transition parameter (rate) is changed
 * externally we update the graph view to reflect this.
 * 
 * This event is likely publised via the {@link EventService}
 * 
 * @param {String} transitionId The id of the transition that has been updated
 * @param {String} parameter The new parameter for the currently selected transition
 */
GraphDrawingController.prototype.onTransitionParameterChange = function({ transitionId, parameter }) {
    if (this._graph.elementNameController.validateTransitionParameter(parameter)) {
        const graphModel = this._graph.getModel();
        const selectedCell = graphModel.getCell(transitionId);
        if (selectedCell) {
            const distribution = selectedCell.value.getAttribute('distribution');
            const label = selectedCell.value.getAttribute('label', 'unknown');
            const transition = SpnpTransition.createTransition(label, distribution, parameter);
            applyChange(graphModel,selectedCell, transition);
        }
    }
};

/**
 * Replaces the value (value object) of the selected cell
 * 
 * @param {mxGraphModel} graphModel The underlying model of the graph
 * @param {mxCell} selectedCell The currently selected cell
 * @param {Object} value The new cell value object to replace the old
 */
const applyChange = (graphModel, selectedCell, value) => {
    graphModel.beginUpdate();
    graphModel.setValue(selectedCell, value);
    graphModel.endUpdate();
};

export default GraphDrawingController;