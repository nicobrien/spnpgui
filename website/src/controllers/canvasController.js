
import { scrollToMiddle } from '../mxGraphInit';
import { eventsService, removeAll, NAV_MENU_BUTTON_CLICKED_EVENT } from '../controllers/events';

/**
 * The canvas controller is responsible for altering the visible state of
 * the graph container. This is required as the graph sits outside the 
 * react root element and we need to be able to control the graphs visibility 
 * based on navigation and events that occur within the react elements
 */
export default class CanvasController {

    /**
     * 
     * @param {mxGraph} graph The SPNP mxGraph
     * @param {Element} domElement The dom element that contains the 
     * element which wraps mxGraph. Should be a DIV
     */
    constructor(graph, domElement) {
        this._graph = graph;
        this._eventSubscriptions = [];
        this.domElement = domElement;
        this.init();
    }

    init() {
        this._eventSubscriptions.push(eventsService.subscribe(NAV_MENU_BUTTON_CLICKED_EVENT, this.onNavClick.bind(this)));
    }

    /**
     * Tear down init
     */
    destroy() {
        removeAll(this._eventSubscriptions);
        this._eventSubscriptions = [];
    }
}

/**
 * Of notable interest is the 'edit' menu selection,
 * in this case we show the container(and therefore 
 * the graph), otherwise we should hide the container
 * 
 * @param {String} clickedItem the nav menu item clicked
 */
CanvasController.prototype.onNavClick = function(clickedItem) {
    switch (clickedItem) {
        case 'edit':
            this.showCanvas();
            scrollToMiddle(this._graph);
            break;
        default: 
            this.hideCanvas();
            break;
    }
};

/**
 * Hide the dom element that contains the graph canvas
 */
CanvasController.prototype.hideCanvas = function() {
    this.domElement.style.display = 'none';
};

/**
 * Show the dom element that contains the graph canvas
 */
CanvasController.prototype.showCanvas = function() {
    this.domElement.style.display = 'block';
};
