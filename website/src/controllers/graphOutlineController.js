
import { 
    eventsService, 
    removeAll, 
    AFTER_GRAPH_LOAD_EVENT,
    HIDE_OUTLINE_VIEW_EVENT,
    SHOW_OUTLINE_VIEW_EVENT,
    NAV_MENU_BUTTON_CLICKED_EVENT
} from './events';
import Logger from './logger';


const OUTLINE_VIEW_TITLE = 'Outline';
const DOM_ELEMENT_TO_CREATE_ID = '../images/maximize.gif';
const MAXIMIZE_IMAGE = '../images/minimize.gif';
const MINIMIZE_IMAGE = '../images/minimize.gif';
const NORMALIZE_IMAGE = '../images/normalize.gif';
const CLOSE_IMAGE = '../images/close.gif';
const RESIZE_IMAGE = '../images/resize.gif';


/**
 * The {@link GraphOutlineController} is responsible for controlling the
 * lifecycle management of the outline view
 */
class GraphOutlineController {

    /**
     * @param {mxGraph} graph The graph this controller is for
     * @param {Element} domElement The DOM element where the outline will be
     * constructed
     */
    constructor(graph) {
        this._logger = new Logger({ name: 'GraphOutlineController' });
        this._graph = graph;
        let el = (document.getElementById(DOM_ELEMENT_TO_CREATE_ID));
        this._domElement = el || document.createElement('div');
        this._outline = null;
        mxWindow.prototype.maximizeImage = MAXIMIZE_IMAGE;
        mxWindow.prototype.minimizeImage = MINIMIZE_IMAGE;
        mxWindow.prototype.normalizeImage = NORMALIZE_IMAGE;
        mxWindow.prototype.resizeImage = RESIZE_IMAGE;
        mxWindow.prototype.closeImage = CLOSE_IMAGE;
        this._window = new mxWindow(
            OUTLINE_VIEW_TITLE, 
            this._domElement, 100, 100, 200, 200, true, true
        );
        this._window.setClosable(false);
        this._window.destroyOnClose = false;
        this._eventSubscriptions = [];
        this._wasShowing = false;
        this.init();
    }

    init() {
        let self = this;
        self._eventSubscriptions.push(eventsService.subscribe(AFTER_GRAPH_LOAD_EVENT, this.onAfterLoadGraph.bind(this)));
        self._eventSubscriptions.push(eventsService.subscribe(SHOW_OUTLINE_VIEW_EVENT, this.onShowOutlineView.bind(this)));
        self._eventSubscriptions.push(eventsService.subscribe(HIDE_OUTLINE_VIEW_EVENT, this.onHideOutlineView.bind(this)));
        self._eventSubscriptions.push(eventsService.subscribe(NAV_MENU_BUTTON_CLICKED_EVENT, this.onNavMenuButtonClicked.bind(this)));
        self._window.addListener(mxEvent.MOVE, mxUtils.bind(this._window, ensureWindowConstraints));
        mxEvent.addListener(window, 'resize', mxUtils.bind(this._window, ensureWindowConstraints));
    }

    /**
     * Tear down init
     */
    destroy() {
        removeAll(this._eventSubscriptions);
        this._eventSubscriptions = [];
    }
}

/**
 * Event handler to trigger after a graph is loaded
 */
GraphOutlineController.prototype.onAfterLoadGraph = function() {
    if (this._outline) {
        this._outline.destroy();
    }
    this._outline = new mxOutline(this._graph, this._domElement);
    this._logger.info('Created new outline view');
    if (this._wasShowing) {
        this._window.show();
    }
};


/**
 * Event handler to show the outline view
 */
GraphOutlineController.prototype.onShowOutlineView = function() {
    this._window.show();
    this._wasShowing = true;
    this._logger.fine('Outline window shown');
};


/**
 * Event handler to hide the outline view
 */
GraphOutlineController.prototype.onHideOutlineView = function() {
    this._window.hide();
    this._wasShowing = false;
    this._logger.fine('Outline window hidden');
};


/**
 * When the nav menu button is clicked we want to hide the outline if the
 * selected view was not edit, otherwise we set the visibility to the previous 
 * visibility of the outline view
 */
GraphOutlineController.prototype.onNavMenuButtonClicked = function(selected) {
    if (this._window) {
        if (selected !== 'edit') {
            this._window.hide();
        } else {
            this._window.setVisible(this._wasShowing);
        }
    } 
};

/**
 * A function that may be bound to events in order to enusure that the 
 * outline window does not go out of the document boundaries.
 * 
 * NOTE: The context (this) of this function should be bound to the window 
 * (mxWindow) whose x/y co-ords are to be checked
 */
const ensureWindowConstraints = function() {
    let iw = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    let ih = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

    const topBarHeight = 65;
    let x = Math.max(0, this.getX());
    let y = Math.max(topBarHeight, this.getY());
    this.setLocation(x, y);
    
    if (x + this.table.clientWidth > iw) {
        x = Math.max(0, iw - this.table.clientWidth);
    }

    if (y + this.table.clientHeight > ih) {
        y = Math.max(topBarHeight, ih - this.table.clientHeight);
    }

    if (this.getX() != x || this.getY() != y) {
        this.setLocation(x, y);
    }
};

export default GraphOutlineController;