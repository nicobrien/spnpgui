
import axios from 'axios';
import { eventsService, removeAll, NAV_MENU_BUTTON_CLICKED_EVENT } from './events';
import { getGraphs } from '../actions/graphsListActions';
import store from '../store';
import Logger from './logger';
const HttpStatus = require('http-status-codes');


const route = '/graphs';

/**
 * This controller is responsible for fetching graphs from the server.
 */
export default class OpenController {

    constructor() { 
        this._logger = new Logger();
        this._eventSubscriptions = [];
        this.init();
    }

    init() {
        this._eventSubscriptions.push(eventsService.subscribe(NAV_MENU_BUTTON_CLICKED_EVENT, this.onNavMenuButtonClicked.bind(this)));
    }

    /**
     * Requests all graphs from the server
     * 
     * @return {Promise} returns promise wrapped array of graph
     * objects (or an empty array if there are no graphs)
     */
    getGraphs() {
        let self = this;
        return new Promise((resolve, reject) =>{
            axios.get(route)
                .then(response => {
                    if (response.status === HttpStatus.OK) {
                        self._logger.info('Received all graphs from server ok.');
                        resolve(response);
                    } else {
                        self._logger.error('Failed to get graphs from server. Status: ', response.status);
                        reject(response.status);
                    }
                })
                .catch(error => {
                    self._logger.error('Exception in getGraphs() - ', error);
                    reject(error);
                });
        });
    }

        /**
     * Tear down init
     */
    destroy() {
        removeAll(this._eventSubscriptions);
        this._eventSubscriptions = [];
    }
}


/** */
OpenController.prototype.onNavMenuButtonClicked = function(selected) {
    if (selected === 'home') {
        store.dispatch(getGraphs(this));
    }
};
