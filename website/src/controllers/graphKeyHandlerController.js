/**
 * @todo destroy function needs to be added to tear down init on destruction of a graph
 */

import { 
    eventsService, 
    removeAll, 
    REDO_GRAPH_EDIT_EVENT, 
    UNDO_GRAPH_EDIT_EVENT ,
    SAVE_GRAPH_EVENT
} from './events';

const KEY_CODE_Z = 90;
const KEY_CODE_R = 82;
const KEY_CODE_DELETE = 46;
const KEY_CODE_S = 83;


/**
 * The {@link KeyHandlerController} is responsible for handling 
 * keyed event interactions with the graph (such as ctrl-z: undo)
 */
class GraphKeyHandlerController {

    /**
     * @param {mxGraph} graph The graph this controller is for
     */
    constructor(graph) {
        this._graph = graph;
        this._eventSubscriptions = [];
        this.init();
    }

    /**
     * Initialize the required handlers
     */
    init() {
        var self = this;
        const keyHandler = new mxKeyHandler(self._graph);
        keyHandler.bindKey(KEY_CODE_DELETE, self.onDeleteKeyPressed.bind(self));
        keyHandler.bindControlKey(KEY_CODE_Z, self.onCtrlZPressed.bind(self));
        keyHandler.bindControlKey(KEY_CODE_R, self.onCtrlRPressed.bind(self));
        keyHandler.bindControlKey(KEY_CODE_S, self.onCtrlSPressed.bind(self));
        window.addEventListener('keydown', this.onKeyDown.bind(this));
        window.addEventListener('keyup', this.onKeyUp.bind(this));
    }

    /**
     * Tear down init
     */
    destroy() {
        removeAll(this._eventSubscriptions);
        this._eventSubscriptions = [];
    }
}

/**
 * @param {Object} event The event that caused this handler to fire
 */
GraphKeyHandlerController.prototype.onKeyDown = function(event) {
    // on ctrl key pressed we want to enable panning of the spnp drawing area
    if (event.ctrlKey) {
        this._graph.panningHandler.ignoreCell = true;
        this._graph.setPanning(true);
    }
};

/**
 * @param {Object} event The event that caused this handler to fire
 */
GraphKeyHandlerController.prototype.onKeyUp = function(event) {
    if (!event.ctrlKey) {
        this._graph.panningHandler.ignoreCell = false;
        this._graph.setPanning(false);
    }
};

/**
 * on delete key pressed, removed selected cells. We also 
 * delete connected edges
 * 
 * @param {Object} event The event that caused this handler to fire
 */
GraphKeyHandlerController.prototype.onDeleteKeyPressed = function(event) {
    const includeEdges = true;
    if (event && this._graph.isEnabled()) {
        let cells = this._graph.getSelectionCells();
        if (cells && cells.length > 0) {
            this._graph.removeCells(cells, includeEdges);
        }
    }
};

/**
 * On ctrl-z we invoke the undoManager undo if there is 
 * an action that can be undone
 * 
 * @param {Object} event The event that caused this handler to fire
 */
GraphKeyHandlerController.prototype.onCtrlZPressed = function(event) {
    eventsService.publish(UNDO_GRAPH_EDIT_EVENT, event);
};


/**
 * On ctrl-s we invoke the save action
 * 
 * @param {Object} event The event that caused this handler to fire
 */
GraphKeyHandlerController.prototype.onCtrlSPressed = function(event) {
    eventsService.publish(SAVE_GRAPH_EVENT, { id: this._graph.spnpID, name: this._graph.spnpName });
};



/**
 * On ctrl-r we invoke the undoManager redo if there is 
 * an action that can be undone
 * Of note: some browsers use ctrl-r to reload page, we
 * prevent this behaviour
 * 
 * @param {Object} event The event that caused this handler to fire
 */
GraphKeyHandlerController.prototype.onCtrlRPressed = function(event) {
    eventsService.publish(REDO_GRAPH_EDIT_EVENT, event);
};

export default GraphKeyHandlerController;
