
/**
 * @todo: check browser support of nice formatting and conditionally render the better looking logging
 * @todo: implement a log-level, perhaps as a param but more likely sent from the server, to restrict certian log levels from being output to console
 * @todo: allow reporting of logging to server
 */

export const LOG_LEVEL = {
    FINE: true,
    LOG: true,
    INFO: true,
    WARN: true,
    ERROR: true
};

const KEYWORD_STYLE ='font-weight: 800; text-transform: uppercase; font-family: courier;';
const INFO_STYLE ='color: hsl(120, 100%, 60%); background-color: hsl(120, 100%, 25%);';
const WARN_STYLE ='color: hsl(39, 100%, 85%); background-color: hsl(39, 100%, 50%);';
const ERROR_STLYE ='color: hsl(0, 100%, 90%); background-color: hsl(0, 100%, 50%);';
const FINE_STYLE ='color: hsl(300, 100%, 85%); background-color: hsl(300, 100%, 25%);';



 /**
 * Nicer console logging
 */
class Logger {

    /**
     * The logger constructor takes a config object that includes
     * the following configurable properites
     * @param {Array} keywords Array of keywords to be highlighted 
     * whenever the logger instance is used
     */
    constructor(config) {
        this._contextName = (config && config.name) || [];
        this._logger = (console && console.log) || function() { };
    }

    /**
     * A standard log
     * @param {String} message A message to be logged
     * @param {Array} params An array of params to be logged
     */
    log(message, ...params) {
        if (LOG_LEVEL.LOG) {
            this._logger(message, ...params);
        }
    }

    /**
     * More important info to be logged
     * @param {String} message A message to be logged
     * @param {Array} params 1, ..., n params to be logged
     */
    info(message, ...params) {
        if (LOG_LEVEL.INFO) {
            this._logger('%c Info ', INFO_STYLE, message, ...params);
        }
    }

    /**
     * Log a warning
     * @param {String} message A message to be logged
     * @param {Array} params 1, ..., n params to be logged
     */
    warn(message, ...params) {
        if (LOG_LEVEL.WARN) {
            this._logger('%c Warning ', WARN_STYLE, message, ...params);
        }
    }

    /**
     * More important info to be logged
     * @param {String} message A message to be logged
     * @param {Array} params 1, ..., n params to be logged
     */
    error(message, ...params) {
        if (LOG_LEVEL.ERROR) {
            this._logger('%c Error ', ERROR_STLYE, message, ...params);
        }
    }

    /**
     * Log an event {@link events#eventService }
     * @param {String} message A message to be logged
     * @param {Array} params 1, ..., n params to be logged
     */
    fine(message, ...params) {
        if (LOG_LEVEL.FINE) {
            let contextWord = ` %c ${this._contextName}` || '';
            let contextStyle = this._contextName ? KEYWORD_STYLE : '';
            this._logger('%c Fine ' + contextWord, FINE_STYLE, contextStyle, message, ...params);
        }
    }
}

export default Logger;