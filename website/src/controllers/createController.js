
import axios from 'axios';
import Logger from './logger';

const HttpStatus = require('http-status-codes');
const route = '/createGraph';

/**
 * This controller is reponsible for managing the 
 * creation of new graphs. At the moment this does not require
 * much. We still follow the controller pattern, see for example
 * {@link SaveController}
 */
export default class CreateController {

    constructor() { 
        this._logger = new Logger();
    }

    /**
     * Create a new graph.
     * 
     * Graphs are created in the db before editing begins so 
     * that the client can know the id for subsequent saving.
     * 
     * @return Promise that wraps the id of the newly created graph
     */
    create() {
        let self = this;
        return new Promise((resolve, reject) => {
            axios.post(route)
                .then(response => {
                    if (response.status === HttpStatus.OK) {
                        self._logger.info('Created graph ok.');
                        resolve(response.data);
                    } else {
                        self._logger.error('Failed to create graph. Status: ', response.status);
                        reject(response.status);
                    }
                })
                .catch(error => {
                    self._logger.error('Exception in fetch of saveGraph', error);
                    reject(error);
                });
        });
    }
}