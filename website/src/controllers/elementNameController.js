/**
 * @todo: the validation in this controller uses the notification controller to 
 * notify the user of incorrect input and corrective action that can be taken
 * to fix it. 
 * 
 * This is a bit intrusive. It was done due to time constraints and should be 
 * refactored / changed to priovied incorrect input feedback on the input element
 * without a popup.
 */


import SpnpPlace from '../models/place';
import SpnpTransition from '../models/transition';
import Logger from './logger';
import { 
    eventsService, 
    SPNP_NOTIFICATION_EVENT,
    SPNP_CLEAR_NOTIFICATION_EVENT
} from './events';

/**
 * The default prefix to give to newly inserted 
 * places. These are followed by a positive 
 * integer to maintain uniqueness of place names
 */
const DEFAULT_PLACE_PREFIX = 'p';

/**
 * A regular expression describing what is considered
 * a legal place name. This is dictated by the SPNP
 * engine.
 */
const RE_LEGAL_PLACE_NAME = /^[a-zA-Z][a-zA-Z0-9_]{0,19}$/;

/**
 * The maximum number of characters that can be used 
 * to name a place. This definition can be found in
 * the SPNP engine c header file.
 */
const MAX_PLACE_NAME_LENGTH = 20;

/**
 * The default prefix to give to newly inserted transitions. These 
 * are followed by a positive integer to maintain uniqueness of
 * transition names
 */
const DEFAULT_TRANSITION_PREFIX = 't';

/**
 * A regular expression describing what is considered
 * a legal transition name. This is dictated by the SPNP
 * engine.
 */
const RE_LEGAL_TRANSITION_NAME = /^[a-zA-Z][a-zA-Z0-9_]{0,19}$/;

/**
 * The maximum number of characters that can be used 
 * to name a transition.
 */
const MAX_TRANSITION_NAME_LENGTH = 20;


class ElementNameController {

    /**
     * 
     * @param {mxGraph} graph The mxGraph that this controller 
     * is to assign and validate names to elements for
     */
    constructor(graph) {
        this._graph = graph;
        this._logger = new Logger({ name: 'ElementNameStore' });

        /**
         * We maintain a list of assigned names (for 
         * places, transitions, ...) because each 
         * element that is sent to the SPNP engine 
         * must have a unique name when analysis is 
         * performed for the containing net.
         * 
         * Name collisions will be treated as an error
         *  by the SPNP engine.
         */
        this._nameStore = [];

        /**
         * Maintain the next place number for this graph
         */
        this._nextPlaceNumber = 0;
        

        /**
         * Maintain the next transition number for this graph
         */
        this._nextTransitionNumber = 0;

        this.init();
    }

    init() {
        let cells = this._graph.getChildCells(this._graph.getDefaultParent(), true, true);
        this._nameStore = cells
            .filter(c => SpnpPlace.isPlace(c) || SpnpTransition.isTransition(c))
            .filter(c => c.value && c.value.getAttribute('label'))
            .map(c => c.value.getAttribute('label'));
    }

    /**
     * Provides a method to get a unique place name
     * automatically. This is useful for automatic
     * insertion of places into the graph.
     * 
     * @return {String} A unique name for a place 
     * (for the loaded petri net)
     */
    getNextPlaceName() {
        let nextPlaceName = DEFAULT_PLACE_PREFIX + this._nextPlaceNumber;
        while (this.isNotUnique(nextPlaceName)) {
            this._nextPlaceNumber++;
            nextPlaceName = DEFAULT_PLACE_PREFIX + this._nextPlaceNumber;
        }
        this._nameStore.push(nextPlaceName);
        return nextPlaceName;
    }

    getNextTransitionName() {
        let nextTransitionName = DEFAULT_TRANSITION_PREFIX + this._nextTransitionNumber;
        while (this.isNotUnique(nextTransitionName)) {
            this._nextTransitionNumber++;
            nextTransitionName = DEFAULT_TRANSITION_PREFIX + this._nextTransitionNumber;
        }
        this._nameStore.push(nextTransitionName);
        return nextTransitionName;
    }

    /**
     * Replace a name in the store
     * 
     * @param {String} oldName The name to remove from the store
     * @param {String} newName The name to add to the store
     */
    replaceName(oldName, newName) {
        const index = this._nameStore.indexOf(oldName);
        if (index !== -1) {
            this._nameStore.splice(index, 1, newName);
        } else {
            this._logger.error(`Could not replace name in store as the oldname (${oldName}) was not found`, this._nameStore);
        }
    }

    /**
     * In general do not use this method, instead use
     * tryAddName() to avoid an exception being thrown
     * if the name is not unique.
     * 
     * @param {String} name The name to add to the name
     * store
     */
    consumeName(name) {
        if (this.isNotUnique(name)) {
            this._logger.error('An attemp was made to add a non unique name to the element name store. This was refused.');
            throw '';
        }
        this._nameStore.push(name);
    }

    /**
     * Check for name collision in the store.
     * 
     * @param {String} name The name to check the name
     * store for a collision.
     * @return {Boolean} true if and only if the name
     * parameter is not in the store, otherwise false
     */
    isNotUnique(name) {
        return this._nameStore.some(n => n === name);
    }

    /**
     * This function can be used to notify the user 
     * of incorrect entry. Intended for 'final' 
     * validation, most likely on blur of input entry.
     * 
     * @param {String} name The name of the place to validate
     */
    validatePlaceName(name) {
        if (!name || name === '') {
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, { message: `A place name can not be empty`, type: 'info' });
            return false;
        }
        if (this.isNotUnique(name)) {
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, { message: `${name} is not a unique place name, this could cause problems during analysis`, type: 'warning' });
            return false;
        } 
        if (name.length > MAX_PLACE_NAME_LENGTH) {
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, { message: `The maximum name length is ${MAX_PLACE_NAME_LENGTH}`, type: 'info' });
            return false;
        }
        if (!RE_LEGAL_PLACE_NAME.test(name)) {
            const lastChar = name && name[name.length - 1];
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, { message: `Invalid input for a place name. Input should look like: ${RE_LEGAL_PLACE_NAME}`, type: 'info' });
            return false;
        }
        return true;
    }

    /**
     * This function can be used to notify the user 
     * of incorrect token number input for a place
     * 
     * @param {String} numberOfTokens The number of 
     * tokens as a string
     */
    validatePlaceTokens(numberOfTokens) {
        // bit outside the madate of this class :(
        if (!parseInt(numberOfTokens)) {
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, { message: `The tokens for a place must an integer`, type: 'warning' });
            return false;
        }
        if (numberOfTokens <= 0) {
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, { message: `The number of tokens must be greater than or equal to 0`, type: 'info' });
            return false;
        }
        if (numberOfTokens > 100) {
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, { message: `Due to engine limitations the number of tokens in a place can not exceed 100`, type: 'info' });
            return false;
        }
        return true;
    }

    /**
     * This function can be used to notify the user 
     * of incorrect transition name entry. Intended for 
     * 'final' validation, most likely on blur of input entry.
     * 
     * @param {String} name The name of the place to validate
     */
    validateTransitionName(name) {
        if (!name || name === '') {
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, {message: `A transition name can not be empty`, type: 'info' });
            return false;
        }
        if (this.isNotUnique(name)) {
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, { message: `${name} is not a unique name, this could cause problems during analysis`, type: 'warning' });
            return false;
        } 
        if (name.length > MAX_TRANSITION_NAME_LENGTH) {
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, { message: `The maximum name length is ${MAX_PLACE_NAME_LENGTH}`, type: 'info' });
            return false;
        }
        if (!RE_LEGAL_TRANSITION_NAME.test(name)) {
            const lastChar = name && name[name.length - 1];
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, { message: `Invalid input for a transition name. Input should look like: ${RE_LEGAL_TRANSITION_NAME}`, type: 'info' });
            return false;
        }
        return true;
    }

    validateTransitionParameter(parameter) {
        // bit outside the madate of this class :(
        if (!parseFloat(parameter) || !parseInt(parameter)) {
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, { message: `A transition param should be a number greater than 0`, type: 'warning' });
            return false;
        }
        if (parameter <= 0) {
            eventsService.publish(SPNP_CLEAR_NOTIFICATION_EVENT, { message: `A transition param should be greater than 0`, type: 'info' });
            return false;
        }
        return true;
    }
}

export default ElementNameController;