
import SpnpTransition from '../models/transition';
import SpnpPlace from '../models/place';
import { eventsService, removeAll, CANVAS_DRAW_ITEM_SELECTED_EVENT } from './events';


const DEFAULT_SELECTED_ITEM = 'arc';

/**
 * The GraphToolbarController is responsible for handling interactions
 * between the spn graph and the toolbar. For example, selection of
 * a drawing tool in the toolbar should then result in subsequent 
 * placement actions in the graph inserting the currently selected 
 * tool object.
 */
class GraphToolbarController {

    /**
     * @param {mxGraph} graph The graph this controller is for
     */
    constructor(graph) {
        this._graph = graph;
        this._currentSelectedItem = DEFAULT_SELECTED_ITEM;
        this._eventSubscriptions = [];
        this.init();
    }

    /**
     * Initialize the required handlers to ensure that the
     * toolbar and graph can interact.
     */
    init() {
        this._eventSubscriptions.push(eventsService.subscribe(CANVAS_DRAW_ITEM_SELECTED_EVENT, this.onDrawToolSelected.bind(this)));
        this._graph.addListener(mxEvent.CLICK, this.onGraphClick.bind(this));
    }

    /**
     * Tear down init
     */
    destroy() {
        removeAll(this._eventSubscriptions);
        this._eventSubscriptions = [];
    }

    /**
     * Event handler to insert the 
     * {@link GraphToolbarController#_currentlySelectedItem} 
     * item into the graph at the event position
     * 
     * @param {Number} x The raw event x click position
     * @param {Number} y The raw event y click position
     */
    insertCurrentItem(x, y) {
        const g = this._graph;
        let adjustedX = x - g.container.scrollLeft;
        let adjustedY = y - g.container.scrollTop;
        switch (this._currentSelectedItem) {
            case 'place':
                SpnpPlace.insert(g, x, y);
                break;
            case 'transition':
                SpnpTransition.insert(g, x, y);
                break;
            default:
                break;
        }
    }
}

/**
 * Event handler to set the currently selected item
 */
GraphToolbarController.prototype.onDrawToolSelected = function(params) {
    this._currentSelectedItem = params;
};

/**
 * Event handler for click on graph
 * @param {Object} element The source element of the event, could be
 * a {@link SpnpPlace} or {@link SpnpTransition}
 * @param {Object} event The click event that triggered this handler
 */
GraphToolbarController.prototype.onGraphClick = function(element, event) {
    const g = this._graph;
    if (!event.consumed) {
        let pageOffsetX = g.getPagePadding().x + Math.abs(g.getPageLayout().x) * g.getPageSize().width;
        let pageOffsetY = g.getPagePadding().y + Math.abs(g.getPageLayout().y) * g.getPageSize().height;
        let x = event.getProperty('event').offsetX - pageOffsetX;
        let y = event.getProperty('event').offsetY - pageOffsetY;
        this.insertCurrentItem(x, y);
    }
};

export default GraphToolbarController;