
import axios from 'axios';
import { 
    eventsService, 
    removeAll, 
    SAVE_GRAPH_EVENT,
    SPNP_NOTIFICATION_EVENT
} from './events';
import GraphEncoderController from './graphEncoderController';
import SpnpTransiton from '../models/transition';
const HttpStatus = require('http-status-codes');
import SpnpPlace from '../models/place';
import Logger from './logger';

const route = '/saveGraph/';


/**
 * This  controller is responsible for sending 
 * graphs to the server for saving
 */
class SaveController {

    /**
     * @param {mxGraph} graph The graph this controller is for
     */
    constructor(graph) {
        this._graph = graph;
        this._logger = new Logger();
        this._eventSubscriptions = [];
        this.init();
    }

    /** 
     * Bind the required event handlers
     */
    init() {
        this._eventSubscriptions.push(eventsService.subscribe(SAVE_GRAPH_EVENT, this.onSaveGraph.bind(this)));
    }

    /**
     * Tear down init
     */
    destroy() {
        removeAll(this._eventSubscriptions);
        this._eventSubscriptions = [];
    }

    /**
     * Save this graph. the model is fetched, not passed in
     * 
     * @param {Number} id The id of the current graph
     * @param {String} name The name of the current graph
     */
    save({ id, name }) {
        let self = this;
        if (!id) {
            throw 'id was null: cannot save a graph without an id';
        }
        const encoder = new mxCodec();
        const result = encoder.encode(this._graph.getModel());
        const xml = mxUtils.getXml(result);
        const encodingController = new GraphEncoderController(xml);
        const nodes = encodingController.encode();
        const graphDTO = { 
            id: id, 
            name: name || '', 
            model: xml,
            nodes: nodes
        };
    
        return new Promise((resolve, reject) => {
            axios.post(route + id, graphDTO)
                .then(response => {
                    if (response.status === HttpStatus.OK) {
                        self._logger.info('Saved graph ok.');
                        eventsService.publish(SPNP_NOTIFICATION_EVENT, { message: "Saved graph OK.", type: 'success' });
                        resolve(response);
                    } else {
                        self._logger.error(`Failed to save graph with id ${id}. Status: ${response.status}`);
                        eventsService.publish(SPNP_NOTIFICATION_EVENT, { message: "Something went wrong and the graph was not saved. Please check the console for more information.", type: 'warning' });
                        reject(response.status);
                    }
                })
                .catch(error => {
                    self._logger.error('Exception in fetch of saveGraph', error);
                    reject(error);
                });
        });
    }
}

/**
 * The event handler for the save graph event
 * 
 * @param {Number} id The id of the current graph
 * @param {String} name The name of the current graph
 */
SaveController.prototype.onSaveGraph = function({ id, name }) {
    return this.save({ id, name });
};

export default SaveController;