
import { eventsService, removeAll, SPNP_NOTIFICATION_EVENT } from './events';
import SaveController from './saveController';
import SpnpTransition from '../models/transition';
import SpnpPlace from '../models/place';

/**
 * A controller that knows how to initialize a SPNP specific graph
 */
export default class GraphConfigController {

    /**
     * @param {mxGraph} graph The mxGraph object to configure
     */
    constructor(graph) {
        this._graph = graph;
        this._eventSubscriptions = [];
        this.init();
    }

    /**
     * Apply configurations to the graph this controller is responsible for
     */
    init() {
        this.configureGraphProperties(this._graph);
        this.configureGraphFunctionality(this._graph);
    }

    /**
     * Tear down init
     */
    destroy() {
        removeAll(this._eventSubscriptions);
        this._eventSubscriptions = [];
    }
}


GraphConfigController.prototype.configureGraphProperties = function() {

    /**
     * Specifies the return value for isHtmlLabel which returns 
     * true if the label must be rendered as HTML markup
     */
    this._graph.htmlLabels = true;

    /**
     *  Option to display background page for graph
     */
    // this._graph.pageVisible = true;

    /**
     * Specifies if autoSize style should be applied when cells are added.
     * Default is false.
     */
    this._graph.autoSizeCellsOnAdd = true;

    /**
     * Disable folding icon for nested cells
     */
    this._graph.isCellFoldable = cell => false;

    /** 
     * Specifies that no edges are allowed which do not have a source and/or 
     * terminal node
     */
    this._graph.setAllowDanglingEdges(false);

    /**
     * Specifies that edges should be disconnected from their terminals when they are moved.
     * This in conjunction with setAllowDanglingEdges ensures that we have no orphaned 
     * edges in the graph
     */
    this._graph.setDisconnectOnMove(false);

    /**
     * Specifies if the graph should allow new connections. 
     * This implementation updates mxConnectionHandler (enabled in <connectionHandler>)
     */
    this._graph.setConnectable(true);

    /**
     * Specifies the size for "tiles" to be used for a graph with
     * scrollbars but no visible background page. A good value is large
     * enough to reduce the number of repaints that is caused for auto-
     * translation (which depends on this value) and small enough to give
     * a small empty buffer around the graph. Default is 400x400.
     */
    this._graph.scrollTileSize = new mxRectangle(0, 0, 400, 400);

};


GraphConfigController.prototype.configureGraphFunctionality = function() {

    const self = this;

    /**
     * When the savegraph event is published, the saveGraphController
     * should take care of than event
     */
    new SaveController(this._graph);

    /*
     * Enables rubberband selection
     */
    new mxRubberband(this._graph);

    /**
     *  Add stylesheets for rubberband selection, etc
     */
    mxClient.link('mxgraph.css', 'css/mxgraph.css');

    /**
     *  Disables the built-in context menu
     */
    mxEvent.disableContextMenu(this._graph.container);

    /**
     * Dynamically insert boundary around label when node edges intersect with the label
     */
    setUpPerimeterBoundFunc();

    /**
     * Override the default window.alert behaviour of validation alerts
     */
    this._graph.validationAlert = function(message) {
        self._eventSubscriptions.push(eventsService.publish(SPNP_NOTIFICATION_EVENT, { message: message, type: 'info' }));
    }.bind(this._graph);

    /**
     * Transitions may only arc to places
     */
    this.assignMultiplicity({ type: SpnpTransition.TRANSITION_TAG_NAME, validNeighbours:[SpnpPlace.PLACE_TAG_NAME], typeError: 'Transitions may only have arcs to places' });

    /**
     * Places may only arc to transitions
     */
    this.assignMultiplicity({ type: SpnpPlace.PLACE_TAG_NAME, validNeighbours:[SpnpTransition.TRANSITION_TAG_NAME], typeError: 'Places may only have arcs to transitions' });


    /**
     *  Automatically handle parallel edges
     */
    let layout = new mxParallelEdgeLayout(this._graph);
    let layoutManager = new mxLayoutManager(this._graph);
    
    layoutManager.getLayout = function(cell) {
        if (cell.getChildCount() > 0) {
            return layout;
        }
    };
    /**
     * Changes the default style for edges "in-place" and assigns
     * an alternate edge style which is applied in mxGraph.flip
     * when the user double clicks on the adjustment control point
     * of the edge. The ElbowConnector edge style switches to TopToBottom
     * if the horizontal style is true.
     */
    let edgeStyle = this._graph.getStylesheet().getDefaultEdgeStyle();
    edgeStyle[mxConstants.STYLE_ROUNDED] = true;
    edgeStyle[mxConstants.STYLE_CURVED] = '1';
    //edgeStyle[mxConstants.STYLE_EDGE] = mxEdgeStyle.ElbowConnector;


};

/**
 * Setup validation rules for the graph
 * This function assists with documentation
 * 
 * @param source Boolean indicating if this rule applies to the source or target 
 * terminal. default true(source)
 * @param type Type of the source or target terminal that this rule applies to.
 * @param attr Optional attribute name to match the source or target terminal.
 * @param value Optional attribute value to match the source or target terminal.
 * @param min Minimum number of edges for this rule. Default is 1.
 * @param max Maximum number of edges for this rule. The default n means infinite
 * @param validNeighbors Array of types of the opposite terminal for which this 
 * rule applies.
 * @param countError Error to be displayed for invalid number of edges.
 * @param typeError Error to be displayed for invalid opposite terminals.
 * @param validNeighborsAllowed Optional boolean indicating if the array of 
 * opposite types should be valid or invalid.
 */
GraphConfigController.prototype.assignMultiplicity = function({ source = true, type, attribute = null, value = null, min = 1, max = 'n', 
    validNeighbours = [], countError = null, typeError = null, validNeighboursAllowed = null }) {

    this._graph.multiplicities.push(new mxMultiplicity(source, type, attribute, value, min, max, validNeighbours, countError, typeError, validNeighboursAllowed));
};


/**
 * Redirects the perimeter to the label bounds 
 * if intersection between edge and label is found
 */
const setUpPerimeterBoundFunc = () => {
    let mxGraphViewGetPerimeterPoint = mxGraphView.prototype.getPerimeterPoint;
    mxGraphView.prototype.getPerimeterPoint = function(terminal, next, orthogonal, border) {
        let point = mxGraphViewGetPerimeterPoint.apply(this, arguments);
        
        if (point !== null) {
            let perimeter = this.getPerimeterFunction(terminal);

            if (terminal.text !== null && terminal.text.boundingBox !== null) {
                // Adds a small border to the label bounds
                let b = terminal.text.boundingBox.clone();
                b.grow(3);

                if (mxUtils.rectangleIntersectsSegment(b, point, next)) {
                    point = perimeter(b, terminal, next, orthogonal);
                }
            }
        }
        
        return point;
    };
};