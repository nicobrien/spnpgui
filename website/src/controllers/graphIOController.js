/**
 * @todo fix {@link onLoadGraph} handler to save a loaded graph before loading a new one
 * @todo add robustness to graph loading, in particular some feedback for 'bad' graph models that cant be loaded
 */

import { 
    eventsService, 
    removeAll, 
    LOAD_GRAPH_EVENT,
    AFTER_GRAPH_LOAD_EVENT 
} from './events';
import { scrollToMiddle } from '../mxGraphInit';
import ElementNameController from './elementNameController';
import Logger from './logger';


/**
 * The {@link GraphIOController} is responsible for handling the
 * loading and unloading of graph models in response to events
 * triggered by server calls for a graph
 */
class GraphIOController {

    /**
     * @param {mxGraph} graph The graph this controller is for
     */
    constructor(graph) {
        this._graph = graph;
        this._logger = new Logger({ name: 'GraphIOController' });
        this._eventSubscriptions = [];
        this.init();
    }

    init() {
        this._eventSubscriptions.push(eventsService.subscribe(LOAD_GRAPH_EVENT, this.onLoadGraph.bind(this)));
    }

    /**
     * Tear down init
     */
    destroy() {
        removeAll(this._eventSubscriptions);
        this._eventSubscriptions = [];
    }

    clearGraph() {
        let g = this._graph;
        let model = g.getModel();
        model.beginUpdate();
        try {
            g.removeCells(g.getChildCells(g.getDefaultParent(), true, true));
        } catch (e) {
            this._logger.error('Failed to clear graph', e);
        } finally {
            // Updates the display
            model.endUpdate();
            // model.clear();
        }
    }
}

/**
 * 
 * Event handler for loading a graph
 *
 * @param {Number} id The ID of the graph to load
 * @param {String} name The name of the graph to load
 * @param {String} model The xml model of the graph to load
 */
GraphIOController.prototype.onLoadGraph = function({ id, name, model }) {
    const g = this._graph;
    // We may need access to the id and name in other modules that have 
    // access to the graph for example the graphKeyHandlerController ctrl-s
    g.spnpID = id;
    g.spnpName = name;
    this.clearGraph();
    if (model) {
        const doc = mxUtils.parseXml(model);
        const codec = new mxCodec(doc);
        g.getModel().beginUpdate();
        try {
            codec.decode(doc.documentElement, g.getModel());
            this._logger.info('Loaded graph', id);
            g.elementNameController = new ElementNameController(g);
            this._logger.info('Reset the name controller for the graph');
        } catch (e) {
            this._logger.error('Failed to update graph', e);
        } finally {
            // Updates the display
            g.getModel().endUpdate();
        }
        fixTranslationOnLoad(g);
        scrollToMiddle(g);
    } else {
        // todo: we are creating a graph (or there was something wrong with the graph model)
    }

    eventsService.publish(AFTER_GRAPH_LOAD_EVENT, { id });
};



/**
 * Arose from a bug where the cells are translated after load if edits are made to them
 * With no zoom (100% zoom) - transaltion is 15px,
 * 
 * @todo: Fix this properly!!
 * 
 * this bug was looked at for some time but the cause could not be determined this 
 * function is a temporary work-around. It selects all cells and translates them 
 * 15px to the left in order to counteract the problem
 */
const fixTranslationOnLoad = (g) => {
    let cells = g.getChildCells(g.getDefaultParent(), true, true);
    g.getModel().beginUpdate();
    try {
        cells.forEach((c) => {
            c.geometry.translate(-15, 0);
        });
    } finally {
        g.getModel().endUpdate();
    }
};


export default GraphIOController;