
import Logger from './logger';

/**
 * Names the event that is used to trigger the 
 * {@link NotificationController#onNotification} event
 */
export const SPNP_NOTIFICATION_EVENT = 'SPNP_NOTIFICATION_EVENT';

/**
 * Names the event that is used to clear all notifications for the 
 * {@link NotificationController}
 */
export const SPNP_CLEAR_NOTIFICATION_EVENT = 'SPNP_CLEAR_NOTIFICATION_EVENT';

/**
 * Names the event which fires when a graphModel is to be loaded into the mxGraph
 */
export const LOAD_GRAPH_EVENT = 'LOAD_GRAPH_EVENT';

/**
 * Names the event which fires after a graphModel is loaded into the mxGraph
 */
export const AFTER_GRAPH_LOAD_EVENT = 'AFTER_GRAPH_LOAD_EVENT';

/**
 * Names the event which fires when a place name is changed by the view. 
 * This can be used to trigger the graph to update in sync with a name 
 * change
 */
export const PLACE_NAME_CHANGED_EVENT = 'PLACE_NAME_CHANGED_EVENT';

/**
 * Names the event which fires when the number of tokens in a place is changed
 * by the view. This can be used to trigger the graph to update in sync.
 */
export const PLACE_TOKENS_CHANGED_EVENT = 'PLACE_TOKENS_CHANGED_EVENT';

/**
 * Names the event which fires when a transition name is changed by the 
 * view. This can be used to trigger the graph to update in sync with a name 
 * change
 */
export const TRANSITION_NAME_CHANGED_EVENT = 'TRANSITION_NAME_CHANGED_EVENT';

/**
 * Names the event which fires when a transition parameter is changed by the 
 * view. This can be used to trigger the graph to update in sync.
 */
export const TRANSITION_PARAMETER_CHANGED_EVENT = 'TRANSITION_PARAMETER_CHANGED_EVENT';

/**
 * Names the event which fires when a user clicks a main menu button
 */
export const NAV_MENU_BUTTON_CLICKED_EVENT = 'NAV_MENU_BUTTON_CLICKED_EVENT';

/**
 * Names the event which fires when a selection change occurs in the mxGraph
 */
export const GRAPH_SELECTION_CHANGED_EVENT = 'GRAPH_SELECTION_CHANGED_EVENT';

/**
 * Names the event which fires when a user changes the details view tab
 */
export const DETAILS_MENU_TAB_SELECTION_EVENT = 'DETAILS_MENU_TAB_SELECTION_EVENT';

/**
 * Names the event which fires when a graph is saved
 */
export const SAVE_GRAPH_EVENT = 'SAVE_GRAPH_EVENT';

/**
 * Names the event which fires when the drawing tool selection is changed
 */
export const CANVAS_DRAW_ITEM_SELECTED_EVENT = 'CANVAS_DRAW_ITEM_SELECTED_EVENT';

/**
 * Names the event which fires in order to trigger a graph undo action
 */
export const UNDO_GRAPH_EDIT_EVENT = 'UNDO_GRAPH_EDIT_EVENT';

/**
 * Names the event which fires in order to trigger a graph redo action
 */
export const REDO_GRAPH_EDIT_EVENT = 'REDO_GRAPH_EDIT_EVENT';

/**
 * Names the event which fires when the name of a graph is changed
 */
export const GRAPH_NAME_CHANGED_EVENT = 'GRAPH_NAME_CHANGED_EVENT';

/**
 * Names the event which triggers the hiding of the outline window
 */
export const HIDE_OUTLINE_VIEW_EVENT = 'HIDE_OUTLINE_VIEW_EVENT';

/**
 * Names the event which triggers the showing of the outline window
 */
export const SHOW_OUTLINE_VIEW_EVENT = 'SHOW_OUTLINE_VIEW_EVENT';

/**
 * Names the event which triggers the re-render layout of the graph as a
 * heirarchical layout
 */
export const SET_HEIRARCHICAL_LAYOUT_EVENT = 'SET_HEIRARCHICAL_LAYOUT_EVENT';

/**
 * Names the event which triggers the re-render layout of the graph as a
 * organic layout
 */
export const SET_ORGANIC_LAYOUT_EVENT = 'SET_ORGANIC_LAYOUT_EVENT';

/**
 * Names the event which triggers the re-render layout of the graph as a
 * stack layout
 */
export const SET_STACK_LAYOUT_EVENT = 'SET_STACK_LAYOUT_EVENT';

/**
 * The logger instance for the eventsService
 */
const logger = new Logger({ name: 'EventService' });


/**
 * The Events service is a singleton publish subscribe service.
 * 
 * It is particularly useful to pass events from the events that
 * occur in the React user view to mxGraph for updating the spn
 * canvas, and vice-vesa.
 * 
 * @example
 * import { eventsService } from './events';
 * const handleEvent = (...params) => { logger.log(params); }
 * eventsService.subscribe('someEvent', handleEvent);
 * eventsService.publish('someEvent', { arg1: 'a param to pass to the handler'});
 */
const EventService = (function() {
    /**
     * EventService is a singleton
     */
    let instance = null;

    /**
     * The internal constructor for the event service
     * @return {Function} an actual event service instance
     */
    const _EventService = function() {

        /**
         * Private event store (by closure)
         */
        let events = {};

        return {
            /**
             * Allows a caller to subscribe to a event
             * 
             * @param {String} eventName The name of the event to subscribe to
             * @param {Function} callback The callback function to be called when the event is published to,
             * this callback function should take a single options parameter of type {Object}
             * @return object with a single function, remove(), which will remove the subscription
             * added by this function
             */
            subscribe: function(eventName, callback) {
                // Create the events object if not yet created
                if(!events.hasOwnProperty(eventName)) {
                    events[eventName] = [];
                }
                // Add the callback function to queue and remember index
                let index = events[eventName].push(callback) - 1;
                let count = events[eventName].filter(s => !(!s)).length;
                logger.fine(`New subscription for '${eventName}' for a total of ${count} subscriptions to this event.`);
                // Provide the subscriber with a callback for removing the event
                return {
                    eventName: eventName,
                    remove: function() {
                        logger.fine(`Removing subscription for event: ${ eventName }`);
                        delete events[eventName][index];
                        logger.fine(`There are ${events[eventName].filter(s => !(!s)).length} subscriptions remaining for the ${ eventName } event`);
                    }
                };
            },

            /**
             * Publish a particular event
             * 
             * @param {String} eventName the event to publish
             * @param {Object} params The params object to pass to the callback function associated with the topic
             */
            publish: function(eventName, params) {
                logger.fine(`'${eventName}' published with params:`, params);
                if(!events.hasOwnProperty(eventName)) {
                    // no one is listening to this event
                    return;
                }
                events[eventName].forEach(function(callback) {
                    params = params || {};
                    callback(params);
                });
            },

            /**
             * Remove all handlers from the event store
             */
            destroy: function() {
                Object.keys(events).forEach(function (prop) {
                    delete events[prop];
                });
            }
        };
    };

    return {

        /**
         * The instance creator for this module
         * @return {_EventService} uses the internal function to 
         * construct an event service instance
         */
        getInstance: function() {
            if (!instance) {
                instance = new _EventService();
            }
            return instance;
        }
    };

}());

// single instance pattern
export let eventsService = EventService.getInstance();

/**
 * Cancel all subscription that the subscription list has removal 
 * objects for. Also delete the reference in the passed array of
 *  subscription removal objects.
 * 
 * @param {Array} subscriptionList a list of subscription removal 
 * objects. These objects are returned from {@link eventsService} 
 * @return The number of removed subscriptions
 */
export const removeAll = function(subscriptionList) {
    let i = 0;
    subscriptionList.forEach(subscription => {
        subscription.remove();
        i++;
    });
    subscriptionList = [];
    return i;
};