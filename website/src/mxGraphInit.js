import GraphKeyHandlerController from './controllers/graphKeyHandlerController';
import GraphUndoRedoController from './controllers/graphUndoRedoController';
import GraphOutlineController from './controllers/graphOutlineController';
import GraphDrawingController from './controllers/graphDrawingController';
import GraphToolbarController from './controllers/graphToolbarController';
import GraphConfigController from './controllers/graphConfigController';
import GraphLayoutController from './controllers/graphLayoutController';
import ElementNameController from './controllers/elementNameController';
import GraphIOController from './controllers/graphIOController';
import SpnpTransition from './models/transition';
import SpnpPlace from './models/place';


// debug
var debugmode = false;


export const init = (container) => {

    if (!mxClient) {
        throw 'mxClient not loaded when needed by graph init()';
    }
    if (!mxClient.isBrowserSupported()) {
        mxUtils.error('Browser is not supported!', 200, false);
    } else {

        // Register the shapes that we can make
        mxCellRenderer.registerShape('spnpPlace', SpnpPlace.Shape);
        mxCellRenderer.registerShape('spnpTransition', SpnpTransition.Shape);

        // Creates the graph inside the given container
        let graph = new mxGraph(container);

        const graphConfigController = new GraphConfigController(graph);
        const graphToolbarController = new GraphToolbarController(graph);
        const graphDrawingController = new GraphDrawingController(graph);
        const graphKeyHandlerController = new GraphKeyHandlerController(graph);
        const graphIOController = new GraphIOController(graph);
        const graphUndoRedoController = new GraphUndoRedoController(graph);
        const graphOutlineController = new GraphOutlineController(graph);
        const graphLayoutController = new GraphLayoutController(graph);

        graph.elementNameController = new ElementNameController(graph);

        initEventHandlers(graph);

        let istokenLabelVisible = true;
        
        // Hook for returning number of tokens for a given cell
        graph.getTokenLabelForPlace = function(cell) {
            return (SpnpPlace.isPlace(cell)) 
                ? cell.value.getAttribute('tokens')
                : null;
        };

        // Creates the shape for the shape number and puts it into the draw pane
        var redrawShape = graph.cellRenderer.redrawShape;
        graph.cellRenderer.redrawShape = function(state, force, rendering) {
            var result = redrawShape.apply(this, arguments);

            if (result 
                && istokenLabelVisible 
                && state.cell.geometry !== null 
                && !state.cell.geometry.relative) {
                var tokenLabel = graph.getTokenLabelForPlace(state.cell);

                if (tokenLabel != null && state.shape != null && state.tokenLabel == null) {
                    state.tokenLabel = new mxText(tokenLabel, new mxRectangle(),
                            mxConstants.ALIGN_LEFT, mxConstants.ALIGN_BOTTOM);

                    // Styles the label
                    state.tokenLabel.color = 'black';
                    state.tokenLabel.family = 'Verdana';
                    state.tokenLabel.size = 12;
                    state.tokenLabel.fontStyle = mxConstants.FONT_ITALIC;
                    state.tokenLabel.background = 'none';
                    state.tokenLabel.border = 'none';
                    state.tokenLabel.valign = 'bottom';
                    state.tokenLabel.dialect = state.shape.dialect;
                    state.tokenLabel.dialect = mxConstants.DIALECT_STRICTHTML;
                    state.tokenLabel.wrap = true;
                    graph.cellRenderer.initializeLabel(state, state.tokenLabel);
                }
            }
            
            if (state.tokenLabel != null) {
                var scale = graph.getView().getScale();
                var bounds = new mxRectangle(
                    state.x + state.width - state.width / 2 * scale - 7, 
                    state.y + SpnpPlace.DEFAULT_PLACE_RADIUS / 2 * scale + 5, 
                    35, 
                    0
                );
                state.tokenLabel.state = state;
                state.tokenLabel.value = graph.getTokenLabelForPlace(state.cell);
                state.tokenLabel.scale = scale;
                state.tokenLabel.bounds = bounds;
                state.tokenLabel.redraw();
            }
            
            return result;
        };

        // Destroys the shape number
        var destroy = graph.cellRenderer.destroy;
        graph.cellRenderer.destroy = function(state) {
            destroy.apply(this, arguments);
            
            if (state.tokenLabel != null) {
                state.tokenLabel.destroy();
                state.tokenLabel = null;
            }
        };
        
        graph.cellRenderer.getShapesForState = function(state) {
            return [state.shape, state.text, state.tokenLabel, state.control];
        };

        graph.isCellResizable = function(cell) {
            let geometry = this.model.getGeometry(cell);
            let isPlaceOrTransition = SpnpPlace.isPlace(cell) || SpnpTransition.isTransition(cell);
            return !isPlaceOrTransition && (geometry == null || !geometry.relative);
        };

        // The verticies store additional information so the default label
        // getter should be overridden
        graph.convertValueToString = (cell) => {
            if (mxUtils.isNode(cell.value)) {
                return cell.getAttribute('label', '');
            }
        };

        let cellLabelChanged = graph.cellLabelChanged;
        graph.cellLabelChanged = function(cell, newValue, autoSize) {
            if (mxUtils.isNode(cell.value)) {
                // Clones the value for correct undo/redo
                var elt = cell.value.cloneNode(true);
                elt.setAttribute('label', newValue);
                newValue = elt;
            }
            cellLabelChanged.apply(this, arguments);
        };

        /**
         * Returns the padding for pages in page view with scrollbars.
         */
        graph.getPagePadding = function() {
            return new mxPoint(Math.max(0, Math.round(graph.container.offsetWidth - 34)),
                    Math.max(0, Math.round(graph.container.offsetHeight - 34)));
        };

        /**
         * Returns the size of the page format scaled with the page size.
         */
        graph.getPageSize = function() {
            return (this.pageVisible) ? new mxRectangle(0, 0, this.pageFormat.width * this.pageScale,
                    this.pageFormat.height * this.pageScale) : this.scrollTileSize;
        };

        /**
         * Returns a rectangle describing the position and count of the
         * background pages, where x and y are the position of the top,
         * left page and width and height are the vertical and horizontal
         * page count.
         */
        graph.getPageLayout = function() {
            var size = (this.pageVisible) ? this.getPageSize() : this.scrollTileSize;
            var bounds = this.getGraphBounds();

            if (bounds.width === 0 || bounds.height === 0) {
                return new mxRectangle(0, 0, 1, 1);
            } else {
                // Computes untransformed graph bounds
                var x = Math.ceil(bounds.x / this.view.scale - this.view.translate.x);
                var y = Math.ceil(bounds.y / this.view.scale - this.view.translate.y);
                var w = Math.floor(bounds.width / this.view.scale);
                var h = Math.floor(bounds.height / this.view.scale);
                
                var x0 = Math.floor(x / size.width);
                var y0 = Math.floor(y / size.height);
                var w0 = Math.ceil((x + w) / size.width) - x0;
                var h0 = Math.ceil((y + h) / size.height) - y0;
                
                return new mxRectangle(x0, y0, w0, h0);
            }
        };

        // Fits the number of background pages to the graph
        graph.view.getBackgroundPageBounds = function() {
            var layout = this.graph.getPageLayout();
            var page = this.graph.getPageSize();
            
            return new mxRectangle(this.scale * (this.translate.x + layout.x * page.width),
                    this.scale * (this.translate.y + layout.y * page.height),
                    this.scale * layout.width * page.width,
                    this.scale * layout.height * page.height);
        };

        graph.getPreferredPageSize = function(bounds, width, height) {
            var pages = this.getPageLayout();
            var size = this.getPageSize();
            
            return new mxRectangle(0, 0, pages.width * size.width, pages.height * size.height);
        };

        /**
         * Guesses autoTranslate to avoid another repaint (see below).
         * Works if only the scale of the graph changes or if pages
         * are visible and the visible pages do not change.
         */
        var graphViewValidate = graph.view.validate;
        graph.view.validate = function() {
            if (this.graph.container !== null && mxUtils.hasScrollbars(this.graph.container)) {
                var pad = this.graph.getPagePadding();
                var size = this.graph.getPageSize();
                
                // Updating scrollbars here causes flickering in quirks and is not needed
                // if zoom method is always used to set the current scale on the graph.
                var tx = this.translate.x;
                var ty = this.translate.y;
                this.translate.x = pad.x / this.scale - (this.x0 || 0) * size.width;
                this.translate.y = pad.y / this.scale - (this.y0 || 0) * size.height;
            }
            
            graphViewValidate.apply(this, arguments);
        };

        var graphSizeDidChange = graph.sizeDidChange;
        graph.sizeDidChange = function() {
            if (this.container !== null && mxUtils.hasScrollbars(this.container)) {
                var pages = this.getPageLayout();
                var pad = this.getPagePadding();
                var size = this.getPageSize();
                
                // Updates the minimum graph size
                var minw = Math.ceil(2 * pad.x / this.view.scale + pages.width * size.width);
                var minh = Math.ceil(2 * pad.y / this.view.scale + pages.height * size.height);
                
                var min = graph.minimumGraphSize;
                
                // LATER: Fix flicker of scrollbar size in IE quirks mode
                // after delayed call in window.resize event handler
                if (min === null || min.width !== minw || min.height !== minh) {
                    graph.minimumGraphSize = new mxRectangle(0, 0, minw, minh);
                }
                
                // Updates auto-translate to include padding and graph size
                var dx = pad.x / this.view.scale - pages.x * size.width;
                var dy = pad.y / this.view.scale - pages.y * size.height;
                
                if (!this.autoTranslate && (this.view.translate.x !== dx || this.view.translate.y !== dy)) {
                    this.autoTranslate = true;
                    this.view.x0 = pages.x;
                    this.view.y0 = pages.y;

                    // NOTE: THIS INVOKES THIS METHOD AGAIN
                    var tx = graph.view.translate.x;
                    var ty = graph.view.translate.y;

                    graph.view.setTranslate(dx, dy);
                    graph.container.scrollLeft += (dx - tx) * graph.view.scale;
                    graph.container.scrollTop += (dy - ty) * graph.view.scale;

                    this.autoTranslate = false;
                    return;
                }

                graphSizeDidChange.apply(this, arguments);
            }
        };

        let style = graph.getStylesheet().getDefaultVertexStyle();
        style[mxConstants.STYLE_SHAPE] = 'spnpPlace';


        // Sets initial scrollbar positions
        window.setTimeout((e) => { scrollToMiddle(graph); }, 0);

        window.addEventListener('resize', (e) => { scrollToMiddle(graph); });

        // we return the graph so that we can destroy it if the view is torn down
        return graph;
    }
};

export const scrollToMiddle = graph => {
    const bounds = graph.getGraphBounds();
    const width = Math.max(bounds.width, (graph.scrollTileSize.width - 300) * graph.view.scale);
    const height = Math.max(bounds.height, graph.scrollTileSize.height * graph.view.scale);
    graph.container.scrollTop = Math.floor(Math.max(0, bounds.y - Math.max(20, (graph.container.clientHeight - height) / 4)));
    graph.container.scrollLeft = Math.floor(Math.max(0, bounds.x - Math.max(0, (graph.container.clientWidth - width) / 2)));
};


const initEventHandlers = (graph) => {
    if (debugmode) {
        mxEvent.addGestureListeners(
            graph.container, 
            function startListener() {}, 
            function moveListener(graph, event) {
                /**
                 * @todo: make this a debug window
                 */
                // console.log(' x: ' +  event.x + '   y: ' + event.y);
                // console.log('ox: ' +  event.offsetX + '  oy: ' + event.offsetY);
            }.bind(this, graph), 
            function endListener() {}
        );


        let oldEncode = mxCodec.prototype.encode;
        mxCodec.prototype.encode = function(obj) {
            mxLog.show();
            mxLog.debug('mxCodec.encode: obj=' + mxUtils.getFunctionName(obj.constructor));
            return oldEncode.apply(this, arguments);
        };

    }
};