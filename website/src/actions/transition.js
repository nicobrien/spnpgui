

import { CANCELLED_BY_ACTION_CONSTRUCTOR } from './defaultActions';
import { 
    eventsService, 
    TRANSITION_NAME_CHANGED_EVENT, 
    TRANSITION_PARAMETER_CHANGED_EVENT 
} from '../controllers/events';

export const SINGLE_TRANSITION_SELECTED = 'SINGLE_TRANSITION_SELECTED';
export const TRANSITION_NAME_CHANGED = 'TRANSITION_NAME_CHANGED';
export const TRANSITION_PARAMETER_CHANGED = 'TRANSITION_PARAMETER_CHANGED';

/**
 * Action constructor
 * @param {String} id The id of the selected transition
 * @param {String} name The name of the transition
 * @param {String} distribution The distribution of the transiton, for 
 * example 'exponential'
 * @param {Number} parameter The rate or probability of the transition
 * @return {Action} an action that may be dispatched to the store when a 
 * transition is selected
 */
export const singleTransitionSelected = (id, name, distribution, parameter) => {
    return {
        type: SINGLE_TRANSITION_SELECTED,
        id: id,
        name: name,
        distribution: distribution,
        parameter: parameter
    };
};

/**
 * Action constructor
 * @param {String} id The id of the selected transition
 * @param {String} name The name of the transition
 * @return {Action} an action that may be dispatched to the store when the 
 * name of the transition is changed
 */
export const transitionNameChanged = (id, name) => {
    // if (!SpnpTransition.isNameValid(name)) { return; }
    eventsService.publish(TRANSITION_NAME_CHANGED_EVENT, { transitionId: id, name });
    return {
        type: TRANSITION_NAME_CHANGED,
        id: id,
        name: name
    };
};

/**
 * Action constructor
 * @param {String} id The id of the selected transition
 * @param {String} parameter The rate or probability of the transition
 * @return {Action} an action that may be dispatched to the store when rate 
 * associated with the transition has been changed
 */
export const transitionParameterChanged = (id, parameter) => {
    // if (!SpnpTransition.isRateValid(parameter)) { return; }
    eventsService.publish(TRANSITION_PARAMETER_CHANGED_EVENT, { transitionId: id, parameter });
    return {
        type: TRANSITION_PARAMETER_CHANGED,
        id: id,
        parameter: parameter
    };
};

/**
 * Action constructor
 * @param {String} id The id of the selected transition
 * @param {String} parameter The distribution of the transition
 * @return {Action} an action that may be dispatched to the store when the 
 * transition distribution is changed
 */
export const transitionDistributionChanged = (id, distribution) => {
    return {
        type: CANCELLED_BY_ACTION_CONSTRUCTOR,
        reason: 'the distribution change functionality has not been implemented yet'
    };
};