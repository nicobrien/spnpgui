
import CreateController from '../controllers/createController';
import { navMenuButtonClicked } from './navMenuActions';
import { eventsService, LOAD_GRAPH_EVENT } from '../controllers/events';


export const EDIT_GRAPH = 'EDIT_GRAPH';
export const EDIT_GRAPH_START = 'EDIT_GRAPH_START';
export const GET_GRAPHS = 'GET_GRAPHS';
export const GET_GRAPHS_PENDING = 'GET_GRAPHS_PENDING';
export const GET_GRAPHS_REJECTED = 'GET_GRAPHS_PENDING';
export const GET_GRAPHS_FULFILLED = 'GET_GRAPHS_FULFILLED';
export const NEW_GRAPH_ADDED = 'NEW_GRAPH_ADDED';
export const CREATE_GRAPH = 'CREATE_GRAPH';
export const CREATE_GRAPH_PENDING = 'CREATE_GRAPH_PENDING';
export const CREATE_GRAPH_REJECTED = 'CREATE_GRAPH_REJECTED';
export const CREATE_GRAPH_FULFILLED = 'CREATE_GRAPH_FULFILLED';
export const DISPLAY_MODE_CHANGED = 'DISPLAY_MODE_CHANGED';
export const SORT_MODE_CHANGED = 'SORT_MODE_CHANGED';


/**
 * @param {Object} createController A controller capapble of 
 * creating a new graph
 */
export const createGraph = (createController) => {

    /**
     * @todo: this is NOT good, each time this action is fired 
     * we do not want to create a new controller becaus if the 
     * controller binds events then we will get duplicate events
     * and a tonne of other problems. We need to work out a way 
     * to enforce the injection of the controller (for an example
     * of this see #getGraphs(openController)). 
     * 
     * At the moment this is a problem because this action is 
     * dispatched by the reducer, so the controller would need 
     * to be on the state (store) in order for this injection 
     * to work.
     * 
     * To avoid complications, controllers that are created in
     * redux flow (eg: in this action creator) should NOT bind 
     * to any events.
     */
    createController = createController || new CreateController();

    /**
     * Thunk middleware knows how to handle functions
     * @param {Function} dispatch the dispatch method is passed as 
     * an argument to the function so as to make it able to dispatch 
     * actions itself.
     */
    return (dispatch) => {

        // First dispatch: the app state is updated to inform
        // that the API call is starting.
        dispatch(createGraphPending());

        createController.create().then((response) => {
            const { id, graph } = response;
            dispatch(createGraphFulfilled(id));
            dispatch(newGraphAdded(graph));
            dispatch(navMenuButtonClicked('edit'));
            // We publish this event as the graph is not within the react DOM
            eventsService.publish(LOAD_GRAPH_EVENT, { id: id, name: '', model: '' });
        }).catch((e) => {
            dispatch(createGraphRejected(e));
        });
    };
};

export const newGraphAdded = (graph) => {
    return {
        type: NEW_GRAPH_ADDED,
        isPending: false,
        graph: graph
    };
};

export const createGraphFulfilled = (id) => {
    return {
        type: CREATE_GRAPH_FULFILLED,
        isPending: false,
        id: id
    };
};

export const createGraphPending = () => {
    return {
        type: CREATE_GRAPH_PENDING,
        isPending: true,
    };
};

export const createGraphRejected = (error) => {
    return {
        type: CREATE_GRAPH_REJECTED,
        isPending: false,
        error: error
    };
};


/**
 * @param {Object} openController A controller capable of fetching 
 * graphs from the server
 */
export const getGraphs = (openController) => {

    /**
     * Thunk middleware knows how to handle functions
     * @param {Function} dispatch the dispatch method is passed as 
     * an argument to the function so as to make it able to dispatch 
     * actions itself.
     */
    return (dispatch) => {

        // First dispatch: the app state is updated to inform
        // that the API call is starting.
        dispatch(getGraphsPending());

        openController.getGraphs().then((response) => {
            dispatch(getGraphsFulfilled(response.data));
        }).catch((e) => {
            dispatch(getGraphsRejected(e));
        });
    };
};

export const getGraphsPending = () => {
    return {
        type: GET_GRAPHS_PENDING,
        isFetching: true,
        graphs: [],
        error: ''
    };
};

export const getGraphsRejected = (reason) => {
    return {
        type: GET_GRAPHS_REJECTED,
        isFetching: false,
        graphs: [],
        error: reason
    };
};

export const getGraphsFulfilled = (graphs) => {
    return {
        type: GET_GRAPHS_FULFILLED,
        isFetching: false,
        graphs: graphs,
        error: ''
    };
};

export const sortModeChanged = (mode) => {
    if (!mode) { mode = 'ascending'; }
    return {
        type: SORT_MODE_CHANGED,
        sortMode: mode
    };
};

export const displayModeChanged = (mode) => {
    if (!mode) { mode = 'card'; }
    return {
        type: DISPLAY_MODE_CHANGED,
        displayMode: mode
    };
};

export const editGraph = ({ id, name, model }) => {
    return (dispatch) => {
        dispatch(editGraphStart(id, name));
        dispatch(navMenuButtonClicked('edit'));
        eventsService.publish(LOAD_GRAPH_EVENT, { id: id, name: name, model: model });
    };
};

export const editGraphStart = (id, name) => {
    return {
        type: EDIT_GRAPH_START,
        id: id,
        name: name,
        isEditPending: true
    };
}; 