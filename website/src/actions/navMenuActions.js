
import { eventsService, NAV_MENU_BUTTON_CLICKED_EVENT } from '../controllers/events';
import { getCSPL } from './csplCodeActions';

export const NAV_MENU_BUTTON_CLICKED = 'NAV_MENU_BUTTON_CLICKED';
export const CSPL_MENU_BUTTON_CLICKED = 'CSPL_MENU_BUTTON_CLICKED';

/**
 * This is a special nav menu action - we also fetch 
 * cspl when the user clicks this menu button.
 * 
 * @param {String} graphModel The XML encoded mxGraph to retrieve cspl for
 */
export const csplNavMenuButtonClicked = (graphModel) => {
    
        /**
         * Thunk middleware knows how to handle functions
         * @param {Function} dispatch the dispatch method is passed as 
         * an argument to the function so as to make it able to dispatch 
         * actions itself.
         */
        return (dispatch) => {
            dispatch(getCSPL(graphModel));
            dispatch(navMenuButtonClicked('cspl'));
        };
    };

/**
 * Action constructor for a menu button click action
 */
export const navMenuButtonClicked = (clickedItem) => {
    eventsService.publish(NAV_MENU_BUTTON_CLICKED_EVENT, clickedItem);
    return {
        type: NAV_MENU_BUTTON_CLICKED,
        clickedItem: clickedItem
    };
};