
import GetCsplController from '../controllers/getCsplController';


export const GET_CSPL_PENDING = 'GET_CSPL_PENDING';
export const GET_CSPL_REJECTED = 'GET_CSPL_REJECTED';
export const GET_CSPL_FULFILLED = 'GET_CSPL_FULFILLED';


/**
 * @param {String} graphModel The XML encoded mxGraph to retrieve cspl for
 */
export const getCSPL = (graphModel) => {

    let getCsplController = new GetCsplController(graphModel);

    /**
     * Thunk middleware knows how to handle functions
     * @param {Function} dispatch the dispatch method is passed as 
     * an argument to the function so as to make it able to dispatch 
     * actions itself.
     */
    return (dispatch) => {

        // First dispatch: the app state is updated to inform
        // that the API call is starting.
        dispatch(getCsplPending());

        getCsplController.getCSPL().then((results) => {
            const csplCode = results.cspl;
            dispatch(getCsplFulfilled(csplCode));
        }).catch((e) => {
            dispatch(getCsplRejected(e));
        });
    };
};

export const getCsplFulfilled = (csplCode) => {
    return {
        type: GET_CSPL_FULFILLED,
        isPending: false,
        code: csplCode,
        hasCode: true,
        hasError: false
    };
};

export const getCsplPending = () => {
    return {
        type: GET_CSPL_PENDING,
        isPending: true,
        hasError: false
    };
};

export const getCsplRejected = (error) => {
    return {
        type: GET_CSPL_REJECTED,
        isPending: false,
        code: '',
        hasCode: false,
        hasError: true,
        error: error
    };
};
