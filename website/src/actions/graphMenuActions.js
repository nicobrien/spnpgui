
import { 
    eventsService, 
    REDO_GRAPH_EDIT_EVENT,
    UNDO_GRAPH_EDIT_EVENT,
    SHOW_OUTLINE_VIEW_EVENT,
    HIDE_OUTLINE_VIEW_EVENT,
    SET_HEIRARCHICAL_LAYOUT_EVENT,
    SET_ORGANIC_LAYOUT_EVENT,
    SET_STACK_LAYOUT_EVENT
} from '../controllers/events';


export const UNDO_GRAPH_MENU_BUTTON_CLICKED = 'UNDO_GRAPH_MENU_BUTTON_CLICKED';
export const REDO_GRAPH_MENU_BUTTON_CLICKED = 'REDO_GRAPH_MENU_BUTTON_CLICKED';
export const SHOW_OUTLINE_MENU_BUTTON_CLICKED = 'SHOW_OUTLINE_MENU_BUTTON_CLICKED';
export const HIDE_OUTLINE_MENU_BUTTON_CLICKED = 'HIDE_OUTLINE_MENU_BUTTON_CLICKED';
export const SET_STACK_LAYOUT_BUTTON_CLICKED = 'SET_STACK_LAYOUT_BUTTON_CLICKED';
export const SET_ORGANCIC_LAYOUT_BUTTON_CLICKED = 'SET_ORGANCIC_LAYOUT_BUTTON_CLICKED';
export const SET_HEIRARCHICAL_LAYOUT_BUTTON_CLICKED = 'SET_HEIRARCHICAL_LAYOUT_BUTTON_CLICKED';

/**
 * Action constructor for a the undo graph edit menu 
 * button action
 */
export const redoEdit = () => {
    eventsService.publish(REDO_GRAPH_EDIT_EVENT, {});
    return {
        type: REDO_GRAPH_MENU_BUTTON_CLICKED,
    };
};

/**
 * Action constructor for a the undo graph edit menu 
 * button action
 */
export const undoEdit = () => {
    eventsService.publish(UNDO_GRAPH_EDIT_EVENT, {});
    return {
        type: REDO_GRAPH_MENU_BUTTON_CLICKED,
    };
};

/**
 * Action constructor for a the show outline
 */
export const showOutline = () => {
    eventsService.publish(SHOW_OUTLINE_VIEW_EVENT, {});
    return {
        type: SHOW_OUTLINE_MENU_BUTTON_CLICKED,
        outlineVisible: true
    };
};

/**
 * Action constructor for a the show outline
 */
export const hideOutline = () => {
    eventsService.publish(HIDE_OUTLINE_VIEW_EVENT, {});
    return {
        type: HIDE_OUTLINE_MENU_BUTTON_CLICKED,
        outlineVisible: false
    };
};

/**
 * Action constructor for a graph heirachical layout
 */
export const setHeirarchicalLayout = () => {
    eventsService.publish(SET_HEIRARCHICAL_LAYOUT_EVENT, {});
    return {
        type: SET_HEIRARCHICAL_LAYOUT_BUTTON_CLICKED
    };
};

/**
 * Action constructor for a graph heirachical layout
 */
export const setOrganicLayout = () => {
    eventsService.publish(SET_ORGANIC_LAYOUT_EVENT, {});
    return {
        type: SET_ORGANCIC_LAYOUT_BUTTON_CLICKED
    };
};


/**
 * Action constructor for a graph heirachical layout
 */
export const setStackLayout = (horizontal) => {
    eventsService.publish(SET_STACK_LAYOUT_EVENT, { horizontal });
    return {
        type: SET_STACK_LAYOUT_BUTTON_CLICKED
    };
};
