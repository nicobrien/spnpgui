

import { CANCELLED_BY_ACTION_CONSTRUCTOR } from './defaultActions';
import { 
    eventsService, 
    GRAPH_NAME_CHANGED_EVENT, 
} from '../controllers/events';

export const GRAPH_NAME_CHANGED = 'GRAPH_NAME_CHANGED';


/**
 * Action constructor
 * @param {String} id The id of the open graph
 * @param {String} name The name of the graph
 * @return {Action} an action that may be dispatched to the store when the 
 * name of the graph is changed
 */
export function graphNameChanged(id, name) {
    eventsService.publish(GRAPH_NAME_CHANGED_EVENT, { id: id, name });
    return {
        type: GRAPH_NAME_CHANGED,
        id: id,
        name: name
    };
}