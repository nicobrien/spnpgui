

import { CANCELLED_BY_ACTION_CONSTRUCTOR } from './defaultActions';
import SpnpPlace from '../models/place';
import { 
    eventsService, 
    PLACE_NAME_CHANGED_EVENT, 
    PLACE_TOKENS_CHANGED_EVENT 
} from '../controllers/events';

export const SINGLE_PLACE_SELECTED = 'SINGLE_PLACE_SELECTED';
export const PLACE_NAME_CHANGED = 'PLACE_NAME_CHANGED';
export const PLACE_TOKENS_CHANGED = 'PLACE_TOKENS_CHANGED';

/**
 * Action constructor
 * @param {String} id The id of the selected place
 * @param {String} name The name of the place
 * @param {Number} tokens The number of tokens associated with the place
 * @return {Action} an action that may be dispatched to the store when a place 
 * is selected
 */
export function singlePlaceSelected(id, name, tokens) {
    return {
        type: SINGLE_PLACE_SELECTED,
        id: id,
        name: name,
        tokens: tokens
    };
}

/**
 * Action constructor
 * @param {String} id The id of the selected place
 * @param {String} name The name of the place
 * @return {Action} an action that may be dispatched to the store when the 
 * name of the place is changed
 */
export function placeNameChanged(id, name) {
    // if (!SpnpPlace.isNameValid(name)) { return; }
    eventsService.publish(PLACE_NAME_CHANGED_EVENT, { placeId: id, name });
    return {
        type: PLACE_NAME_CHANGED,
        id: id,
        name: name
    };
}

/**
 * Action constructor
 * @param {String} id The id of the selected place
 * @param {String} tokens The number of tokens
 * @return {Action} an action that may be dispatched to the store when a the
 * number of tokens in a place changes
 */
export function placeTokensChanged(id, tokens) {
    if (!SpnpPlace.areTokensValid(tokens)) { 
        return {
            type: CANCELLED_BY_ACTION_CONSTRUCTOR,
            reason: 'The input number of tokens was not valid: ' + tokens
        };
    }
    eventsService.publish(PLACE_TOKENS_CHANGED_EVENT, { placeId: id, tokens });
    return {
        type: PLACE_TOKENS_CHANGED,
        id: id,
        tokens: tokens
    };
}