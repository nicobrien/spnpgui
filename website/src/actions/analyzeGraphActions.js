
import AnalyzeGraphController from '../controllers/analyzeGraphController';
import { navMenuButtonClicked } from './navMenuActions';
import { eventsService } from '../controllers/events';


export const ANALYZE_GRAPH_PENDING = 'ANALYZE_GRAPH_PENDING';
export const ANALYZE_GRAPH_REJECTED = 'ANALYZE_GRAPH_REJECTED';
export const ANALYZE_GRAPH_FULFILLED = 'ANALYZE_GRAPH_FULFILLED';


/**
 * @param {String} graphModel an XML representation of the
 * selected mxGraph that is to be analyzed
 */
export const analyzeGraph = (graphModel) => {

    let analyzeGraphController = new AnalyzeGraphController(graphModel);

    /**
     * Thunk middleware knows how to handle functions
     * @param {Function} dispatch the dispatch method is passed as 
     * an argument to the function so as to make it able to dispatch 
     * actions itself.
     */
    return (dispatch) => {

        // First dispatch: the app state is updated to inform
        // that the API call is starting.
        dispatch(analyzeGraphPending());

        analyzeGraphController.requestAnalysis().then((results) => {
            dispatch(analyzeGraphFulfilled(results));
        }).catch((e) => {
            dispatch(analyzeGraphRejected(e));
        });
    };
};

export const analyzeGraphFulfilled = (results) => {
    const hasResults = Object.keys(results).length !== 0;    
    let hasError = hasResults 
        && Object.keys(results.netInfo).length === 0;
    
    return {
        type: ANALYZE_GRAPH_FULFILLED,
        isPending: false,
        results: results,
        hasError: hasError
    };
};

export const analyzeGraphPending = () => {
    return {
        type: ANALYZE_GRAPH_PENDING,
        isPending: true,
        results: {},
        hasError: false
    };
};

export const analyzeGraphRejected = (error) => {
    return {
        type: ANALYZE_GRAPH_REJECTED,
        isPending: false,
        error: error,
        results: {},
        hasError: true
    };
};
