
/**
 * Use to signify that an action has been aborted befor dispatch, it is a good
 * idea to track the reason as shown in the example for logging purposes.
 * @example
 * return {
 *     type: CANCELLED_BY_ACTION_CONSTRUCTOR,
 *            reason: 'The input number of tokens was not valid'
 *     };
 * }
 */
export const CANCELLED_BY_ACTION_CONSTRUCTOR = 'CANCELLED_BY_ACTION_CONSTRUCTOR';

/**
 * Not explicitly required but can be a useful constant for the default action 
 * that is passed to the reducers the first time
 * @example
 * import { NO_OP } from './defaultActions';
 * const reducer = (states = { d: default states}, action = NO_OP) { ...
 */
export const NO_OP = 'NO_OP';