
import reducers from './reducers/reducers';
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger'


/**
 * We use redux-thunk as middleware for dispatching async actions
 */
import thunkMiddleware from 'redux-thunk';

/**
 * We use logging as a middleware for the actions performed on the store.
 * This is primarily for debugging purposes and hence could be option
 * controlled so that it is not shipped in production releases.
 *
 * Note that the npm install is for --save-dev only
 *
 * Further configuration options are explained here:
 * https://github.com/evgenyrodionov/redux-logger
 */
const logger = createLogger({
    collapsed: true
});



/**
 * The redux 'global' store
 * Note: order matters for the application of middleware. Specifically,
 * the logger should go AFTER thunkMiddleware to ensure that async
 * actions are correctly logged
 */
const store = createStore(
    reducers,
    applyMiddleware(
        thunkMiddleware,
        logger
    )
);

/**
 * The store exportation allows non-react modules
 * (such as the SPNP graph modules) to dispatch actions
 * to the store.
 * We probably want to change this pattern and instead
 * inject the store into the functions that need it
 */
export default store;