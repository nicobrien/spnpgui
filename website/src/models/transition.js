/**
 * This model can create and insert transitions into the graph. 
 * It uses the mxGraph library function {@link insertVertex()} 
 * to achieve this. 
 * 
 * For clarity, this function has the following signature:
 * 
 * mxGraph.prototype.insertVertex = function(parent, id, value, x, y, width, height, style, relative) {...}
 * parent   mxCell that specifies the parent of the new vertex
 * id       Optional string that defines the Id of the new vertex
 * value    Object to be used as the user object
 * x        Integer that defines the x coordinate of the vertex
 * y        Integer that defines the y coordinate of the vertex
 * width    Integer that defines the width of the vertex
 * height   Integer that defines the height of the vertex
 * style    Optional string that defines the cell style
 * relative Optional boolean that specifies if the geometry is relative. Default is false
 * 
 */



/**
 * The xml tag that will be assigned to new spnp transition by default
 */
const TRANSITION_TAG_NAME = 'transition';

/**
 * The default width, in pixes, of any newly created transitions
 */
const DEFAULT_TRANSITION_WIDTH = 10;

/**
 * The default height, in pixes, of any newly created transitions
 */
const DEFAULT_TRANSITION_HEIGHT = 50;

/**
 * The default transition style is a key value pair list of styles to be applied
 * to the transition. The list is of the form 'key=value;...'
 * Style keys are defined {@see mxClient.js mxConstants }
 */
const DEFAULT_TRANSITION_STYLE = 'shape=spnpTransition;verticalLabelPosition=bottom;spacingTop=-25';

/**
 * The distribution function that associated with the transition.
 */
const DEFAULT_TRANSITION_DISTRIBUTION = 'Exponential';

/**
 * The default rate associated with the transition
 * Note that this should not be 0
 */
const DEFAULT_TRANSITION_PARAMETER = 1;

/**
 * Create and return an XML object to represent the state of 
 * a transition.
 * 
 * @param {String} name The name of the transition, 
 * Default T1, T2, ...
 * @param {String} distribution The distribution function that 
 * associated with the transition
 * @param {Number} parameter is a rate parameter for timed 
 * transitions [0, ...] 
 */
const createTransition = (name, distribution, parameter) => {
    let doc = mxUtils.createXmlDocument();
    let node = doc.createElement(TRANSITION_TAG_NAME);
    node.setAttribute('label', name);
    node.setAttribute('distribution', distribution);
    node.setAttribute('parameter', parameter);
    return node;
};

/**
 * Returns the next XML transition object. This function is 
 * used to ensure a default insertion of transitions have a 
 * unique tag and have the default values set. 
 * 
 * Transitions start at T0 and continue sequentially:
 * T0, T1, ..., Tn
 */
const getNextTransition = graph => {
    let name = graph.elementNameController.getNextTransitionName();
    // the tag is the initial tag name
    return createTransition(
        name, 
        DEFAULT_TRANSITION_DISTRIBUTION, 
        DEFAULT_TRANSITION_PARAMETER
    );
};

function Shape(bounds, fill, stroke, strokewidth) { 
    mxShape.call(this);
    this.fill = fill;
    this.stroke = stroke;
    this.strokewidth = (strokewidth !== null) ? strokewidth : 1;
}

Shape.prototype = new mxRectangleShape();

Shape.prototype.paintVertexShape = function(c, x, y, w, h) {
    c.rect(x, y, w, h);
    c.fillAndStroke();
};

/**
 * Insert a new transition into a given graph at co-ordinates
 * (x, y) and re-render the graph to make the changes visible 
 * to the user.
 * The new transition state data is generated using 
 * {@link getNextTransition()}
 * 
 * @param {mxGraph} graph The graph in which to insert a transition
 * @param {Number} x The x co-ordinate to insert the transition
 * @param {Number} y The y co-ordinate to insert the transition
 */
function insert(graph, x, y) {
    let parent = graph.getDefaultParent();
    let xOffset = (DEFAULT_TRANSITION_WIDTH / 2);
    let yOffset = (DEFAULT_TRANSITION_HEIGHT / 2);
    let vertex;
    graph.getModel().beginUpdate();
    try {
        x = x - xOffset;
        y = y - yOffset;
        vertex = graph.insertVertex(
            parent, 
            null, 
            getNextTransition(graph),
            x, 
            y, 
            DEFAULT_TRANSITION_WIDTH, 
            DEFAULT_TRANSITION_HEIGHT, 
            DEFAULT_TRANSITION_STYLE
        );
    } finally {
        // Updates the display
        graph.getModel().endUpdate();
    }
    return vertex;
}

/**
 * @param {mxCell} cell The cell to test if it is a transition
 */
function isTransition(cell) {
    return cell && cell.geometry && cell.style && cell.style.indexOf('shape=spnpTransition') !== -1;
}


export default {
    Shape: Shape,
    insert: insert,
    isTransition: isTransition,
    createTransition: createTransition,
    TRANSITION_TAG_NAME: TRANSITION_TAG_NAME
};