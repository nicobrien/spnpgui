/**
 * This model can create and insert places into the graph. 
 * It uses the mxGraph library function {@link insertVertex()} 
 * to achieve this. 
 * 
 * For clarity, the {@link insert vertex} function has the following signature:
 * 
 * mxGraph.prototype.insertVertex = function(parent, id, value, x, y, width, height, style, relative) {...}
 * parent   mxCell that specifies the parent of the new vertex
 * id       Optional string that defines the Id of the new vertex
 * value    Object to be used as the user object
 * x        Integer that defines the x coordinate of the vertex
 * y        Integer that defines the y coordinate of the vertex
 * width    Integer that defines the width of the vertex
 * height   Integer that defines the height of the vertex
 * style    Optional string that defines the cell style
 * relative Optional boolean that specifies if the geometry is relative. Default is false
 * 
 */

import ElementNameController from '../controllers/elementNameController';

/**
 * The xml tag that will be assigned to new spnp places by default
 */
const PLACE_TAG_NAME = 'place';

/**
 * The default radius, in pixes, of any newly created places
 */
const DEFAULT_PLACE_RADIUS = 50;

/**
 * The default place style is a key value pair list of styles to be applied
 * to the place. The list is of the form 'key=value;...'
 * Style keys are defined {@see mxClient.js mxConstants }
 */
const DEFAULT_PLACE_STYLE = 'shape=spnpPlace;verticalLabelPosition=bottom;resizable=false;spacingTop=-25;perimeter=ellipsePerimeter;targetPerimeterSpacing=0';

/**
 * The default number of tokens that will be assigned to a place when 
 * it is first inserted into a graph
 */
const DEFAULT_INITIAL_NUMBER_OF_TOKENS = 0;

if (!mxClient) {
    throw 'mxClient not loaded when needed by place model';
}

/**
 * Create and return an XML object to represent the state of 
 * a place.
 * 
 * @param {String} name The name of the place
 * @param {Number} tokens The number of tokens assigned to this place
 */
const create = (name, tokens) => {
    let doc = mxUtils.createXmlDocument();
    let node = doc.createElement(PLACE_TAG_NAME);
    node.setAttribute('label', name);
    node.setAttribute('tokens', tokens);
    return node;
};

const getNextPlace = (graph) => {
    let name = graph.elementNameController.getNextPlaceName();
    return create(name, DEFAULT_INITIAL_NUMBER_OF_TOKENS);
};

function Shape(bounds, fill, stroke, strokewidth) {
    mxShape.call(this);
    this.bounds = bounds;
    this.fill = fill;
    this.stroke = stroke;
    this.strokewidth = (strokewidth !== null) ? strokewidth : 1;
}

// or alternatively mxUtils.extend(Shape, mxShape);
Shape.prototype = new mxShape();

Shape.prototype.paintVertexShape = function(c, x, y, w, h) {
    c.ellipse(x, y, w, h);
    c.fillAndStroke();
};

function insert(graph, x, y) {
    let parent = graph.getDefaultParent();
    let offset = (DEFAULT_PLACE_RADIUS / 2);
    let vertex = null;
    let xCorord = x - offset;
    let yCoord = y - offset;
    graph.getModel().beginUpdate();
    try {
        // This vertex is the place
        vertex = graph.insertVertex(
            parent, 
            null, 
            getNextPlace(graph),
            xCorord, 
            yCoord, 
            DEFAULT_PLACE_RADIUS, 
            DEFAULT_PLACE_RADIUS, 
            DEFAULT_PLACE_STYLE
        );
    } finally {
        // Updates the display
        graph.getModel().endUpdate();
    }
    return vertex;
}

/** @param {mxCell} cell The cell to test if it is a place */
const isPlace = cell =>  cell && cell.geometry && cell.style && cell.style.indexOf('shape=spnpPlace') !== -1;

/** validates a number of tokens */
const areTokensValid = tokens => /^\d+$/.test(tokens);



export default {
    Shape: Shape,
    insert: insert,
    create: create,
    isPlace: isPlace,
    areTokensValid: areTokensValid,
    DEFAULT_PLACE_RADIUS: DEFAULT_PLACE_RADIUS,
    PLACE_TAG_NAME: PLACE_TAG_NAME
};