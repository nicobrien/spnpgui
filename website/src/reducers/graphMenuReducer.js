


import { 
    SHOW_OUTLINE_MENU_BUTTON_CLICKED, 
    HIDE_OUTLINE_MENU_BUTTON_CLICKED 
} from '../actions/graphMenuActions';
import { NO_OP } from '../actions/defaultActions';


const initialState = {
    outlineVisible: false,
};

/**
 * This reducer is responsible for transforming state of the graph menu
 */
const graphMenuReducer = (state = initialState, action = NO_OP) => {
    switch (action.type) {
        case SHOW_OUTLINE_MENU_BUTTON_CLICKED:
            return Object.assign({}, state, { outlineVisible: action.outlineVisible });
        case HIDE_OUTLINE_MENU_BUTTON_CLICKED:
            return Object.assign({}, state, { outlineVisible: action.outlineVisible });
        default:
            return state;
    }
};

export default graphMenuReducer;