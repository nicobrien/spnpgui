
import {
    GET_CSPL_PENDING,
    GET_CSPL_REJECTED,
    GET_CSPL_FULFILLED
} from '../actions/csplCodeActions';
import { EDIT_GRAPH_START } from '../actions/graphsListActions';
import { NO_OP } from '../actions/defaultActions';


const initialState = {
    hasCode: false,
    code: ''
};

/**
 * This reducer is responsible for transforming the
 * state of the cspl code editor
 */
const csplCodeReducer = (state = initialState, action = NO_OP) => {
    switch (action.type) {
        case EDIT_GRAPH_START:
            return { ...state, code: '', hasCode: false };
        case GET_CSPL_PENDING: 
            return { ...state, isPending: action.isPending };
        case GET_CSPL_REJECTED: 
            return { ...state, hasError: action.hasError, isPending: action.isPending, code: action.code, hasCode: action.hasCode };
        case GET_CSPL_FULFILLED: 
            return { ...state, hasError: action.hasError, isPending: action.isPending, code: action.code, hasCode: action.hasCode };
        default:
            return state;
    }
};

export default csplCodeReducer;