

import { combineReducers } from 'redux';
import transitionPropertiesReducer from './transitionPropertiesReducer';
import graphPropertiesReducer from './graphPropertiesReducer';
import placePropertiesReducer from './placePropertiesReducer';
import analyzeGraphReducer from './analyzeGraphReducer';
import graphsListReducer from './graphsListReducer';
import graphMenuReducer from './graphMenuReducer';
import csplCodeReducer from './csplCodeReducer';
import navMenuReducer from './navMenuReducer';

const propertiesEditorReducer = combineReducers({
    transitionProperties: transitionPropertiesReducer,
    placeProperties: placePropertiesReducer,
    graphProperties: graphPropertiesReducer
});

const reducers = combineReducers({
    propertiesEditor: propertiesEditorReducer,
    graphAnalysis: analyzeGraphReducer,
    graphsList: graphsListReducer,
    graphMenu: graphMenuReducer,
    csplCode: csplCodeReducer,
    navMenu: navMenuReducer
});


export default reducers;