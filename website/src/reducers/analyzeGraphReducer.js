
import { 
    ANALYZE_GRAPH_PENDING, 
    ANALYZE_GRAPH_REJECTED,
    ANALYZE_GRAPH_FULFILLED
} from '../actions/analyzeGraphActions';
import { EDIT_GRAPH_START } from '../actions/graphsListActions';
import { NO_OP } from '../actions/defaultActions';


const initialState = {
    isPending: false,
    hasResults: false,
    results: {},
    hasError: false
};

/**
 * This reducer is responsible for transforming state of graph analyzer
 */
const analyzeGraphReducer = (state = initialState, action = NO_OP) => {
    switch (action.type) {
        case EDIT_GRAPH_START:
            return Object.assign({}, state, { results: {}, hasError: false });
        case ANALYZE_GRAPH_REJECTED:
            return Object.assign({}, state, { isPending: action.isPending, results: action.results, hasError: action.hasError });
        case ANALYZE_GRAPH_PENDING:
            return Object.assign({}, state, { isPending: action.isPending, results: action.results, hasError: action.hasError });
        case ANALYZE_GRAPH_FULFILLED:
            return Object.assign({}, state, { isPending: action.isPending, results: action.results, hasError: action.hasError });
        default:
            return state;
    }
};

export default analyzeGraphReducer;