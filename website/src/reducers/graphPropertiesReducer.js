

import { GRAPH_NAME_CHANGED } from '../actions/graph';
import { EDIT_GRAPH_START } from '../actions/graphsListActions';
import { NO_OP, CANCELLED_BY_ACTION_CONSTRUCTOR } from '../actions/defaultActions';

const initialState = { 
    id: '',
    name: ''
};

/**
 * This reducer is responsible for the state changes applicable to the 
 * place tab in the properties editor
 */
const graphPropertiesReducer = (state = initialState, action = NO_OP) => {
    switch (action.type) {
        case EDIT_GRAPH_START:
            return Object.assign({}, state, { id: action.id, name: action.name });
        case GRAPH_NAME_CHANGED:
            return Object.assign({}, state, { id: action.id, name: action.name });
        default:
            return state;
    }
};

export default graphPropertiesReducer;