
import { NO_OP } from '../actions/defaultActions';
import { 
    CREATE_GRAPH,
    CREATE_GRAPH_PENDING, 
    CREATE_GRAPH_REJECTED, 
    CREATE_GRAPH_FULFILLED,
    DISPLAY_MODE_CHANGED,
    EDIT_GRAPH_START,
    GET_GRAPHS,
    GET_GRAPHS_PENDING, 
    GET_GRAPHS_REJECTED, 
    GET_GRAPHS_FULFILLED ,
    NEW_GRAPH_ADDED,
    SORT_MODE_CHANGED
} from '../actions/graphsListActions';
import { GRAPH_NAME_CHANGED } from '../actions/graph';

const initialState = {
    sortMode: 'ascending',
    displayMode: 'card',
    isPending: false,
    graphs: [],
    error: '',
    id: null,
    name: ''
};

/**
 * This reducer is responsible for state changes that manage the list of graphs
 * that are available to edit.
 */
const graphsListReducer = (state = initialState, action = NO_OP) => {
    switch (action.type) {
        case SORT_MODE_CHANGED:
            return { ...state, sortMode: action.sortMode };
        case DISPLAY_MODE_CHANGED:
            return { ...state, displayMode: action.displayMode };
        case NEW_GRAPH_ADDED:
            return { ...state, graphs: state.graphs.concat(action.graph) };
        case GRAPH_NAME_CHANGED:
            return { ...state, name: action.name };
        case EDIT_GRAPH_START:
            return { ...state, id: action.id, name: action.name };
        case GET_GRAPHS:
            return state;
        case GET_GRAPHS_PENDING:
            return { ...state,  isPending: action.isPending, error: '' };
        case GET_GRAPHS_REJECTED:
            return { ...state, isPending: action.isPending, error: action.error };
        case GET_GRAPHS_FULFILLED:
            return { ...state,  isPending: action.isPending, error: '', graphs: action.graphs };
        case CREATE_GRAPH:
            return state;
        case CREATE_GRAPH_PENDING:
            return { ...state, isPending: action.isPending, error: '' };
        case CREATE_GRAPH_REJECTED:
            return { ...state, isPending: action.isPending, error: action.error };
        case CREATE_GRAPH_FULFILLED:
            return { ...state,  isPending: action.isPending, error: '', id: action.id };
        default:
            return state;
    }
};

export default graphsListReducer;