
import { NAV_MENU_BUTTON_CLICKED } from '../actions/navMenuActions';
import { NO_OP } from '../actions/defaultActions';

const initialState = {
    activeView: 'home',
    homeClickAction: () => {},
    editClickAction: () => {},
    isEditDisabled: false
};

/**
 * This reducer is responsible for state changes that manage which view the 
 * user is looking at. For example, the 'open/save graph view' or the 
 * 'edit graph view'
 */
const navMenuReducer = (state = initialState, action = NO_OP) => {
    switch (action.type) {
        case NAV_MENU_BUTTON_CLICKED:
            return Object.assign({}, state, { activeView: action.clickedItem });
        default:
            return state;
    }
};

export default navMenuReducer;