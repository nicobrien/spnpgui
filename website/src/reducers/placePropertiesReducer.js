

import { SINGLE_PLACE_SELECTED, PLACE_NAME_CHANGED, PLACE_TOKENS_CHANGED } from '../actions/place';
import { NO_OP, CANCELLED_BY_ACTION_CONSTRUCTOR } from '../actions/defaultActions';

const initialState = { 
    id: '',
    name: '',
    tokens: ''
};

/**
 * This reducer is responsible for the state changes applicable to the 
 * place tab in the properties editor
 */
const placePropertiesReducer = (state = initialState, action = NO_OP) => {
    switch (action.type) {
        case SINGLE_PLACE_SELECTED:
            return Object.assign({}, state, { id: action.id, name: action.name, tokens: action.tokens });
        case PLACE_NAME_CHANGED:
            return Object.assign({}, state, { name: action.name });
        case PLACE_TOKENS_CHANGED:
            return Object.assign({}, state, { tokens: action.tokens });
        case CANCELLED_BY_ACTION_CONSTRUCTOR:
            return state;
        default:
            return state;
    }
};

export default placePropertiesReducer;