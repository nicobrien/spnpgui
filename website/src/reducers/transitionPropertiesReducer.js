

import { 
    SINGLE_TRANSITION_SELECTED, 
    TRANSITION_NAME_CHANGED, 
    TRANSITION_PARAMETER_CHANGED,
    TRANSITION_DISTRIBUTION_CHANGED
} from '../actions/transition';

import { NO_OP, CANCELLED_BY_ACTION_CONSTRUCTOR } from '../actions/defaultActions';

const initialState = { 
    id: '',
    name: '',
    parameter: ''
};

/**
 * This reducer is responsible for the state changes applicable to the 
 * transition tab in the properties editor
 */
const transitionPropertiesReducer = (state = initialState, action = NO_OP) => {
    switch (action.type) {
        case SINGLE_TRANSITION_SELECTED:
            return Object.assign({}, state, { id: action.id, name: action.name, distribution: action.distribution, parameter: action.parameter });
        case TRANSITION_NAME_CHANGED:
            return Object.assign({}, state, { name: action.name });
        case TRANSITION_PARAMETER_CHANGED:
            return Object.assign({}, state, { parameter: action.parameter });
        case TRANSITION_DISTRIBUTION_CHANGED:
            return Object.assign({}, state, { distribution: action.distribution });
        case CANCELLED_BY_ACTION_CONSTRUCTOR:
            return state;
        default:
            return state;
    }
};

export default transitionPropertiesReducer;