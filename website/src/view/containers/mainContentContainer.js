

import { connect } from 'react-redux';
import MainContentComponent from '../components/mainContentComponent';

const mapStateToProps = state => {
    const { activeView } = state.navMenu;
    return { 
        activeView: activeView
    };
};

const mapDispatchToProps = dispatch => {
    return { };
};

const MainContentContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(MainContentComponent);

MainContentContainer.defaultProps = {
    isEditing: false,
    activeView: 'home'
};

export default MainContentContainer;

