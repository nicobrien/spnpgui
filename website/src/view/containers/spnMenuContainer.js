

import { connect } from 'react-redux';
import SpnMenuComponent from '../components/spnMenuComponent';
import { 
    undoEdit,
    redoEdit,
    showOutline,
    hideOutline,
    setHeirarchicalLayout,
    setOrganicLayout,
    setStackLayout
} from '../../actions/graphMenuActions';
import Logger from '../../controllers/logger';


const logger = new Logger();

const mapStateToProps = state => {
    let { outlineVisible } = state.graphMenu;
    let { id, name } = state.graphsList;
    return { 
        id: id,
        name: name,
        outlineVisible: outlineVisible
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onUndo: () => {
            dispatch(undoEdit());
        },
        onRedo: () => {
            dispatch(redoEdit());
        },
        onShowOutline: (vent) => {
            dispatch(showOutline());
        },
        onHideOutline: () => {
            dispatch(hideOutline());
        },
        onHeirarchicalLayout: () => {
            dispatch(setHeirarchicalLayout());
        },
        onOrganicLayout: () => {
            dispatch(setOrganicLayout());
        },
        onStackLayout: (horizontal) => {
            dispatch(setStackLayout(horizontal));
        }
    };
};

const SpnMenuContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(SpnMenuComponent);

SpnMenuContainer.defaultProps = {
    onSave: () => { logger.warn('SpnMenuContainer props not bound'); }
};

export default SpnMenuContainer;

