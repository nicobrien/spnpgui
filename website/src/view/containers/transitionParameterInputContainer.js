

import { connect } from 'react-redux';
import PropertyNumberInputComponent from '../components/propertyNumberInputComponent';
import { transitionParameterChanged } from '../../actions/transition';
import Logger from '../../controllers/logger';
const logger = new Logger();


const mapStateToProps = state => {
    const { id, parameter } = state.propertiesEditor.transitionProperties;
    return {
        id: id,
        value: parameter
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onChange: (id, e, opt) => {
            const parameter = opt.value;
            dispatch(transitionParameterChanged(id, parameter));
        }
    };
};

const TransitionParameterInputContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(PropertyNumberInputComponent);

TransitionParameterInputContainer.defaultProps = {
    id: '',
    value: '',
    label: 'Rate',
    placeHolder: 'Please enter a transition parameter...',
    onChange: () => { logger.warn('TransitionDistributionInputContainer props not bound'); }
};

export default TransitionParameterInputContainer;

