

import { connect } from 'react-redux';
import CSPLCodeComponent from '../components/csplCodeComponent';
import Logger from '../../controllers/logger';


const logger = new Logger({ name: 'CSPLCodeContainer' });

const mapStateToProps = state => {
    const { hasCode, code } = state.csplCode;
    const graphName = state.graphsList.name;
    return { hasCode, code, graphName };
};

const mapDispatchToProps = dispatch => {
    return { };
};

const CSPLCodeContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(CSPLCodeComponent);

export default CSPLCodeContainer;

