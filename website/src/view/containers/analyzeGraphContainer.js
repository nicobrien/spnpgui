

import { connect } from 'react-redux';
import AnalyzeGraphController from '../../controllers/analyzeGraphController';
import AnalyzeGraphComponent from '../components/analyzeGraphComponent';
import { analyzeGraph } from '../../actions/analyzeGraphActions';
import Logger from '../../controllers/logger';


const logger = new Logger({ name: 'AnalyzeGraphContainer' });

const mapStateToProps = state => {
    const selectedGraphId = state.graphsList.id;
    const graphName = state.graphsList.name;
    const graphs = state.graphsList.graphs || [];
    const graph = graphs.find(g => g.id === selectedGraphId);
    let { hasError } = state.graphAnalysis;
    if (!graph) {
        logger.error(`Could not find graph id '${selectedGraphId}' to analyze in the list of graphs (length '${graphs.length}') in the redux state`);
        hasError = true;
    }
    const graphModel = graph.model;
    logger.fine('GraphModel for analysis is currently:', { model: graphModel });
    const { results, isPending } = state.graphAnalysis;
    const hasResults = Object.keys(results).length !== 0;
    return { 
        hasResults,
        isPending,
        results,
        graphModel,
        graphName,
        hasError,
    };
};

const mapDispatchToProps = dispatch => {
    return { 
        boundStartAnalysis: graphModel => () => dispatch(analyzeGraph(graphModel))
    };
};

const AnalyzeGraphContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(AnalyzeGraphComponent);

export default AnalyzeGraphContainer;

