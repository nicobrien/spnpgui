

import { connect } from 'react-redux';
import NavMenuComponent from '../components/navMenuComponent';
import { navMenuButtonClicked, csplNavMenuButtonClicked } from '../../actions/navMenuActions';

const mapStateToProps = state => {
    const activeView = state.navMenu.activeView;
    const activeGraphId = state.graphsList.id;
    const noGraphLoaded = !(activeGraphId);
    const graphRecord = noGraphLoaded ? {} : state.graphsList.graphs.find(g => g.id === activeGraphId) || {};
    const graphModel = graphRecord.model;
    // we only certain views if a graph is loaded
    const isEditDisabled = noGraphLoaded;
    const isAnalyzeDisabled = noGraphLoaded;
    const isCSPLCodeDisabled = noGraphLoaded;
    return { 
        activeView,
        isEditDisabled,
        isAnalyzeDisabled,
        isCSPLCodeDisabled,
        graphModel
    };
};

const mapDispatchToProps = dispatch => {
    return { 
        homeClickAction: () => dispatch(navMenuButtonClicked('home')),
        editClickAction: () => dispatch(navMenuButtonClicked('edit')),
        analyzeClickAction: () => dispatch(navMenuButtonClicked('analyze')),
        bindCsplCodeClickAction: graphModel => () => dispatch(csplNavMenuButtonClicked(graphModel))
    };
};

const NavMenuContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(NavMenuComponent);

NavMenuContainer.defaultProps = {
    activeView: 'home'
};

export default NavMenuContainer;

