

import { connect } from 'react-redux';
import GraphsListComponent from '../components/graphsListComponent';
import { 
    createGraph, 
    displayModeChanged,
    editGraph,
    sortModeChanged 
} from '../../actions/graphsListActions';

const mapStateToProps = state => {
    const { graphs, isFetching, displayMode, sortMode } = state.graphsList;
    return { 
        isFetching: isFetching,
        graphs: graphs,
        displayMode,
        sortMode
    };
};

const mapDispatchToProps = dispatch => {
    return { 
        onCreateClick: () => dispatch(createGraph()),
        onEditClick: (id) => dispatch(editGraph(id)),
        onSetDisplayMode: (mode) => dispatch(displayModeChanged(mode)),
        onSetSortMode: (mode) => dispatch(sortModeChanged(mode))
    };
};

const GraphsListContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(GraphsListComponent);

GraphsListContainer.defaultProps = {
    graphs: []
};

export default GraphsListContainer;

