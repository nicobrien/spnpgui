

import { connect } from 'react-redux';
import PropertyInputComponent from '../components/propertyInputComponent';
import { placeNameChanged } from '../../actions/place';
import Logger from '../../controllers/logger';
const logger = new Logger();

const mapStateToProps = state => {
    const { id, name } = state.propertiesEditor.placeProperties;
    return {
        id: id,
        value: name
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onChange: (id, e, opt) => {
            const name = opt.value;
            dispatch(placeNameChanged(id, name));
        }
    };
};

const PlaceNameInputContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(PropertyInputComponent);

PlaceNameInputContainer.defaultProps = {
    id: '',
    value: '',
    label: 'Place Name',
    placeHolder: 'Please enter a place name...',
    onChange: () => { logger.warn('PlaceNameInputContainer props not bound'); }
};

export default PlaceNameInputContainer;

