

import { connect } from 'react-redux';
import PropertyInputComponent from '../components/propertyInputComponent';
import { transitionNameChanged } from '../../actions/transition';
import Logger from '../../controllers/logger';
const logger = new Logger();


const mapStateToProps = state => {
    const { id, name } = state.propertiesEditor.transitionProperties;
    return {
        id: id,
        value: name
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onChange: (id, e, opt) => {
            const name = opt.value;
            dispatch(transitionNameChanged(id, name));
        }
    };
};

const TransitionNameInputContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(PropertyInputComponent);

TransitionNameInputContainer.defaultProps = {
    id: '',
    value: '',
    label: 'Transition Name',
    placeHolder: 'Please enter a transition name',
    onChange: () => { logger.warn('TransitionNameInputContainer default props not bound'); }
};

export default TransitionNameInputContainer;