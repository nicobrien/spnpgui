

import { connect } from 'react-redux';
import PropertyInputComponent from '../components/propertyInputComponent';
import { placeTokensChanged } from '../../actions/place';
import Logger from '../../controllers/logger';
const logger = new Logger();


const mapStateToProps = state => {
    const { id, tokens } = state.propertiesEditor.placeProperties;
    return {
        id: id,
        value: tokens
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onChange: (id, e, opt) => {
            const tokens = opt.value;
            dispatch(placeTokensChanged(id, tokens));
        }
    };
};

const PlaceTokenInputContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(PropertyInputComponent);

PlaceTokenInputContainer.defaultProps = {
    id: '',
    value: '',
    label: 'Number of Tokens',
    placeHolder: 'Please enter the number of tokens...',
    onChange: () => { logger.warn('PlaceTokenInputContainer props not bound'); }
};

export default PlaceTokenInputContainer;

