

import { connect } from 'react-redux';
import PropertyDropdownComponent from '../components/propertyDropdownComponent';
import { transitionDistributionChanged } from '../../actions/transition';
import Logger from '../../controllers/logger';
const logger = new Logger();


const mapStateToProps = state => {
    const { id, distribution } = state.propertiesEditor.transitionProperties;
    return {
        id: id,
        defaultValue: distribution
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onChange: (id, e, opt) => {
            const distribution = opt.value;
            dispatch(transitionDistributionChanged(id, distribution));
        }
    };
};

const TransitionDistributionInputContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(PropertyDropdownComponent);

TransitionDistributionInputContainer.defaultProps = {
    id: '',
    defaultValue: 'Exponential',
    label: 'Distribution',
    value: 'Exponential',
    options: [
        { text: 'Exponential', value: 'Exponential', active: true, selected: true }
    ],
    placeHolder: 'Please select a distribution function type...',
    onChange: () => { logger.warn('TransitionDistributionInputContainer props not bound'); }
};

export default TransitionDistributionInputContainer;
