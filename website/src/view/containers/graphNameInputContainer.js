

import { connect } from 'react-redux';
import PropertyInputComponent from '../components/propertyInputComponent';
import { graphNameChanged } from '../../actions/graph';
import Logger from '../../controllers/logger';

const logger = new Logger({ name: 'GraphNameInputContainer' });

const mapStateToProps = state => {
    const { id } = state.graphsList;
    const { name } = state.propertiesEditor.graphProperties;
    return {
        id: id,
        value: name
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onChange: (id, e, opt) => {
            const name = opt.value;
            dispatch(graphNameChanged(id, name));
        }
    };
};

const GraphNameInputContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(PropertyInputComponent);

GraphNameInputContainer.defaultProps = {
    id: '',
    name: '',
    label: 'Graph Name',
    placeHolder: 'Please enter a place name...',
    onChange: () => { logger.warn('PlaceNameInputContainer props not bound'); }
};

export default GraphNameInputContainer;

