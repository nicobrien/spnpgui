
import React from 'react';
import { Header } from 'semantic-ui-react';
import EmptyStateComponent from './emptyStateComponent';
import CSPLEditorComponent from './csplEditorComponent';


const CSPLWrapper = ({ code, graphName }) => {
    let divHeight = `${(window.innerHeight || document.documentElement.clientHeight) - 122}px`;
    return (
        <div>
            <Header as='h3' style={{ padding: '85px 0px 0px 10px' }}>{ graphName && `CSPL for ${graphName}` || 'CSPL code' }</Header>
            <div style={{ height: divHeight, borderTop: '1px gainsboro solid' }}>
                <CSPLEditorComponent code={ code } />
            </div>
        </div>
    );
};

const NoCode = () => (
    <EmptyStateComponent  
            iconName='warning sign'
            iconSize='huge'
            heading='No Code' 
            message='There is no CSPL available.' />
);

const CSPLCodeComponent = ({ hasCode, code, graphName }) => {
    return (hasCode) ? <CSPLWrapper code={ code } graphName={ graphName }/> : <NoCode />;
};

CSPLCodeComponent.defaultProps = {
    hasCode: false, 
    code: ''
};

export default CSPLCodeComponent;