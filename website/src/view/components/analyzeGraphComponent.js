
import React from 'react';
import { 
    Button, 
    Container, 
    Grid, 
    Header, 
    Icon,
    Message,
    Segment,
    Statistic, 
    Tab, 
    TextArea
} from 'semantic-ui-react';
import { Tooltip } from 'react-tippy';
import EmptyStateComponent from './emptyStateComponent';
import { Bar, defaults } from 'react-chartjs-2';
import CSPLEditorComponent from './csplEditorComponent';


defaults.responsive = false;

const createAnalysisPanes = (graphName, results) => {
    const panes = [];
    panes.push({ menuItem: 'Graph', render: () => createGraphPane(graphName, results) });
    panes.push({ menuItem: 'Stats', render: () => createStatsPane(graphName, results) });
    panes.push({ menuItem: 'Raw',   render: () => createRawPane(graphName, results) });
    return panes;
};

const createGraphNameComponent = (graphName) => (
    <div style={{ borderBottom: '1px gainsboro solid' }}>
        <Header as='h3' style={{ padding: '10px 20px' }}>{ graphName || 'Unnamed Graph' }</Header>
    </div>
);

const createRawPane = (graphName, results) => {
    const rawText = results && results.raw || 'No raw analysis results found.';
    let divHeight = `${(window.innerHeight || document.documentElement.clientHeight) - 176}px`;
    return (
        <div style={{ padding: '15px 0px 0px 0px' }}>
            { createGraphNameComponent(graphName) }
            <div style={{ height: divHeight }}>
                <CSPLEditorComponent code={ rawText } />
            </div>
        </div>
    );
};

const getChartPlacesProbabilityNonEmpty = (places) => {
    const options = {
        legend : {
            display: false
        },
        title: {
            display: true,
            text: 'The probability that a place will NOT be empty (steady state analysis)'
        },
        scales: {
            yAxes: [{ 
                display: true, 
                position: 'left', 
                ticks: {
                    beginAtZero: true
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Probability (%)'
                }
            }],
            xAxes: [{ 
                scaleLabel: {
                    display: true,
                    labelString: 'Place name'
                }
            }]
        }
    };
    const placeLabels = Object.keys(places);
    const placeData = placeLabels.map(key => {
        const stats = places[key];
        const probabilityNonEmpty = parseFloat(stats.nonempty) * 100;
        return probabilityNonEmpty;
    });
    return <Bar
        options={ options } 
        data={{
            labels: placeLabels,
            datasets: [{
                    data: placeData
            }]
        }} />;
};


const getChartPlacesTokenCount = (places) => {
    const options = {
        legend : {
            display: false
        },
        title: {
            display: true,
            text: 'The average number of tokens expected to be in a place (steady state analysis)'
        },
        scales: {
            yAxes: [{ 
                display: true, 
                position: 'left', 
                scaleLabel: {
                    display: true,
                    labelString: 'Number of tokens'
                }
            }],
            xAxes: [{ 
                scaleLabel: {
                    display: true,
                    labelString: 'Place name'
                }
            }]
        }
    };
    const placeLabels = Object.keys(places);
    const placeData = placeLabels.map(key => {
        const stats = places[key];
        const averageNumberOfTokens = parseFloat(stats.tokens);
        return averageNumberOfTokens;
    });
    return <Bar
        options={ options } 
        data={{
            labels: placeLabels,
            datasets: [{
                    data: placeData
            }]
        }} />;
};

const createGraphPane = (graphName, results) => {
    let divHeight = `${(window.innerHeight || document.documentElement.clientHeight) - 118}px`;    
    return (
        <div style={{ padding: '15px 0px 0px 0px', height: divHeight, overflowY: 'scroll' }}>
            { createGraphNameComponent(graphName) }
            <div style={{ margin: 'auto', maxWidth: '800px' }}>
                { getChartPlacesProbabilityNonEmpty(results.average.places) }
                { getChartPlacesTokenCount(results.average.places) }
            </div>
        </div>
    );
};

const createStatsPane = (graphName, results) => {
    if (results.netInfo) {
        const stats = Object.keys(results.netInfo).map(key => {
            const value = results.netInfo[key];
            const color = parseInt(value) > 0 ? 'orange' : 'black';
            return (<Statistic key={ key } color={ color } value={ value } label={ key } />);
        });
        const expecteds = results.expected.map(expected => (
            <Segment key={ expected } style={{ margin: '0px 20px' }} vertical>
                <Header as='h4'>
                    <Tooltip
                        title='Expectation defined in CSPL by pr_expected()'
                        arrow={ true }
                        position='right'
                        delay={ 500 } >
                        <Icon name='info circle' size={ 'small' }/>
                    </Tooltip>
                    <Header.Content>
                        Expected
                        <Header.Subheader>{ expected }</Header.Subheader>
                    </Header.Content>
                </Header>
            </Segment>
        ));
        return (
            <div style={{ padding: '15px 0px 0px 0px' }}>
                { createGraphNameComponent(graphName) }
                <div style={{ margin: 'auto', maxWidth: '800px' }}>
                    <Grid style={{ marginTop: '20px' }}verticalAlign='middle' centered>
                        <Grid.Row stretched>
                            <Grid.Column stretched textAlign='center'>
                                <Statistic.Group widths={ 4 } size='mini'>
                                    { stats }
                                </Statistic.Group>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <div style={{ padding: '60px 0px' }}>
                        { expecteds }
                    </div>
                </div>
            </div>
        );
    } else {
        return <EmptyStateComponent  
            iconName='empty heart'
            iconSize='huge'
            heading='No Statistics' 
            message='No statistics were generated for this graph.' />;
    }
};

const AnalyzeResultsComponent = ({ graphName, results, onAnalyze }) => {
    return (
        <div style={{ padding: '75px 0px 0px 0px' }}>
            <Tab panes={ createAnalysisPanes(graphName, results) } />
        </div>
    );
};

const BlankAnalysisComponent = ({ onAnalyze, isPending }) => {
    return (
        <EmptyStateComponent  
            iconName='bar graph'
            iconSize='huge'
            heading='Analyze SPN' 
            message='The stochastic petri net may be analyzed. Click Start to begin a new analysis'
            callToAction={
                <Button 
                    color='green'
                    icon='play'
                    loading={ isPending }
                    content='Start'
                    onClick={ onAnalyze } />
            } />
    );
};

const AnalysisErrorComponent = () => (
    <EmptyStateComponent  
        iconName='warning sign'
        iconSize='huge'
        heading='Analysis Error' 
        message='There was some error processing the CSPL code for this net, please see the output logs in the analysis service.' />
);


const AnalyzeGraphComponent = (props) => {
    const { 
        hasError, 
        hasResults, 
        results, 
        graphName, 
        graphModel, 
        boundStartAnalysis,
        isPending
    } = props;

    const onAnalyze = boundStartAnalysis(graphModel);
    if (hasError) {
        return <AnalysisErrorComponent />;
    }
    return (hasResults) 
        ? <AnalyzeResultsComponent results={ results } graphName={ graphName } onAnalyze={ onAnalyze } />
        : <BlankAnalysisComponent onAnalyze={ onAnalyze } isPending={ isPending }/>;
};

AnalyzeGraphComponent.defaultProps = {
    results: {}
};

export default AnalyzeGraphComponent;