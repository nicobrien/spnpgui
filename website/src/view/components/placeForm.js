

import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import PlaceNameInputContainer from '../containers/placeNameInputContainer';
import PlaceTokenInputContainer from '../containers/placeTokenInputContainer';


const PlaceForm = (props) => {
    return(
        <Form>
            <PlaceNameInputContainer />
            <PlaceTokenInputContainer />
        </Form>
    );
};

export default PlaceForm;