/* jshint ignore:start */ // Dont use jshint to parse react jsx

import React, { Component } from 'react';
import { eventsService, CANVAS_DRAW_ITEM_SELECTED_EVENT } from '../../controllers/events';
import { 
    Tooltip,
    withTooltip
} from 'react-tippy';


class ToolbarItem extends Component {
    render() {
        var classList = 'toolbarItem';
        if (this.props.active) {
            classList += ' active';
        }
        return (
            <li className={ classList } onClick={ this.props.onClick }>
                <div className='toolbarItemContent noselect'>
                    <img draggable='false' src={ this.props.imgSource } />
                </div>
            </li>
        );
    }
}

class ToolbarComponent extends Component {
    constructor() {
        super();
        this.state = {
            activeItem: 'arc'
        };
    }
    handleClick(event, name) { 
        this.setState({ activeItem: name });
        eventsService.publish(CANVAS_DRAW_ITEM_SELECTED_EVENT, name);
    };
    isActive(name) { 
        return this.state.activeItem === name; 
    };
    render() {
        return (
            <div id="spnToolbar" className="leftToolbar">
                <ul className='toolbarItems'>
                    <Tooltip
                        title='Arc and canvas selection'
                        arrow={ true }
                        position='right'
                        delay={ 500 } >
                        <ToolbarItem
                            name='arc' 
                            imgSource='../images/arc.svg'
                            active={ this.isActive('arc') } 
                            onClick={ (proxy, event) => this.handleClick(event, 'arc') }/>
                    </Tooltip>
                    <Tooltip
                        title='Place drawing tool'
                        arrow={ true }
                        position='right'
                        delay={ 500 } >
                        <ToolbarItem
                            name='place' 
                            imgSource='../images/place.svg'
                            active={ this.isActive('place') } 
                            onClick={ (proxy, event) => this.handleClick(event, 'place') }/> 
                    </Tooltip>
                    <Tooltip
                        title='Transition drawing tool'
                        arrow={ true }
                        position='right'
                        delay={ 500 } >
                        <ToolbarItem
                            name='transition' 
                            imgSource='../images/transition.svg'
                            active={ this.isActive('transition') } 
                            onClick={ (proxy, event) => this.handleClick(event, 'transition') }/> 
                    </Tooltip>
                </ul>
            </div>
        );
    }
}

export default ToolbarComponent;