

import React from 'react';
import CodeMirror from 'react-codemirror';
require('codemirror/mode/clike/clike');


class CSPLEditorComponent extends React.Component {
   
    constructor(props) {
        super(props);
        this.state = {
            code: '// no cspl'
        };
    }

    updateCode(newCode) {
        this.setState({
            code: newCode
        });
    }

    render() {
        let { code } = this.props;
        let options = {
            lineNumbers: true, 
            mode: 'clike'
        };
        
        return (
                <CodeMirror 
                    value={ code } 
                    onChange={ this.updateCode.bind(this) } 
                    options={ options } />
        );
    }
}


export default CSPLEditorComponent;