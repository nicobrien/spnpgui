
import React from 'react';
import { Container, Grid, Header, Icon } from 'semantic-ui-react';


const canRenderProp = prop => prop ? true : false;

/**
 * 
 * @param {String} iconName The name of the icon to be displayed, 
 * or a value that coalesces to false(null, '', ...) if no icon 
 * is to be displayed
 * @param {String} iconSize { mini | tiny | small | large | big | huge | massive }
 * This is big by default
 * @param {String} header The header text to be displayed
 * @param {String} message The message to be displayed
 * @param {Object} callToAction A button component for a call to 
 * action
 */
const EmptyStateComponent = ({ iconName, iconSize, heading, message, callToAction }) => (
    <Grid style={ { height: '100%' } } verticalAlign='middle' centered>
        <Grid.Row stretched>
            <Grid.Column stretched textAlign='center'>
                <Container textAlign='center'>
                    { canRenderProp(iconName) && <Icon name={ iconName } size={ iconSize || 'big' } color='grey' circular /> }
                </Container>
                <Header as='h2'>
                    <Header.Content>{ heading }</Header.Content>
                    <Header.Subheader>{ message }</Header.Subheader>
                    <div style={ { paddingBottom: '15px' } } ></div>
                </Header>
                <Container textAlign='center'>
                    { canRenderProp(callToAction) && callToAction }
                </Container>
            </Grid.Column>
        </Grid.Row>
    </Grid>
);

export default EmptyStateComponent;