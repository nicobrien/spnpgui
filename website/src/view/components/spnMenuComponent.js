/* jshint ignore:start */ // Dont use jshint to parse react jsx

import React from 'react';
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react';
import { eventsService, SAVE_GRAPH_EVENT } from '../../controllers/events';

/**
 * @param {Function} onUndo The function to call when the undo
 * Dropdown.Item is clicked
 * @param {Function} onRedo The function to call when the redo
 * Dropdown.Item is clicked
 */
const EditDropdown = ({ onUndo, onRedo }) => {
    return (
        <Menu.Item name='Edit'>
            <Dropdown
                text='Edit'
                multiple={ false }
                closeOnChange={ true }>
                <Dropdown.Menu>
                    <Dropdown.Item text='Undo' description='ctrl + z' onClick={ onUndo } />
                    <Dropdown.Item text='Redo' description='ctrl + r' onClick={ onRedo }/>
                </Dropdown.Menu>
            </Dropdown>
        </Menu.Item>
    );
};

/**
 * @param {Function} onSave the callback function that will
 * fire when the save menu drop down is clicked
 */
const FileDropdown = ({ onSave }) => {
    return (
        <Menu.Item>
            <Dropdown
                text='File'
                multiple={ false }
                closeOnBlur={ true }
                closeOnChange={ true }>
                <Dropdown.Menu>
                    <Dropdown.Item
                        text='Save'
                        onClick={ onSave } />
                </Dropdown.Menu>
            </Dropdown>
        </Menu.Item>
    );
};

const ViewComponent = ({ onShowOutline, onHideOutline, outlineVisible, onHeirarchicalLayout, onOrganicLayout, onStackLayout }) => {
    // let options = [];
    let outline = outlineVisible
        ? <Dropdown.Item icon='checkmark' text='Show Outline' onClick={ onHideOutline } />
        : <Dropdown.Item text='Show Outline' onClick={ onShowOutline } />;
    return (
        <Menu.Item>
            <Dropdown
                text='View'
                multiple={ false }
                closeOnBlur={ true }
                closeOnChange={ true }>
                <Dropdown.Menu>
                    { outline }
                    <Dropdown.Divider />
                    <Dropdown.Item text='Render Organic Layout' onClick= { onOrganicLayout }/>
                    <Dropdown.Item text='Render Horizontal Layout' onClick= { onStackLayout.bind(this, true) }/>
                    
                    {/* having some trouble with these ones :(  */}
                    {/* <Dropdown.Item text='Render Heirarchical Layout' onClick= { onHeirarchicalLayout }/> */}
                    {/* <Dropdown.Item text='Render Vertical Layout' onClick= { onStackLayout.bind(this, false) }/> */}
                </Dropdown.Menu>
            </Dropdown>
        </Menu.Item>
    );
};

const SearchComponent = (props) => {
    return (
        <Menu.Menu position='right'>
            <div className='ui right aligned category search item'>
                <div className='ui transparent icon input'>
                    <input className='prompt' type='text' placeholder='Search...' />
                    <i className='search link icon' />
                </div>
                <div className='results'></div>
            </div>
        </Menu.Menu>
    );
};

/**
 * The Spn Menu is the main menu at the top of the graph view
 *
 * @param {Number} id The id of the graph currently being edited
 * @param {Function} onUndo The function to call when the undo
 * button is clicked
 * @param {Function} onRedo The function to call when the redo
 * button is clicked
 */
const SpnMenuComponent = ({ id, name, onRedo, onUndo, onShowOutline, onHideOutline, outlineVisible, onHeirarchicalLayout, onOrganicLayout, onStackLayout }) => {
    return (
        <div id='spnMenu' className='spnMenu'>
            <Menu secondary>
                <FileDropdown onSave={ () => { eventsService.publish(SAVE_GRAPH_EVENT, { id: id, name: name }); } }/>
                <EditDropdown onRedo={ onRedo } onUndo={ onUndo }/>
                <ViewComponent
                    outlineVisible={ outlineVisible }
                    onShowOutline={ onShowOutline }
                    onHideOutline={ onHideOutline }
                    onOrganicLayout={ onOrganicLayout }
                    onStackLayout={ onStackLayout }
                    onHeirarchicalLayout={ onHeirarchicalLayout }/>
            </Menu>
        </div>
    );
};

export default SpnMenuComponent;
