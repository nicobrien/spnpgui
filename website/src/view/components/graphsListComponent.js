
import React from 'react';
import { 
    Button,
    Card, 
    Container, 
    Grid,
    Label,
    List,
    Menu
} from 'semantic-ui-react';
import { Tooltip } from 'react-tippy';
import EmptyWelcomeComponent from './emptyWelcomeComponent';

const mostRecentComponent = (mostRecent) => {
    return mostRecent 
        ? <Tooltip 
            title='This SPN was edited last'
            arrow={ true }
            position='right'
            distance={ 150 }
            delay={ 500 } ><Label icon='clock' color='orange' corner='right' /></Tooltip> 
        : null;
};

const GraphCard = ({ id, name, model, createTime, updateTime, onEditClick, mostRecent }) => {
    return (
        <Card>
            <Card.Content>
                <Card.Header>{ name || 'Unnamed' }{ mostRecentComponent(mostRecent) }</Card.Header>
            </Card.Content>
            <Card.Content>
                <Card.Meta>Created: { createTime }</Card.Meta>
                <Card.Meta>Last Update: { updateTime }</Card.Meta>
            </Card.Content>
            <Card.Content>
                <Menu secondary>
                    <Menu.Menu position='right'>
                        <Menu.Item icon='edit' onClick={ onEditClick.bind(this, { id, name, model }) } />
                    </Menu.Menu>
                </Menu>
            </Card.Content>
        </Card>
    );
};

const CreateFab = ({ onCreateClick }) => {
    return (
        <div style={{ position: 'fixed', zIndex: '800', right: '60px', bottom: '60px'}}>
            <Button 
                style={{ boxShadow: '2px 2px 2px 0 rgba(34,36,38,.15)' }}
                circular
                color='green'
                icon='plus'
                size='massive'
                onClick={ onCreateClick } />
        </div>
    );
};

const getCards = (graphs, onEditClick) => {
    let mostRecent = null;
    if (graphs && graphs.length > 1) {
        mostRecent = [...graphs]
        .sort((g1, g2) => {
            return (g1.update_time > g2.update_time) ? -1 : 1;
        })
        .slice(0, 1)[0];
    }
    return graphs.map(graph => 
        <GraphCard 
            key={ graph.id } 
            id={ graph.id } 
            name={ graph.name } 
            mostRecent={ mostRecent && graph.id === mostRecent.id }
            model={ graph.model }
            createTime={ graph.create_time } 
            updateTime={ graph.update_time } 
            onEditClick= { onEditClick }/>
    );
};

const GraphListItem = ({ id, name, model, createTime, updateTime, onEditClick }) => {
    const displayName = name || 'Unnamed Graph';
    const  displayUpdateTime  = updateTime || 'the beginning of time';
    return (
        <List.Item>
            <List.Content floated='left'>
                <Button 
                    circular
                    icon='edit'
                    onClick={ onEditClick.bind(this, { id, name, model }) } />
            </List.Content>
            <List.Content>
                <List.Header>{ displayName }</List.Header>
                <List.Description>This graph was last updated at { updateTime }</List.Description>
            </List.Content>
        </List.Item>
    );
};

const getList = (graphs, onEditClick) => {
    return graphs.map(graph => (
        <GraphListItem
            key={ graph.id } 
            id={ graph.id } 
            name={ graph.name } 
            model={ graph.model }
            createTime={ graph.create_time } 
            updateTime={ graph.update_time } 
            onEditClick= { onEditClick } />
        )
    );
};

const WidgetMenu = (props) => {
    const { 
        graphs, 
        onEditClick, 
        displayMode, 
        onSetDisplayMode,
        onSetSortMode,
        sortMode
    } = props;

    const isListMode = displayMode === 'list';
    const dispalyIconActionParam = isListMode ? 'card' : 'list';
    const displayIconName = isListMode ? 'block layout' : 'list layout';

    const isSortAsc = sortMode === 'ascending';
    const displaySortActionParam = isSortAsc ? 'descending' : 'ascending';
    const sortIconName = isSortAsc ? 'sort alphabet ascending' : 'sort alphabet descending';
    

    return (
        <div style={{ position: 'fixed', top: '85px', right: '65px' }}>
            <Menu secondary>
                <Menu.Menu position='right'>

                    <Tooltip
                        title={ `Sort ${ isSortAsc ? 'descending' : 'ascending' }` }
                        arrow={ true }
                        position='bottom'
                        delay={ 500 } >
                        <Menu.Item icon={ sortIconName } onClick={ onSetSortMode.bind(this, displaySortActionParam) } />
                    </Tooltip>

                    <Tooltip
                        title={ `Show ${ isListMode ? 'grid' : 'list' } view` }
                        arrow={ true }
                        position='bottom'
                        delay={ 500 } >
                        <Menu.Item icon={ displayIconName } onClick={ onSetDisplayMode.bind(this, dispalyIconActionParam) } />
                    </Tooltip>

                </Menu.Menu>
            </Menu>
        </div>
    );
};

const GraphsListComponent = (props) => {
    const { 
        graphs, 
        onCreateClick, 
        onEditClick, 
        displayMode, 
        onSetDisplayMode,
        onSetSortMode,
        sortMode
    } = props;

    if (!graphs || graphs.length === 0) {
        return <EmptyWelcomeComponent onClick={ onCreateClick } />
    }

    if (sortMode === 'descending') {
        graphs.sort((g1, g2) => {
            return (g1.name > g2.name) ? -1 : 1;
        });
    } else if (sortMode === 'ascending') {
        graphs.sort((g1, g2) => {
            return (g1.name <= g2.name) ? -1 : 1;
        });
    }

    const isListMode = displayMode === 'list';    
    let wrappedView = null;
    if (isListMode) {
        const items = getList(graphs, onEditClick);
        wrappedView = (<div style={{ paddingRight: '60px' }}><List divided relaxed>{ items }</List></div>);
    } else {
        const cards = getCards(graphs, onEditClick);
        wrappedView = (<Card.Group>{cards}</Card.Group>);
    }
    return (
        <div>
            <WidgetMenu 
                graphs={ graphs }
                onEditClick={ onEditClick } 
                displayMode={ displayMode }
                onSetDisplayMode={ onSetDisplayMode } 
                onSetSortMode={ onSetSortMode }
                sortMode={ sortMode }/>
            <Container style={{ paddingTop: '120px', paddingLeft: '20px', paddingRight: '20px' }}>
                { wrappedView }
                <CreateFab onCreateClick={ onCreateClick }/>
            </Container>
        </div>
    );
};

export default GraphsListComponent;