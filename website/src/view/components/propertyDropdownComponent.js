/* jshint ignore:start */ // Dont use jshint to parse react jsx


import React from 'react';
import { Form, Dropdown } from 'semantic-ui-react';

/**
 * @param {String} id The id of the currently selected cell. This is required
 * so that the onChange event knows how to update the canvas with changes
 * @param {String} selectedLabel The currently selected value that will be 
 * displayed in the Dropdown
 * @param {String} label The Dropdown heading label
 * @param {String} placeHolder the text to display when the Dropdown is empty
 * @param {Function} onChange The callback function that will fire when
 * the onChange event fires
 */
const PropertyInputComponent = ({ id, selectedLabel, label, placeHolder, options, value, onChange }) => {
    return (
        <Form.Field>
            <label>{ label }</label>
            <Dropdown 
                search
                fluid={ true }
                selection={ true }
                value={ value }
                placeholder={ placeHolder }
                selectedLabel={ selectedLabel } 
                options= { options }
                onChange={ onChange.bind(this, id) } /> 
        </Form.Field>
    );
};

export default PropertyInputComponent;