/* jshint ignore:start */ // Dont use jshint to parse react jsx

import React from 'react';

import SpnBannerComponent from './spnBannerComponent';
import MainContentContainer from '../containers/mainContentContainer';

const PageComponent = (props) => {
    return (
        <div id='spnPageWrapper'>
            <SpnBannerComponent />
            <MainContentContainer />
        </div>
    );
};

export default PageComponent;