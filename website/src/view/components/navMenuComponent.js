/* jshint ignore:start */ // Dont use jshint to parse react jsx


import React from 'react';
import { Menu, Icon } from 'semantic-ui-react';
import { Tooltip } from 'react-tippy';


/**
 * Forms the main navigation menu for SPNP
 */
const NavMenuComponent = (props) => {
    let { 
        activeView, 
        isEditDisabled, 
        isAnalyzeDisabled, 
        homeClickAction, 
        editClickAction, 
        analyzeClickAction,
        isCSPLCodeDisabled,
        bindCsplCodeClickAction,
        graphModel
    } = props;

    if (isEditDisabled) {
        editClickAction = () => {};
    }
    return (
        <Menu 
            icon
            borderless 
            secondary
            style={ { marginTop: '0px', paddingLeft: '25px' } }>

            {/* HOME */}
            <Tooltip
                title='Home'
                arrow={ true }
                distance={ 22 }
                position='bottom'
                delay={ 500 } >
                <Menu.Item 
                    name='home' 
                    onClick={ homeClickAction }>
                    <span style={{ color: 'white'}}>
                        <Icon name='home' />
                    </span>
                </Menu.Item>
            </Tooltip>

            {/* EDIT */}
            <Tooltip
                title={ isEditDisabled ? 'You must open a SPN to begin editing' : 'Edit a SPN' }
                arrow={ true }
                distance={ 22 }
                position='bottom'
                delay={ 500 } >
                <Menu.Item 
                    disabled={ isEditDisabled }
                    name='edit' 
                    onClick={ editClickAction } >
                    <span style={{ color: 'white'}}>
                        <Icon name='sticky note' disabled={ isEditDisabled }/>
                    </span>
                </Menu.Item>
            </Tooltip>

            {/* ANALYZE */}
            <Tooltip
                title={ isAnalyzeDisabled ? 'You must open a SPN to run analysis on it' : 'Analyze a SPN' }
                arrow={ true }
                distance={ 22 }
                position='bottom'
                delay={ 500 } >
                <Menu.Item 
                    disabled={ isAnalyzeDisabled }
                    name='analyze' 
                    onClick={ analyzeClickAction } >
                    <span style={{ color: 'white'}}>
                        <Icon name='bar graph' disabled={ isAnalyzeDisabled }/>
                    </span>
                </Menu.Item>
            </Tooltip>

            {/* ANALYZE */}
            <Tooltip
                title={ isCSPLCodeDisabled ? 'You must open a SPN to generate CSPL for it' : 'View the CSPL for the SPN' }
                arrow={ true }
                distance={ 22 }
                position='bottom'
                delay={ 500 } >
                <Menu.Item 
                    disabled={ isCSPLCodeDisabled }
                    name='cspl' 
                    onClick={ bindCsplCodeClickAction(graphModel) } >
                    <span style={{ color: 'white'}}>
                        <Icon name='code' disabled={ isCSPLCodeDisabled }/>
                    </span>
                </Menu.Item>
            </Tooltip>

        </Menu>
    );
};

export default NavMenuComponent;