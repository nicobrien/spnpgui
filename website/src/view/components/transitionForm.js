

import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import TransitionNameInputContainer from '../containers/transitionNameInputContainer';
import TransitionParameterInputContainer from '../containers/transitionParameterInputContainer';
import TransitionDistributionInputContainer from '../containers/transitionDistributionInputContainer';


const TransitionForm = (props) => {
    return(
        <Form>
            <TransitionNameInputContainer />
            <TransitionDistributionInputContainer />
            <TransitionParameterInputContainer />
        </Form>
    );
};

export default TransitionForm;