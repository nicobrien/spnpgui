/* jshint ignore:start */ // Dont use jshint to parse react jsx


import React, { Component } from 'react';
import DetailsMenuComponent from './detailsMenuComponent';
import PropertiesEditorComponent from './propertiesEditorComponent';

const SpnPropertiesComponent = (props) => {
    return (
        <div id='spnProperties' className='spnProperties'>
            <DetailsMenuComponent />
            <PropertiesEditorComponent />
        </div>
    );
}

export default SpnPropertiesComponent;