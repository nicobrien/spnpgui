/* jshint ignore:start */ // Dont use jshint to parse react jsx


import React, { Component } from 'react';
import { eventsService, DETAILS_MENU_TAB_SELECTION_EVENT } from '../../controllers/events';

class MenuItem extends Component {
    render() {
        let className = this.props.className;
        let classList = className ? className + ' menuItem' : 'menuItem';
        if (this.props.active) {
            classList += ' active';
        }
        return (
            <li className={ classList } onClick={ this.props.onClick }>
                <div className='menuItemContent noselect'>
                    { this.props.name }
                </div>
            </li>
        );
    }
}

function MenuSpacer(props) {
    return (
        <li className='spacer'>
            <div className='menuItemContent noselect'></div>
        </li>
    );
}

class DetailsMenuComponent extends Component {
    constructor() {
        super();
        this.state = {
            activeItem: 'Properties'
        };
    }
    handleClick = (event, name) => { 
        this.setState({ activeItem: name });
        eventsService.publish(DETAILS_MENU_TAB_SELECTION_EVENT, name);
    };
    isActive = (name) => { return this.state.activeItem === name; };
    render() {
        return (
            <div id="detailsMenu" className="detailsMenu">
                <ul className='menuItems'>
                    <MenuItem   
                        name='Properties' 
                        active={ this.isActive('Properties') } 
                        onClick={ (proxy, event) => this.handleClick(event, 'Properties') } /> 
                    {/* Add more tabs here */}
                    <MenuSpacer />
                </ul>
            </div>
        );
    }
}

export default DetailsMenuComponent;