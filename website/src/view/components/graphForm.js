

import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import GraphNameInputContainer from '../containers/graphNameInputContainer';


const GraphForm = (props) => {
    return(
        <Form>
            <GraphNameInputContainer />
        </Form>
    );
};

export default GraphForm;