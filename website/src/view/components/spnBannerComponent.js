/* jshint ignore:start */ // Dont use jshint to parse react jsx


import React from 'react';
import NavMenuContainer from '../containers/navMenuContainer';
import { Header } from 'semantic-ui-react';


const spacer = (w) => (<span style={{ width: w, height: '90%', display: 'inline-block' }}>&nbsp;</span>);

const SpnLogo = (props) => (
    <div style={{ display: 'flex', verticalAlign: 'center', alignItems: 'center' }}>
        { spacer(props.spacing) }
        <img src="../images/logo.svg" className="logo"></img>
        { spacer(props.spacing) }
    </div>
);

const SpnBannerComponent = (props) => {
    let header = <Header as='h3' style={{ marginTop: '12px', marginLeft: '10px' }}><span style={{ color: 'white' }}>SPNP Gui</span></Header>;
    return (
        <div id='banner' className='banner'>
            <SpnLogo spacing='10px'/>
            { header }
            <NavMenuContainer /> 
        </div>
    );
};

export default SpnBannerComponent;