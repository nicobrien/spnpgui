/* jshint ignore:start */ // Dont use jshint to parse react jsx


import React from 'react';
import { Form, Input } from 'semantic-ui-react';

/**
 * @param {String} id The id of the currently selected cell. This is required
 * so that the onChange event knows how to update the canvas with changes
 * @param {String} value The value that will be displayed in the input
 * @param {String} label The Input heading label
 * @param {String} placeHolder the text to display when the Input is empty
 * @param {Function} onChange The callback function that will fire when
 * the onChange event fires
 */
const PropertyInputComponent = ({ id, value, label, placeHolder, onChange }) => {
    return (
        <Form.Field>
            <label>{ label }</label>
            <Input 
                placeholder={ placeHolder }
                value={ value } 
                onChange={ onChange.bind(this, id) } /> 
        </Form.Field>
    );
};

export default PropertyInputComponent;