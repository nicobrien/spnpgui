
import React from 'react';
import { Button } from 'semantic-ui-react';
import EmptyStateComponent from './emptyStateComponent';

/**
 * This component provides a 'blank slate' action message
 * for users who have no graphs
 */
const EmptyWelcomeComponent = (props) => {
    const { onClick } = props;
    return (
        <EmptyStateComponent 
            iconName='info'
            iconSize='huge'
            heading='Welcome' 
            message='Welcome to SPNP GUI: a visual stochastic petri net editor. Create a new stochastic petri net.'
            callToAction={
                <Button 
                color='green'
                icon='plus'
                content='Create'
                onClick={ onClick } />
            }
        />
    );
};

export default EmptyWelcomeComponent;