
import React from 'react';
import ToolbarComponent from './toolbarComponent';
import SpnPropertiesComponent from './spnPropertiesComponent';
import SpnMenuContainer from '../containers/spnMenuContainer';
import CSPLCodeContainer from '../containers/csplCodeContainer';
import GraphsListContainer from '../containers/graphsListContainer';
import AnalyzeGraphContainer from '../containers/analyzeGraphContainer';


const OpenGraphsComponent = (props) => {

};

const MainContentComponent = ({ activeView }) => {
    let content = <div></div>;
    switch (activeView) {
        case 'home':
            content = <GraphsListContainer />;
            break;
        case 'analyze':
            content = <AnalyzeGraphContainer />;
            break;
        case 'cspl':
            content = <CSPLCodeContainer />;
            break;
        case 'edit':
            // do the default
        default:
            content = (
                <div>
                    <ToolbarComponent />
                    <SpnMenuContainer />
                    <SpnPropertiesComponent /> 
                </div>
            );
            break;
    }
    return content;
};

export default MainContentComponent;