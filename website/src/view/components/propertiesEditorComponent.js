/* jshint ignore:start */ // Dont use jshint to parse react jsx

'use strict';

import React, { Component } from 'react';
import { Input } from 'semantic-ui-react';
import EmptyStateComponent from '../components/emptyStateComponent';
import GraphForm from '../components/graphForm';
import { eventsService, GRAPH_SELECTION_CHANGED_EVENT } from '../../controllers/events';
import Logger from '../../controllers/logger';

import PlaceForm from './placeForm';
import TransitionForm from './transitionForm';

import SpnpPlace from '../../models/place';
import SpnpTransition from '../../models/transition';

const logger = new Logger({ name: 'PropertiesEditorComponent'});


class PropertiesEditorComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedCellId: '',
            selectedCellValue: {},
            graphSelection: 'other'
        };
        this._changeEvent = eventsService.subscribe(GRAPH_SELECTION_CHANGED_EVENT, this.onSelectionChanged.bind(this));
    }
    componentWillUnmount() {
        logger.fine('Unbinding change event binding:', this._changeEvent);
        this._changeEvent.remove();
    }
    // if one place or one transition was seleced then we want to update the state of the properties editor
    // and push the id of the selection down through props
    onSelectionChanged = (params) => {
        if (params && params.selected) {
            const cells = params.selected;
            if (cells.length !== 1) {
                this.setState({ graphSelection: 'other', selectedCellId: '', selectedCellProps: {} });
                return;
            }
            const selected = cells[0];
            let id = selected.id;
            if (id && SpnpPlace.isPlace(selected)) {
                this.setState({ 
                    graphSelection: 'place', 
                    selectedCellId: id,
                    selectedCellValue: selected.value
                });
            } else if (id && SpnpTransition.isTransition(selected)) {
                this.setState({ 
                    graphSelection: 'transition',
                    selectedCellId: id,
                    selectedCellValue: selected.value 
                });
            } else {
                this.setState({ 
                    graphSelection: 'other',
                    selectedCellId: '',
                    selectedCellValue: {}
                });
            }
        }
    }
    render() {
        let selected = this.state.graphSelection;
        let form = <div></div>;
        if (selected === 'place') {
            form = <PlaceForm 
                        selectedCellId={ this.state.selectedCellId } 
                        selectedCellValue={ this.state.selectedCellValue }
                    />;
        } else if (selected === 'transition') {
            form = <TransitionForm 
                        selectedCellId={ this.state.selectedCellId }
                        selectedCellValue={ this.state.selectedCellValue }
                    />;
        } else {
            form = <GraphForm />;
        }
        return (
            <div className='propertiesForm'>{form}</div>
        );
    }
}

export default PropertiesEditorComponent;