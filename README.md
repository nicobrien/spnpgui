# SPNP Gui

The use of accurate stochastic models assists an analyst to predict future or expected states in a complex system. One formalism which can be used to model logical interactions in complex systems are Stochastic Petri Nets (SPN)s. There are various software packages that simplify the creation, manipulation and analysis of a SPN. One of these is the Stochastic Petri Nets Package (SPNP).

This application provides: 
 - a container service for SPNP, 
 - a web service to mediate requests between the client, DB persistence, and the SPNP container application
 - a React/mxGraph front end for manipulating SPNs graphically,
 - tooling to convert graphical representations of SPNs to executable C code, and 
 - basic graphing of various stochastic analysis functionality

Setup:
```sh
git clone https://gitlab.com/nicobrien/spnpgui.git

nvm install 8.5.0
nvm use 8.5.0

cd spnpgui/website
npm install
npx gulp package
cd ../api
npm install
npm run start
```

See the docs or https://gitlab.com/nicobrien/spnpgui-report for futher information
